<?php

namespace App\Providers;

use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\ClassAssignedCourse;
use App\Models\Classes;
use App\Models\Comment;
use App\Models\Course;
use App\Models\Lesson;
use App\Models\Like;
use App\Models\Post;
use App\Models\Question;
use App\Models\StudentClass;
use App\Models\Test;
use App\Policies\ChatMessagePolicy;
use App\Policies\ChatRoomPolicy;
use App\Policies\ClassAssignedCoursePolicy;
use App\Policies\ClassPolicy;
use App\Policies\CommentPolicy;
use App\Policies\CoursePolicy;
use App\Policies\LessonPolicy;
use App\Policies\LikePolicy;
use App\Policies\PostPolicy;
use App\Policies\QuestionPolicy;
use App\Policies\StudentClassPolicy;
use App\Policies\TestPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Course::class => CoursePolicy::class,
        Lesson::class => LessonPolicy::class,
        Test::class => TestPolicy::class,
        Question::class => QuestionPolicy::class,
        Classes::class => ClassPolicy::class,
        StudentClass::class => StudentClassPolicy::class,
        ClassAssignedCourse::class => ClassAssignedCoursePolicy::class,
        Post::class => PostPolicy::class,
        Comment::class => CommentPolicy::class,
        Like::class => LikePolicy::class,
        ChatRoom::class => ChatRoomPolicy::class,
        ChatMessage::class => ChatMessagePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
