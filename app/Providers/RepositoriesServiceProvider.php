<?php

namespace App\Providers;

use App\Repositories\ClassAssignedCourse\ClassAssignedCourseRepository;
use App\Repositories\ClassAssignedCourse\ClassAssignedCourseRepositoryInterface;
use App\Repositories\Classes\ClassRepository;
use App\Repositories\Classes\ClassRepositoryInterface;
use App\Repositories\Course\CourseRepository;
use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\CourseCategory\CourseCategoryRepository;
use App\Repositories\CourseCategory\CourseCategoryRepositoryInterface;
use App\Repositories\Lesson\LessonRepository;
use App\Repositories\Lesson\LessonRepositoryInterface;
use App\Repositories\Notification\NotificationRepository;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\Post\PostRepository;
use App\Repositories\Post\PostRepositoryInterface;
use App\Repositories\Question\QuestionRepository;
use App\Repositories\Question\QuestionRepositoryInterface;
use App\Repositories\StudentAnswer\StudentAnswerRepository;
use App\Repositories\StudentAnswer\StudentAnswerRepositoryInterface;
use App\Repositories\StudentClass\StudentClassRepository;
use App\Repositories\StudentClass\StudentClassRepositoryInterface;
use App\Repositories\StudentCompleteLesson\StudentCompleteLessonRepository;
use App\Repositories\StudentCompleteLesson\StudentCompleteLessonRepositoryInterface;
use App\Repositories\TeacherApprovalCourse\TeacherApprovalCourseRepository;
use App\Repositories\TeacherApprovalCourse\TeacherApprovalCourseRepositoryInterface;
use App\Repositories\Test\TestRepository;
use App\Repositories\Test\TestRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\UserViewHistory\UserViewHistoryRepository;
use App\Repositories\UserViewHistory\UserViewHistoryRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(CourseCategoryRepositoryInterface::class, CourseCategoryRepository::class);
        $this->app->bind(CourseRepositoryInterface::class, CourseRepository::class);
        $this->app->bind(LessonRepositoryInterface::class, LessonRepository::class);
        $this->app->bind(TestRepositoryInterface::class, TestRepository::class);
        $this->app->bind(QuestionRepositoryInterface::class, QuestionRepository::class);
        $this->app->bind(ClassRepositoryInterface::class, ClassRepository::class);
        $this->app->bind(ClassAssignedCourseRepositoryInterface::class, ClassAssignedCourseRepository::class);
        $this->app->bind(StudentClassRepositoryInterface::class, StudentClassRepository::class);
        $this->app->bind(TeacherApprovalCourseRepositoryInterface::class, TeacherApprovalCourseRepository::class);
        $this->app->bind(UserViewHistoryRepositoryInterface::class, UserViewHistoryRepository::class);
        $this->app->bind(StudentCompleteLessonRepositoryInterface::class, StudentCompleteLessonRepository::class);
        $this->app->bind(StudentAnswerRepositoryInterface::class, StudentAnswerRepository::class);
        $this->app->bind(PostRepositoryInterface::class, PostRepository::class);
        $this->app->bind(NotificationRepositoryInterface::class, NotificationRepository::class);
    }
}
