<?php

namespace App\Console;

use App\Console\Commands\SendNotification;
use App\Console\Commands\SendNotificationWorkTest;
use App\Console\Commands\UserViewStatiscCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SendNotificationWorkTest::class,
        SendNotification::class,
        UserViewStatiscCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('notif:test')->everyMinute();
        // $schedule->command('notif:run')->everyMinute();
        $schedule->command('userview:statisc')->dailyAt('01:00');
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
