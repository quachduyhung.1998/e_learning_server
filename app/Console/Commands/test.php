<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $domain = 'test.hdev.info';

        $process = new Process(['sudo', 'touch', '/etc/httpd/conf/'.$domain.'.conf']);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $process = new Process(['sudo', 'touch', '/etc/httpd/conf.d/'.$domain.'.conf']);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $process = new Process(['sudo', 'touch', '/etc/httpd/conf.d/'.$domain.'-le-ssl.conf']);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        $this->writeFileConf($domain);
        $this->writeFileConfd($domain);
        $this->writeFileConfdSSL($domain);
        $this->runCertbot($domain);
    }

    public function writeFileConf($domain)
    {
        $stream = fopen('/etc/httpd/conf/'.$domain.'.conf', 'w+');

        $process = new Process(['cat']);
        $process->setInput($stream);
        $process->start();

        fwrite($stream, '<VirtualHost *:80>
    <Directory /var/www/html/e_learning_server>
        DirectoryIndex index.php index.html
        AllowOverride ALL
    </Directory>
    DocumentRoot /var/www/html/e_learning_server
    ServerName '.$domain.'
    ServerAlias www.'.$domain.'
</VirtualHost>');
        fclose($stream);

        $process->wait();

        echo $process->getOutput();
    }

    public function writeFileConfd($domain)
    {
        $stream = fopen('/etc/httpd/conf.d/'.$domain.'.conf', 'w+');

        $process = new Process(['cat']);
        $process->setInput($stream);
        $process->start();

        fwrite($stream, '<VirtualHost *:80>
    <Directory /var/www/html/e_learning_server/public/>
        DirectoryIndex index.php
        AllowOverride ALL
    </Directory>
    DocumentRoot /var/www/html/e_learning_server/public
    ServerName '.$domain.'
    ServerAlias www.'.$domain.'
RewriteEngine on
RewriteCond %{SERVER_NAME} ='.$domain.' [OR]
RewriteCond %{SERVER_NAME} =www.'.$domain.'
RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>');
        fclose($stream);

        $process->wait();

        echo $process->getOutput();
    }

    public function writeFileConfdSSL($domain)
    {
        $stream = fopen('/etc/httpd/conf/'.$domain.'-le-ssl.conf', 'w+');

        $process = new Process(['cat']);
        $process->setInput($stream);
        $process->start();

        fwrite($stream, '<IfModule mod_ssl.c>
<VirtualHost *:443>
    <Directory /var/www/html/e_learning_server/public/>
        DirectoryIndex index.php
        AllowOverride ALL
    </Directory>
    DocumentRoot /var/www/html/e_learning_server/public
    ServerName '.$domain.'
    ServerAlias www.'.$domain.'
SSLCertificateFile /etc/letsencrypt/live/'.$domain.'/cert.pem
SSLCertificateKeyFile /etc/letsencrypt/live/'.$domain.'/privkey.pem
Include /etc/letsencrypt/options-ssl-apache.conf
SSLCertificateChainFile /etc/letsencrypt/live/hdev.info/chain.pem
</VirtualHost>
</IfModule>');
        fclose($stream);

        $process->wait();

        echo $process->getOutput();
    }

    public function writeFileNginx($domain)
    {
        $stream = fopen('/etc/httpd/conf/'.$domain.'.conf', 'w+');

        $process = new Process(['cat']);
        $process->setInput($stream);
        $process->start();

        fwrite($stream, 'server {
    listen 443 ssl;
    server_name '.$domain.' www.'.$domain.';
    #ssl_certificate_key  /home/ssl/acabiz.key;
    #ssl_certificate /home/ssl/acabiz.pem;
    error_log /home/acabiz/logs/acabiz_error.log;
    index index.php;
    root /home/acabiz/public_html/public;
    autoindex off;
    index index.php;
    location = /favicon.ico {
            log_not_found off;
            access_log off;
    }

    location = /robots.txt {
    allow all;
    log_not_found off;
    access_log off;
    }

    location ~ \.(js|css|png|jpg|jpeg|gif|ico|woff|woff2|ttf|svg|eot|otf)$ {
            add_header      Pragma "public";
            add_header      Cache-Control "public";
            expires         3M;
            access_log      off;
            log_not_found   off;
    }

    location / {
    try_files $uri $uri/ /index.php?$query_string;
    }

    #location ~* ^/(assets|files|robots\.txt) { }

    location ~ ^/(status|ping)$ {
            access_log off;
            allow 127.0.0.1;
            allow 123.25.21.12;
            allow 113.190.253.131;
            deny all;
            fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
            include fastcgi_params;
            #fastcgi_pass 127.0.0.1:9000;
            fastcgi_pass    unix:/var/run/php-fpm/salemall.vn.sock;
    }

    #location ~ .(php|phtml)$ {
        location ~ \.php$ {
                fastcgi_pass    unix:/var/opt/remi/php72/run/php-fpm/acabiz.sock;
                fastcgi_index index.php;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                include fastcgi_params;
                fastcgi_split_path_info ^(.+\.php)(/.+)$;
                fastcgi_read_timeout 60000;
        }
        location ~ /\.ht {
        deny  all;
    }

    #listen 443 ssl; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}

server {
    if ($host = '.$domain.') {
        return 301 https://$host$request_uri;
    } # managed by Certbot


        listen 80;
        server_name '.$domain.' www.'.$domain.';
    return 404; # managed by Certbot
}');
        fclose($stream);

        $process->wait();

        echo $process->getOutput();
    }

    public function runCertbot($domain)
    {
        $process = new Process(['sudo', 'certbot', '--apache', '-d', $domain]);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        echo $process->getOutput();
    }
}
