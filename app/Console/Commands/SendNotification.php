<?php

namespace App\Console\Commands;

use App\Models\Notification;
use Illuminate\Console\Command;

class SendNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification from notifications table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $notifications = Notification::query()
            ->where('created_at', '>', date('Y-m-d H:i:s', time() - 60))
            ->where('created_at', '<',  date('Y-m-d H:i:s'))
            ->get();

        dd($notifications);
    }
}
