<?php

namespace App\Console\Commands;

use App\Models\Classes;
use App\Models\Test;
use App\Services\NotificationService;
use App\Services\ToolService;
use Illuminate\Console\Command;

class SendNotificationWorkTest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notif:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Notify users when it's time to take the test";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $list_test_send = [];
        $tests = Test::query()
            ->where('status', 1)
            ->whereNotNull('time_start')
            ->where('time_start', '>', date('Y-m-d H:i:s'))
            ->get();
        foreach ($tests as $test) {
            // 15 phút
            $time_before_start = strtotime($test->time_start) - (15 * 60);
            $time_now = strtotime(date('Y-m-d H:i'));
            if ($time_before_start == $time_now) {
                $list_test_send[] = [
                    'test' => $test,
                    'list_class' => $test->course->classes
                ];
            }
        }
        foreach ($list_test_send as $data) {
            $test = $data['test'];
            $list_token_fcm = [];
            foreach ($data['list_class'] as $class) {
                $class = Classes::find($class['id']);
                if ($class && $class->students) {
                    foreach ($class->students as $student) {
                        if ($student && isset($student->fcm_token) && $student->fcm_token != '' && $student->fcm_token !== null) {
                            $list_token_fcm[] = $student->fcm_token;
                        }
                    }
                }
            }
            if (!empty($list_token_fcm)) {
                $body = 'Còn 15 phút nữa đến giờ làm bài kiểm tra: "'.$test->name.'"';
                NotificationService::sendMulti($list_token_fcm, [
                    'title' => "E-learning thông báo",
                    'body' => ToolService::subWord($body),
                    'icon' => 'https://hdev.info/images/favicon.png',
                    'click_action' => 'https://e-learning.hdev.info/'        
                ]);
            }
        }
        $this->info("Notification sent!");
    }
}
