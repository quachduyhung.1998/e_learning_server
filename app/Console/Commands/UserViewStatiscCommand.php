<?php

namespace App\Console\Commands;

use App\Models\UserViewHistory;
use App\Models\UserViewStatiscs;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UserViewStatiscCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'userview:statisc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Statisc view of user flow daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info("Started calculating yesterday's numbers...");

        $yesterday = Carbon::yesterday()->format('Y-m-d');
        $views = UserViewHistory::whereBetween('created_at', [$yesterday.' 00:00:00', $yesterday.' 23:59:59'])->get();
        
        $list_unique_user_id = [];
        foreach ($views as $item) {
            if (!in_array($item->user_id, $list_unique_user_id)) {
                $list_unique_user_id[] = $item->user_id;
            }
        }

        $statisc = UserViewStatiscs::where('date', $yesterday)->first();
        if (!$statisc) {
            $statisc = new UserViewStatiscs();
            $statisc->date = $yesterday;
        }
        $statisc->view = count($list_unique_user_id);
        $statisc->save();

        // // chạy số liệu từ 01-04-2021
        // $date_fix = '2021-04-01';
        // $today = date('Y-m-d');
        // $data = UserViewHistory::query()
        //     ->whereBetween('created_at', [$date_fix, $today])
        //     ->orderBy('created_at')
        //     ->get()
        //     ->groupBy(function($value) {
        //         return Carbon::parse($value->created_at)->format('Y-m-d');
        //     });
            
        // // $data = UserViewHistory::select(
        // //     DB::raw("(DATE_FORMAT(created_at, '%d/%m/%Y')) as time"),
        // //         DB::raw("count(*) as total")
        // //     )
        // //     ->whereBetween('created_at', [$last_6_day, $today])
        // //     ->orderBy('created_at')
        // //     ->groupBy('user_id', DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
        // //     ->get();

        // // $data = DB::table("user_view_histories")
        // //     ->select(
        // //         DB::raw("COUNT(*) as total"),                    
        // //         DB::raw("(DATE_FORMAT(created_at, '%d/%m/%Y')) as time")
        // //     )
        // //     ->whereBetween('created_at', [$last_6_day, $today])
        // //     ->groupBy('user_view_histories.user_id', DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y')"))
        // //     ->get();
        
        // $timeDiff = abs(strtotime($date_fix) - strtotime($today));
        // $numberDays = $timeDiff/86400;
        // $numberDays = intval($numberDays);
        // $list_date = [];
        // for ($i = $numberDays; $i >= 0; $i--) { 
        //     $list_date[] = Carbon::now()->subDays($i)->format('Y-m-d');
        // }

        // foreach ($list_date as $date) {
        //     $statisc = new UserViewStatiscs();
        //     $statisc->date = $date;
            
        //     $exists = false;
        //     foreach ($data as $key => $value) {
        //         if ($date == $key) {
        //             $exists = true;
        //             $list_unique_user_id = [];
        //             foreach ($value as $item) {
        //                 if (!in_array($item->user_id, $list_unique_user_id)) {
        //                     $list_unique_user_id[] = $item->user_id;
        //                 }
        //             }
        //             $statisc->view = count($list_unique_user_id);
        //             $statisc->save();
        //             break;
        //         }
        //     }
        //     if (!$exists) {
        //         $statisc->view = 0;
        //         $statisc->save();
        //     }
        // }
    }
}
