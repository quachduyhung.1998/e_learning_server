<?php

namespace App\Repositories\Notification;

interface NotificationRepositoryInterface
{
    public function findById($notificationId);
    
    public function getAll();

    public function create($attributes = []);

    public function createMany($list_user_id = [], $attributes = []);

    public function updateStatusReaded($notificationId);

    public function delete($notificationId);
}