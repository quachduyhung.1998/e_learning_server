<?php

namespace App\Repositories\Notification;

use App\Models\Notification;

class NotificationRepository implements NotificationRepositoryInterface
{
    public function findById($notificationId)
    {
        $post = Notification::find($notificationId);
        if (!$post) {
            return false;
        }
        return $post;
    }
    
    public function getAll()
    {
        return Notification::query()
            ->where('user_id', auth()->user()->id)
            ->orderBy('id', 'desc')
            ->paginate();
    }

    public function create($attributes = [])
    {
        return Notification::create([
            'user_id' => (int) $attributes['user_id'],
            'type' => isset($attributes['type']) ? (int) $attributes['type'] : null,
            'content_id' => isset($attributes['content_id']) ? (int) $attributes['content_id'] : null,
            'message' => $attributes['message'],
            'url' => isset($attributes['url']) ? $attributes['url'] : null,
        ]);
    }

    public function createMany($list_user_id = [], $attributes = [])
    {
        $data = [];
        foreach ($list_user_id as $user_id) {
            $data[] = [
                'user_id' => (int) $user_id,
                'type' => isset($attributes['type']) ? (int) $attributes['type'] : null,
                'content_id' => isset($attributes['content_id']) ? (int) $attributes['content_id'] : null,
                'message' => $attributes['message'],
                'url' => $attributes['url'],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
        }
        return Notification::insert($data);
    }

    public function updateStatusReaded($notificationId)
    {
        $notif = $this->findById($notificationId);
        if ($notif) {
            $notif->status = Notification::STATUS_READED;
            $notif->save();
            return true;
        }
        return false;
    }

    public function delete($notificationId)
    {
        $notif = $this->findById($notificationId);
        if ($notif) {
            $notif->delete();
            return true;
        }
        return false;
    }
}