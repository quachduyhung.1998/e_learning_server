<?php

namespace App\Repositories\Lesson;

use App\Models\Lesson;
use App\Services\ToolService;
use Illuminate\Support\Str;

class LessonRepository implements LessonRepositoryInterface
{
    public function findById($lessonId)
    {
        $lesson = Lesson::find($lessonId);
        if (!$lesson) {
            return false;
        }
        return $lesson;
    }

    public function create($attributes = [])
    {
        return Lesson::create([
            'name' => $attributes['name'],
            'slug' => Str::slug($attributes['name'], '-'),
            'description' => $attributes['description'] ?? null,
            'content' => $attributes['content'],
            'image' => $attributes['image'] ?? null,
            'order' => $attributes['order'] ?? null,
            'course_id' => (int) $attributes['course_id'],
            'type' => isset($attributes['type']) ? (int) $attributes['type'] : 0
        ]);
    }

    public function update($lessonId, $attributes = [])
    {
        $lesson = $this->findById($lessonId);
        if ($lesson) {
            $lesson->update([
                'name' => $attributes['name'],
                'slug' => Str::slug($attributes['name'], '-'),
                'description' => $attributes['description'] ?? null,
                'content' => $attributes['content'],
                'image' => $attributes['image'] ?? null,
                'order' => $attributes['order'] ?? null,
                'type' => isset($attributes['type']) ? (int) $attributes['type'] : (isset($lesson->type) ? $lesson->type : 0)
            ]);
            return $lesson;
        }
        return false;
    }

    public function handleChangeOrder($lessonId, $order)
    {
        $lesson = $this->findById($lessonId);
        if ($lesson) {
            $lesson->order = (int) $order;
            $lesson->save();
            return $lesson;
        }
        return false;
    }
    
    public function delete($lessonId)
    {
        $lesson = $this->findById($lessonId);
        if ($lesson) {
            // xóa ảnh cũ
            ToolService::handleDeleteImage(last(explode('/', $lesson->image)), 'lesson');
            $lesson->delete();
            return true;
        }
        return false;
    }
}