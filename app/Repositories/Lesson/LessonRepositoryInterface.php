<?php

namespace App\Repositories\Lesson;

interface LessonRepositoryInterface
{
    public function findById($lessonId);

    public function create($attributes = []);

    public function update($lessonId, $attributes = []);

    public function handleChangeOrder($lessonId, $order);

    public function delete($lessonId);
}