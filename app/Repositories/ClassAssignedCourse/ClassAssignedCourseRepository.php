<?php

namespace App\Repositories\ClassAssignedCourse;

use App\Models\ClassAssignedCourse;
use App\Services\ToolService;
use Illuminate\Support\Facades\DB;

class ClassAssignedCourseRepository implements ClassAssignedCourseRepositoryInterface
{
    public function findById($classId, $courseId)
    {
        return ClassAssignedCourse::query()
            ->where('class_id', $classId)
            ->where('course_id', $courseId)
            ->first();
    }

    public function getAll($filter)
    {
        $list = ClassAssignedCourse::with([
            'class' => function($query) use ($filter) {
                if (!empty($filter) && isset($filter['class_name'])) {
                    $query->where('classes.name', 'LIKE', '%'. $filter['class_name'] .'%');
                }
            },
            'course' => function($query) use ($filter) {
                if (!empty($filter) && isset($filter['course_name'])) {
                    $query->where('courses.name', 'LIKE', '%'. $filter['course_name'] .'%');
                }
            }
        ])->paginate();

        // $list = DB::table('class_assigned_courses')->select('*')
        //     ->join('classes', function($query) use ($filter) {
        //         $query->on('class_assigned_courses.class_id', '=', 'classes.id');
        //         if (!empty($filter) && isset($filter['class_name'])) {
        //             $query->where('classes.name', 'LIKE', '%'. $filter['class_name'] .'%');
        //         }
        //     })
        //     ->join('courses', function($query) use ($filter) {
        //         $query->on('class_assigned_courses.course_id', '=', 'courses.id');
        //         if (!empty($filter) && isset($filter['course_name'])) {
        //             $query->where('courses.name', 'LIKE', '%'. $filter['course_name'] .'%');
        //         }
        //     })
        //     ->paginate();

        if (!$list) {
            return [];
        }
        
        $datas = [];
        foreach ($list as $item) {
            if ($item->class !== null && $item->course !== null) {
                $datas[] = $item;
            }
        }

        if (!$datas) {
            return [];
        }

        $datas = ToolService::handleCustomPagination($list, $datas);

        return $datas;
    }

    public function create($classId, $courseId)
    {
        $class_assigned = $this->findById($classId, $courseId);
        if (!$class_assigned) {
            ClassAssignedCourse::create([
                'class_id' => (int) $classId,
                'course_id' => (int) $courseId
            ]);
            return true;
        }

        return false;
    }
    
    public function delete($classId, $courseId)
    {
        $class_assigned = $this->findById($classId, $courseId);
        if ($class_assigned) {
            $class_assigned->delete();
            return true;
        }
        return false;
    }
}