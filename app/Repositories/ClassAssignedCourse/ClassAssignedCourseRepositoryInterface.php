<?php

namespace App\Repositories\ClassAssignedCourse;

interface ClassAssignedCourseRepositoryInterface
{
    public function findById($classId, $courseId);

    public function getAll($filter);
    
    public function create($classId, $courseId);

    public function delete($classId, $courseId);
}