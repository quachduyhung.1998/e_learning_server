<?php

namespace App\Repositories\Question;

interface QuestionRepositoryInterface
{
    public function findQuestion($questionId);
    
    public function findById($questionId);

    public function findByListId($listTestId);

    public function findByName($questionName);
    
    public function getAllQuestionOfCategory($courseCategoryId, $questionName);

    public function getAllQuestionOfTest($testId);

    public function create($attributes = []);
    
    public function update($questionId, $attributes = []);
    
    public function delete($questionId);
}