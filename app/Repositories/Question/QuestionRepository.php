<?php

namespace App\Repositories\Question;

use App\Models\Question;
use App\Models\User;

class QuestionRepository implements QuestionRepositoryInterface
{
    public function findQuestion($questionId)
    {
        return Question::query()
            ->where('id', $questionId)
            ->first();
    }

    public function findById($questionId)
    {
        return Question::query()
            ->where('id', $questionId)
            ->where('status', 1)
            ->first();
    }

    public function findByListId($listTestId)
    {
        $list_question = Question::query()
            ->whereIn('id', $listTestId)
            ->where('status', 1)
            ->get()
            ->map(function($question) {
                return $question->format($question);
            });

        return $list_question ?? [];
    }

    public function findByName($questionName)
    {
        return Question::query()
            ->where('name', $questionName)
            ->where('user_id', auth()->user()->id)
            ->where('status', 1)
            ->get();
    }

    public function getAllQuestionOfCategory($courseCategoryId, $questionName)
    {
        $query = Question::where('course_category_id', $courseCategoryId)->where('status', 1);

        if (isset($questionName) && $questionName != null) {
            $query->where('name', 'LIKE', '%'. $questionName .'%');
        }

        if (auth()->user()->role === User::ROLE_TEACHER) {
            $query = $query->where('user_id', auth()->user()->id);
        }

        $questions = $query->get()
            ->map(function($question) {
                return $question->format($question);
            });

        return $questions ?? [];
    }

    public function getAllQuestionOfTest($listQuestionId)
    {
        $questions = Question::query()
            ->whereIn('id', $listQuestionId)
            // ->where('status', 1)
            ->get()
            ->map(function($question) {
                return $question->format($question);
            });

        return $questions ?? [];
    }

    public function create($attributes = [])
    {
        return Question::create([
            'name' => $attributes['name'],
            'user_id' => auth()->user()->id,
            'course_category_id' => (int) $attributes['course_category_id'],
            'type_question' => (int) $attributes['type_question'],
            'answers' => $attributes['answers'],
            'mark' => (int) $attributes['mark']
        ]);
    }
    
    public function update($questionId, $attributes = [])
    {
        $question = $this->findById($questionId);
        if ($question) {
            $question->update([
                'name' => $attributes['name'],
                'course_category_id' => (int) $attributes['course_category_id'],
                'type_question' => (int) $attributes['type_question'],
                'answers' => $attributes['answers'],
                'mark' => (int) $attributes['mark']
            ]);
            return $question;
        }
        return false;
    }
    
    public function delete($questionId)
    {
        $question = $this->findById($questionId);
        if ($question) {
            $question->status = 0;
            $question->save();
            return true;
        }
        return false;
    }

}