<?php

namespace App\Repositories\Test;

use App\Models\Question;
use App\Models\Test;
use Illuminate\Support\Str;

class TestRepository implements TestRepositoryInterface
{
    public function findById($testId)
    {
        $test = Test::find($testId);
        if (!$test) {
            return false;
        }
        return $test;
    }
    
    public function create($attributes = [])
    {
        $total_mark = Question::whereIn('id', json_decode($attributes['list_question_id'], true))->sum('mark');
        return Test::create([
            'name' => $attributes['name'],
            'slug' => Str::slug($attributes['name'], '-'),
            'description' => $attributes['description'] ?? null,
            'lesson_id' => (int) $attributes['lesson_id'],
            'course_id' => (int) $attributes['course_id'],
            'list_question_id' => (string) $attributes['list_question_id'],
            'total_question' => count(json_decode($attributes['list_question_id'], true)),
            'total_mark' => (int) $total_mark,
            'time_work' => (int) $attributes['time_work'],
            'is_required' => $attributes['is_required'] ?? 0,
            'time_start' => $attributes['time_start'] ?? null,
            'time_end' => $attributes['time_end'] ?? null,
        ]);
    }

    public function update($testId, $attributes = [])
    {
        $test = $this->findById($testId);
        if ($test) {
            $total_mark = Question::whereIn('id', json_decode($attributes['list_question_id'], true))->sum('mark');
            $test->update([
                'name' => $attributes['name'],
                'slug' => Str::slug($attributes['name'], '-'),
                'description' => $attributes['description'] ?? null,
                'list_question_id' => (string) $attributes['list_question_id'],
                'total_question' => count(json_decode($attributes['list_question_id'], true)),
                'total_mark' => (int) $total_mark,
                'time_work' => (int) $attributes['time_work'],
                'is_required' => $attributes['is_required'] ?? 0,
                'time_start' => $attributes['time_start'] ?? null,
                'time_end' => $attributes['time_end'] ?? null,
            ]);
            return $test;
        }
        return false;
    }
    
    public function delete($testId)
    {
        $test = $this->findById($testId);
        if ($test) {
            $test->delete();
            return true;
        }
        return false;
    }
}