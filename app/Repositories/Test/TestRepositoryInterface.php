<?php

namespace App\Repositories\Test;

interface TestRepositoryInterface
{
    public function findById($testId);

    public function create($attributes = []);

    public function update($testId, $attributes = []);

    public function delete($testId);
}