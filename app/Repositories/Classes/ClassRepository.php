<?php

namespace App\Repositories\Classes;

use App\Models\Classes;
use App\Models\StudentClass;
use App\Models\User;
use Illuminate\Support\Str;

class ClassRepository implements ClassRepositoryInterface
{
    public function findById($classId)
    {
        return Classes::query()
            ->where('id', $classId)
            // ->where('status', 1)
            ->first();
    }

    public function getAllClass()
    {
        $classes = Classes::get();

        return $classes;
    }

    public function getAllClassOfUser()
    {
        $user_role = auth()->user()->role;
        $classes = [];
        if ($user_role === User::ROLE_STUDENT) {
            $classes = auth()->user()->inClasses->latest();
        }
        elseif ($user_role === User::ROLE_TEACHER) {
            $classes = auth()->user()->classes->latest();
        }
        elseif ($user_role === User::ROLE_ADMIN || $user_role === User::ROLE_SUPER_ADMIN) {
            $classes = Classes::with(['teacher'])->latest()->paginate(20);
        }

        return $classes;
    }

    public function create($attributes = [])
    {
        $class = Classes::create([
            'name' => $attributes['name'],
            'slug' => Str::slug($attributes['name'], '-'),
            'user_id' => (int) $attributes['user_id']
        ]);
        if (!$class) {
            return false;
        }

        return $this->format($class);
    }
    
    public function update($classId, $attributes = [])
    {
        $class = $this->findById($classId);
        if ($class) {
            $class->update([
                'name' => $attributes['name'],
                'slug' => Str::slug($attributes['name'], '-'),
                'user_id' => (int) $attributes['user_id']
            ]);
            return $this->format($class);
        }
        return false;
    }
    
    public function updateTotalStudent($classId, $number, $calculationName)
    {
        $class = $this->findById($classId);
        if ($class) {
            $old_total_student = $class->total_student;
            if ($calculationName == 'plus') {
                $class->total_student = $old_total_student + $number;
            } else {
                $class->total_student = $old_total_student != 0 ? $old_total_student - $number : 0;
            }
            $class->save();
            return true;
        }
        return false;
    }

    public function delete($classId)
    {
        $class = $this->findById($classId);
        if ($class) {
            // $class->status = 0;
            $class->delete();
            return true;
        }
        return false;
    }
    
    public function format($class)
    {
        $teacher = User::find($class->user_id);
        return [
            'id' => $class->id,
            'name' => $class->name,
            'slug' => $class->slug,
            'total_student' => $class->total_student ?? 0,
            'teacher' => [
                'id' => $teacher->id,
                'name' => $teacher->name,
                'email' => $teacher->email,
                'avatar' => $teacher->avatar,
            ],
            'status' => $class->status ?? 0,
            'created_at' => $class->created_at,
            'updated_at' => $class->updated_at,
        ];
    }

    /********* ADMIN *********/
    public function countAll()
    {
        return Classes::count();
    }
}