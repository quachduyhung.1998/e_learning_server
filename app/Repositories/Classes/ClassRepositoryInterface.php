<?php

namespace App\Repositories\Classes;

interface ClassRepositoryInterface
{
    public function findById($classId);

    public function getAllClass();

    public function getAllClassOfUser();
    
    public function create($attributes = []);
    
    public function update($classId, $attributes = []);

    public function updateTotalStudent($classId, $number, $calculationName);

    public function delete($classId);

    public function format($class);

    /********* ADMIN *********/
    public function countAll();
}