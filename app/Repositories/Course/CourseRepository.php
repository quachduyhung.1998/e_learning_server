<?php

namespace App\Repositories\Course;

use App\Models\Course;
use App\Models\StudentProgressCourse;
use App\Models\TeacherApprovalCourse;
use App\Models\Test;
use App\Services\ToolService;
use Illuminate\Support\Str;

class CourseRepository implements CourseRepositoryInterface
{
    const PAGINATION = 10;

    public function getAll($name = null, $course_category_id = null)
    {
        $query = Course::where('status', Course::STATUS_PUBLIC);
        if ($name && $name != null) {
            $query->where('name', 'LIKE', '%'. $name .'%');
        }
        if (isset($course_category_id) && $course_category_id != null) {
            $query->where('course_category_id', $course_category_id);
        }
        return $query->paginate(self::PAGINATION);
    }

    public function getAllCourseOfTeacher($params = [], $is_student = false, $list_course_id = [])
    {
        if ($is_student) {
            $query = Course::where('status', Course::STATUS_PUBLIC)
                ->whereIn('id', $list_course_id);
        } else {
            if (isset($params['status']) && $params['status'] != null) {
                $query = Course::where('user_id', auth()->user()->id)->where('status', $params['status']);
            } else {
                $query = Course::where('user_id', auth()->user()->id)
                    ->whereIn('status', [Course::STATUS_DRAFT, Course::STATUS_COMPLETE, Course::STATUS_PUBLIC]);
            }
        }

        if (isset($params['name']) && $params['name'] != null) {
            $query->where('name', 'LIKE', '%'. $params['name'] .'%');
        }

        if (isset($params['category']) && $params['category'] != null) {
            $query->where('course_category_id', $params['category']);
        }

        return $query->orderBy('id', 'desc')->paginate(self::PAGINATION);
    }

    public function getAllTest($courseId)
    {
        return Test::where('course_id', $courseId)->paginate(20);
    }

    public function findById($courseId)
    {
        $course = Course::find($courseId);
        if (!$course) {
            return false;
        }
        return $course;
    }

    public function create($attributes = [])
    {
        return Course::create([
            'name' => $attributes['name'],
            'slug' => Str::slug($attributes['name'], '-'),
            'description' => $attributes['description'] ?? null,
            'image' => $attributes['image'] ?? null,
            'course_category_id' => (int) $attributes['course_category_id'],
            'is_sequence' => (int) $attributes['is_sequence'] ?? 0,
            'user_id' => auth()->user()->id
        ]);
    }

    public function update($courseId, $attributes = [])
    {
        $course = $this->findById($courseId);
        if ($course) {
            $course->update([
                'name' => $attributes['name'],
                'slug' => Str::slug($attributes['name'], '-'),
                'description' => $attributes['description'] ?? null,
                'image' => $attributes['image'] ?? null,
                'course_category_id' => (int) $attributes['course_category_id'],
                'is_sequence' => (int) $attributes['is_sequence'] ?? 0,
            ]);
            return $course;
        }
        return false;
    }

    public function handleUpdateStatusComplete($courseId)
    {
        $course = $this->findById($courseId);
        if ($course) {
            $course->update([
                'status' => Course::STATUS_COMPLETE
            ]);
            return true;
        }
        return false;
    }

    public function handleUpdateStatusPublic($courseId)
    {
        $course = $this->findById($courseId);
        if ($course) {
            $course->update([
                'status' => Course::STATUS_PUBLIC
            ]);
            return true;
        }
        return false;
    }

    public function handleUpdateTotalLesson($courseId, $calculationName)
    {
        $course = $this->findById($courseId);
        if ($course) {
            $old_total_lesson = $course->total_lesson;
            if ($calculationName == 'plus') {
                $course->total_lesson = $old_total_lesson + 1;
            } else {
                $course->total_lesson = $old_total_lesson != 0 ? $old_total_lesson - 1 : 0;
            }
            $course->save();
            return true;
        }
        return false;
    }

    public function handleUpdateStatusSequence($courseId)
    {
        $course = $this->findById($courseId);
        if ($course) {
            $old_is_sequence = $course->is_sequence;
            $course->is_sequence = $old_is_sequence === Course::NOT_SEQUENCE ? Course::SEQUENCE : Course::NOT_SEQUENCE;
            $course->save();
            return true;
        }
        return false;
    }

    public function checkPermission($courseId)
    {
        $allow = TeacherApprovalCourse::where('course_id', $courseId)->where('user_id', auth()->user()->id)->first();
        if ($allow) {
            return true;
        }

        return false;
    }

    public function getProgressOfUser($courseId)
    {
        $percent = StudentProgressCourse::query()
            ->where('course_id', $courseId)
            ->where('user_id', auth()->user()->id)
            ->first();

        if (!$percent) {
            return 0;
        }

        return $percent->progress;
    }

    public function delete($courseId)
    {
        $course = $this->findById($courseId);
        if ($course) {
            // xóa ảnh cũ
            ToolService::handleDeleteImage(last(explode('/', $course->image)), 'course');
            $course->delete();
            return true;
        }
        return false;
    }

    /********* ADMIN *********/
    public function countAll()
    {
        return Course::where('status', Course::STATUS_PUBLIC)->count();
    }
}