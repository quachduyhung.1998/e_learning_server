<?php

namespace App\Repositories\Course;

interface CourseRepositoryInterface
{
    public function getAll($name = null, $course_category_id = null);

    public function getAllCourseOfTeacher($params = [], $is_student = false, $list_course_id = []);

    public function getAllTest($courseId);

    public function findById($courseId);

    public function create($attributes = []);

    public function update($courseId, $attributes = []);

    public function handleUpdateStatusComplete($courseId);

    public function handleUpdateStatusPublic($courseId);

    public function handleUpdateTotalLesson($courseId, $calculationName);

    public function handleUpdateStatusSequence($courseId);
    
    public function checkPermission($courseId);

    public function getProgressOfUser($courseId);

    public function delete($courseId);

    /********* ADMIN *********/
    public function countAll();
}