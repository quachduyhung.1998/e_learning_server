<?php

namespace App\Repositories\CourseCategory;

use App\Models\CourseCategory;
use App\Models\Course;

class CourseCategoryRepository implements CourseCategoryRepositoryInterface
{
    public function findById($courseCategoryId)
    {
        return CourseCategory::with([
                'courses' => function($query) {
                    $query->where('courses.status', Course::STATUS_PUBLIC);
                }
            ])
            ->where('id', $courseCategoryId)
            ->where('status', 1)
            ->first();
    }
    
    public function showListCourse($courseCategoryId)
    {
        return Course::with(['user'])
            ->where('course_category_id', $courseCategoryId)
            ->where('status', Course::STATUS_PUBLIC)
            ->paginate(20);
    }
}