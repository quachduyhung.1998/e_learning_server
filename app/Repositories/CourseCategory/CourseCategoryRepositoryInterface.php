<?php

namespace App\Repositories\CourseCategory;

interface CourseCategoryRepositoryInterface
{
    public function findById($courseCategoryId);

    public function showListCourse($courseCategoryId);
}