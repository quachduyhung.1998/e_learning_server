<?php

namespace App\Repositories\StudentClass;

interface StudentClassRepositoryInterface
{
    public function findById($classId, $userId);
    
    public function create($classId, $userId);

    public function delete($classId, $userId);
}