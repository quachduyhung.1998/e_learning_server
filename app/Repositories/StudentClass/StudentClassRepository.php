<?php

namespace App\Repositories\StudentClass;

use App\Models\StudentClass;

class StudentClassRepository implements StudentClassRepositoryInterface
{
    public function findById($classId, $userId)
    {
        return StudentClass::query()
            ->where('class_id', $classId)
            ->where('user_id', $userId)
            ->first();
    }

    public function create($classId, $userId)
    {
        // StudentClass::insertOrIgnore($arrayRecord);
        $student_class = $this->findById($classId, $userId);
        if (!$student_class) {
            StudentClass::create([
                'class_id' => (int) $classId,
                'user_id' => (int) $userId
            ]);
            return true;
        }

        return false;
    }
    
    public function delete($classId, $userId)
    {
        $student_class = $this->findById($classId, $userId);
        if ($student_class) {
            $student_class->delete();
            return true;
        }
        return false;
    }
}