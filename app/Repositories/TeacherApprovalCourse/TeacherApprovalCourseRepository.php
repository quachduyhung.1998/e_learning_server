<?php

namespace App\Repositories\TeacherApprovalCourse;

use App\Models\TeacherApprovalCourse;
use App\Services\ToolService;

class TeacherApprovalCourseRepository implements TeacherApprovalCourseRepositoryInterface
{
    public function findById($id)
    {
        return TeacherApprovalCourse::query()
            ->where('id', $id)
            ->first();
    }

    public function findByIdCourse($courseId)
    {
        return TeacherApprovalCourse::query()
            ->where('course_id', $courseId)
            ->first();
    }

    public function getAllCourseApproval()
    {
        $course_approval = TeacherApprovalCourse::query()
            ->where('user_id', auth()->user()->id)
            ->where('status', TeacherApprovalCourse::STATUS_NOT_APPROVED)
            ->orderBy('id', 'desc')
            ->get();
        if (empty($course_approval)) {
            return [];
        }

        return $course_approval;
    }

    public function getAllCourseApproved()
    {
        $course_approval = TeacherApprovalCourse::query()
            ->where('user_id', auth()->user()->id)
            ->where('status', TeacherApprovalCourse::STATUS_APPROVED)
            ->orderBy('id', 'desc')
            ->get();
        if (empty($course_approval)) {
            return [];
        }

        return $course_approval;
    }

    public function getAll($filter)
    {
        // $list = TeacherApprovalCourse::with(['user', 'course'])->paginate();
        $list = TeacherApprovalCourse::with([
            'user' => function ($query) use ($filter) {
                if (!empty($filter) && isset($filter['user_name'])) {
                    $query->where('users.name', 'LIKE', '%' . $filter['user_name'] . '%');
                }
            },
            'course' => function ($query) use ($filter) {
                if (!empty($filter) && isset($filter['course_name'])) {
                    $query->where('courses.name', 'LIKE', '%' . $filter['course_name'] . '%');
                }
            }
        ])->paginate();

        if (!$list) {
            return [];
        }

        $datas = [];
        foreach ($list as $item) {
            if ($item->user !== null && $item->course !== null) {
                $datas[] = $item;
            }
        }

        if (!$datas) {
            return [];
        }

        $datas = ToolService::handleCustomPagination($list, $datas);

        return $datas;
    }

    public function create($courseId, $userId)
    {
        $approval_course = $this->findByIdCourse($courseId);
        if (!$approval_course) {
            TeacherApprovalCourse::create([
                'course_id' => (int) $courseId,
                'user_id' => (int) $userId
            ]);
            return true;
        }

        return false;
    }

    public function updateStatusApproved($courseId)
    {
        $approval_course = $this->findByIdCourse($courseId);
        if ($approval_course) {
            $approval_course->status = 0;
            $approval_course->save();
            return true;
        }
        return false;
    }
}
