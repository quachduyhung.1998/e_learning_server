<?php

namespace App\Repositories\TeacherApprovalCourse;

interface TeacherApprovalCourseRepositoryInterface
{
    public function findById($id);
    
    public function findByIdCourse($courseId);

    public function getAllCourseApproval();

    public function getAllCourseApproved();

    public function getAll($filter);
    
    public function create($courseId, $userId);

    public function updateStatusApproved($courseId);
}