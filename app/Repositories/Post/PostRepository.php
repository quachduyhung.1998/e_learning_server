<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Models\Comment;
use App\Models\Like;
use App\Models\User;

class PostRepository implements PostRepositoryInterface
{
    public function findAuthorById($userId)
    {
        $user = User::find($userId);
        if (!$user) {
            return false;
        }

        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'avatar' => $user->avatar,
            'role' => $user->role
        ];
    }

    /*************** POST ***************/
    public function getAllPostWithClass($list_class_id = [])
    {
        $posts = Post::whereIn('class_id', $list_class_id)->orderBy('id', 'desc')->paginate(5);
        if (!$posts) {
            return false;
        }
        return $posts;
    }

    public function findPostById($postId)
    {
        $post = Post::find($postId);
        if (!$post) {
            return false;
        }
        return $post;
    }

    public function findPostByClassId($classId)
    {
        $post = Post::where('class_id', $classId)->first();
        if (!$post) {
            return false;
        }
        return $post;
    }

    public function createPost($attributes = [])
    {
        return Post::create([
            'user_id' => auth()->user()->id,
            'class_id' => (int) $attributes['class_id'],
            'content' => $attributes['content'],
            'status' => 1
        ]);
    }

    public function updatePost($postId, $content)
    {
        $post = $this->findPostById($postId);
        if ($post) {
            $post->update([
                'content' => $content
            ]);
            return $post;
        }
        return false;
    }
    
    public function deletePost($postId)
    {
        $post = $this->findPostById($postId);
        if ($post) {
            $post->delete();
            return true;
        }
        return false;
    }

    public function updateTotalCommentForPost($postId, $calculation)
    {
        $post = $this->findPostById($postId);
        if ($post) {
            $old_total_comment = $post->total_comment;
            if ($calculation === 'plus') {
                $post->total_comment = $old_total_comment + 1;
            } else {
                $post->total_comment = $old_total_comment != 0 ? $old_total_comment - 1 : 0;
            }
            $post->save();

            return true;
        }
        return false;
    }

    public function updateTotalLikeForPost($postId, $calculation)
    {
        $post = $this->findPostById($postId);
        if ($post) {
            $old_total_like = $post->total_like;
            if ($calculation === 'plus') {
                $post->total_like = $old_total_like + 1;
            } else {
                $post->total_like = $old_total_like != 0 ? $old_total_like - 1 : 0;
            }
            $post->save();
            
            return true;
        }
        return false;
    }
    
    /*************** COMMENT ***************/
    public function findCommentById($commentId)
    {
        $comment = Comment::find($commentId);
        if (!$comment) {
            return false;
        }
        return $comment;
    }
    
    public function findCommentByPostId($postId)
    {
        $comments = Comment::where('post_id', $postId)->get();
        if (!$comments) {
            return false;
        }
        return $comments;
    }
    
    public function getAllCommentOfPost($postId)
    {
        $comments = Comment::query()
            ->where('post_id', $postId)
            ->whereNull('comment_id')
            ->orderBy('id', 'desc')
            ->paginate(10);

        if (!$comments) {
            return false;
        }
        return $comments;
    }
    
    public function getTenCommentOfPost($postId)
    {
        $comments = Comment::query()
            ->where('post_id', $postId)
            ->whereNull('comment_id')
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();

        if (!$comments) {
            return false;
        }
        return $comments;
    }
    
    public function getAllReply($postId, $commentId)
    {
        $comments = Comment::where('post_id', $postId)->where('comment_id', $commentId)->orderBy('id', 'desc')->get();
        if (!$comments) {
            return false;
        }
        return $comments;
    }

    public function createComment($attributes = [])
    {
        $data = [
            'user_id' => auth()->user()->id,
            'post_id' => (int) $attributes['post_id'],
            'content' => $attributes['content'],
            'type' => (int) $attributes['type'],
            'status' => 1
        ];

        if (isset($attributes['comment_id'])) {
            $data['comment_id'] = (int) $attributes['comment_id'];
        }

        return Comment::create($data);
    }

    public function updateComment($commentId, $content, $type)
    {
        $comment = $this->findCommentById($commentId);
        if ($comment) {
            $comment->update([
                'content' => $content,
                'type' => (int) $type,
            ]);
            return $comment;
        }
        return false;
    }
    
    public function deleteComment($commentId)
    {
        $comment = $this->findCommentById($commentId);
        if ($comment) {
            $comment->delete();
            return true;
        }
        return false;
    }

    public function updateTotalReplyForComment($commentId, $calculation)
    {
        $comment = $this->findCommentById($commentId);
        if ($comment) {
            $old_total_reply = $comment->total_reply;
            if ($calculation === 'plus') {
                $comment->total_reply = $old_total_reply + 1;
            } else {
                $comment->total_reply = $old_total_reply != 0 ? $old_total_reply - 1 : 0;
            }
            $comment->save();
            
            return true;
        }
        return false;
    }

    public function updateTotalLikeForComment($commentId, $calculation)
    {
        $comment = $this->findCommentById($commentId);
        if ($comment) {
            $old_total_like = $comment->total_like;
            if ($calculation === 'plus') {
                $comment->total_like = $old_total_like + 1;
            } else {
                $comment->total_like = $old_total_like != 0 ? $old_total_like - 1 : 0;
            }
            $comment->save();
            
            return true;
        }
        return false;
    }
    
    /*************** LIKE ***************/
    public function findLikeById($likeId)
    {
        $like = Like::find($likeId);
        if (!$like) {
            return false;
        }
        return $like;
    }

    public function findLikeByPostId($postId)
    {
        $like = Like::query()
            ->where('user_id', auth()->user()->id)
            ->where('post_id', $postId)
            ->whereNull('comment_id')
            ->first();
        if (!$like) {
            return false;
        }
        return $like;
    }

    public function findLikeByPostIdAndCommentId($postId, $commentId)
    {
        $like = Like::query()
            ->where('user_id', auth()->user()->id)
            ->where('post_id', $postId)
            ->where('comment_id', $commentId)
            ->first();
        if (!$like) {
            return false;
        }
        return $like;
    }
    
    public function getAllLikeOfPost($postId)
    {
        $comments = Like::query()
            ->where('post_id', $postId)
            ->whereNull('comment_id')
            ->orderBy('id', 'desc')
            ->get();

        if (!$comments) {
            return false;
        }
        return $comments;
    }
    
    public function getAllLikeOfComment($postId, $commentId)
    {
        $comments = Like::query()
            ->where('post_id', $postId)
            ->where('comment_id', $commentId)
            ->orderBy('id', 'desc')
            ->get();

        if (!$comments) {
            return false;
        }
        return $comments;
    }

    public function createLike($attributes = [])
    {
        $data = [
            'user_id' => auth()->user()->id,
        ];

        if (isset($attributes['post_id'])) {
            $data['post_id'] = (int) $attributes['post_id'];
        }

        if (isset($attributes['comment_id'])) {
            $data['comment_id'] = (int) $attributes['comment_id'];
        }

        return Like::create($data);
    }
    
    public function deleteLike($likeId)
    {
        $like = $this->findLikeById($likeId);
        if ($like) {
            $like->delete();
            return true;
        }
        return false;
    }
}