<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Models\User;

interface PostRepositoryInterface
{
    public function findAuthorById($userId);

    // post
    public function getAllPostWithClass($list_class_id = []);
    
    public function findPostById($postId);

    public function findPostByClassId($classId);

    public function createPost($attributes = []);

    public function updatePost($postId, $content);
    
    public function deletePost($postId);

    public function updateTotalCommentForPost($postId, $calculation);

    public function updateTotalLikeForPost($postId, $calculation);

    // comment
    public function findCommentById($commentId);
    
    public function findCommentByPostId($postId);
    
    public function getAllCommentOfPost($postId);

    public function getTenCommentOfPost($postId);
    
    public function getAllReply($postId, $commentId);

    public function createComment($attributes = []);

    public function updateComment($commentId, $content, $type);
    
    public function deleteComment($commentId);

    public function updateTotalReplyForComment($commentId, $calculation);

    public function updateTotalLikeForComment($commentId, $calculation);
    
    // like
    public function findLikeById($likeId);

    public function findLikeByPostId($postId);

    public function findLikeByPostIdAndCommentId($postId, $commentId);
    
    public function getAllLikeOfPost($postId);
    
    public function getAllLikeOfComment($postId, $commentId);
    
    public function createLike($attributes = []);
    
    public function deleteLike($likeId);

}