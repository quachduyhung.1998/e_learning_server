<?php

namespace App\Repositories\StudentCompleteLesson;

use App\Models\StudentCompleteLesson;

class StudentCompleteLessonRepository implements StudentCompleteLessonRepositoryInterface
{
    public function findByLessonId($lessonId)
    {
        return StudentCompleteLesson::query()
            ->where('user_id', auth()->user()->id)
            ->where('lesson_id', $lessonId)
            ->first();
    }

    public function countLesonCompleteByIdUser($list_lesson_id = [])
    {
        return StudentCompleteLesson::query()
            ->where('user_id', auth()->user()->id)
            ->whereIn('lesson_id', $list_lesson_id)
            ->count();
    }
    
    public function create($lessonId)
    {
        $check_exists = $this->findByLessonId($lessonId);
        if (!$check_exists) {
            StudentCompleteLesson::create([
                'user_id' => auth()->user()->id,
                'lesson_id' => $lessonId,
            ]);
        }
    }
}