<?php

namespace App\Repositories\StudentCompleteLesson;

interface StudentCompleteLessonRepositoryInterface
{
    public function findByLessonId($lessonId);

    public function countLesonCompleteByIdUser($list_lesson_id = []);
    
    public function create($lessonId);
}