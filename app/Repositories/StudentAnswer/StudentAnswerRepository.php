<?php

namespace App\Repositories\StudentAnswer;

use App\Models\StudentAnswer;

class StudentAnswerRepository implements StudentAnswerRepositoryInterface
{
    public function findOneDetail($testId, $questionId, $number_time_worked, $userId = false)
    {
        return StudentAnswer::query()
            ->where('user_id', $userId ?: auth()->user()->id)
            ->where('test_id', $testId)
            ->where('question_id', $questionId)
            ->where('number_time_worked', $number_time_worked)
            ->first();
    }

    public function findByTestId($testId, $number_time_worked)
    {
        return StudentAnswer::query()
            ->where('user_id', auth()->user()->id)
            ->where('test_id', $testId)
            ->where('number_time_worked', $number_time_worked)
            ->get();
    }

    public function getTimeWorkedTest($testId, $userId = false)
    {
        $user_id = auth()->user()->id;
        if ($userId) {
            $user_id = (int) $userId;
        }
        $student_answer = StudentAnswer::query()
            ->where('user_id', $user_id)
            ->where('test_id', $testId)
            ->latest()
            ->first();

        if (!$student_answer) return 0;

        return $student_answer->number_time_worked;
    }

    public function create($array_record = [])
    {
        try {
            StudentAnswer::insert($array_record);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

}