<?php

namespace App\Repositories\StudentAnswer;

interface StudentAnswerRepositoryInterface
{
    public function findOneDetail($testId, $questionId, $number_time_worked, $userId = false);

    public function findByTestId($testId, $number_time_worked);

    public function getTimeWorkedTest($testId, $userId = false);

    public function create($array_record = []);
}