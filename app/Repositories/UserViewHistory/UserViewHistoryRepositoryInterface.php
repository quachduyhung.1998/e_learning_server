<?php

namespace App\Repositories\UserViewHistory;

interface UserViewHistoryRepositoryInterface
{
    public function findByIdCourse($courseId);
    
    public function findUserViewCourseToday($courseId, $userId);

    public function findUserViewLessonToday($lessonId, $userId);
    
    public function saveUserViewCourse($courseId);

    public function saveUserViewLesson($lessonId);

    public function delete($courseId, $userId);

    /** admin */
    public function countViewLesson($listCourseId = []);
}