<?php

namespace App\Repositories\UserViewHistory;

use App\Models\UserViewHistory;

class UserViewHistoryRepository implements UserViewHistoryRepositoryInterface
{
    public function findByIdCourse($courseId)
    {
        return UserViewHistory::query()
            ->where('course_id', $courseId)
            ->first();
    }
    
    public function findUserViewCourseToday($courseId, $userId)
    {
        return UserViewHistory::query()
            ->where('user_id', $userId)
            ->where('course_id', $courseId)
            ->where([
                ['created_at', '>=', date('Y-m-d 00:00:00')],
                ['created_at', '<=', date('Y-m-d 23:59:59')]
            ])
            ->first();
    }
    
    public function findUserViewLessonToday($lessonId, $userId)
    {
        return UserViewHistory::query()
            ->where('user_id', $userId)
            ->where('lesson_id', $lessonId)
            ->where([
                ['created_at', '>=', date('Y-m-d 00:00:00')],
                ['created_at', '<=', date('Y-m-d 23:59:59')]
            ])
            ->first();
    }

    public function saveUserViewCourse($courseId)
    {
        $check_exists = $this->findUserViewCourseToday($courseId, auth()->user()->id);
        if (!$check_exists) {
            UserViewHistory::create([
                'user_id' => auth()->user()->id,
                'course_id' => $courseId,
            ]);
        }
    }
    
    public function saveUserViewLesson($lessonId)
    {
        $check_exists = $this->findUserViewLessonToday($lessonId, auth()->user()->id);
        if (!$check_exists) {
            UserViewHistory::create([
                'user_id' => auth()->user()->id,
                'lesson_id' => $lessonId,
            ]);
        }
    }

    public function delete($courseId, $userId)
    {
        $approval_course = $this->findByIdCourse($courseId);
        if ($approval_course) {
            $approval_course->delete();
            return true;
        }
        return false;
    }

    /** admin */
    public function countViewLesson($listCourseId = [])
    {
        return UserViewHistory::whereIn('lesson_id', $listCourseId)->count();
    }
}