<?php

namespace App\Repositories\User;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserRepositoryInterface
{
    public function findById($userId)
    {
        return User::where('id', $userId)
            ->where('status', 1)
            ->first();
    }

    public function findByListId($listUserId)
    {
        return User::whereIn('id', $listUserId)
            ->where('status', 1)
            ->get();
    }

    public function create($attributes = [])
    {
        return User::create([
            'name' => $attributes['name'],
            'email' => $attributes['email'],
            'password' => Hash::make($attributes['password']),
            'role' => (int) $attributes['role'],
            'user_id' => auth()->user()->id,
            'status' => 1
        ]);
    }

    public function update($id, $attributes = [])
    {
        $user = $this->findById($id);
        if ($user) {
            $user->update($attributes);
            return $user;
        }
        return false;
    }

    public function updateProfile($attributes = [])
    {
        $user = $this->findById(auth()->user()->id);
        if ($user) {
            $user->update($attributes);
            return $user;
        }
        return false;
    }

    public function handleChangePermissions($userId, $role)
    {
        $user = User::where('id', $userId)->first();

        if (!$user) return false;

        return $user->update(['role' => $role]);
    }

    public function getListUser($role = null, $keyword = null)
    {
        $query = User::where('status', 1);
        if ($role !== null) {
            $query = $query->where('role', $role);
        }
        if ($keyword) {
            $query = $query->where(function($query) use ($keyword) {
                $query->where('name', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('email', 'LIKE', '%'.$keyword.'%');
            });
        }
        return $query->latest()->paginate(10);
    }

    public function getListUserByRole($role)
    {
        return User::where('role', $role)->where('status', 1)->latest()->get();
    }

    public function getRole($userId)
    {
        $user = User::where('id', $userId)
            ->where('status', 1)
            ->first();

        if ($user) {
            return $user->role;
        }

        return false;
    }

    public function delete($userId)
    {
        $user = User::findOrFail($userId);
        if ($user) {
            $user->delete();
            return true;
        }
        return false;
    }

    /********* ADMIN *********/
    public function countAllTeacherAndStudent()
    {
        $count_teacher = User::where('role', User::ROLE_TEACHER)->count();
        $count_student = User::where('role', User::ROLE_STUDENT)->count();
        return [
            'total_teacher' => $count_teacher,
            'total_student' => $count_student
        ];
    }

    public function getAdmin()
    {
        return User::where('id', 2)->first();
    }
}
