<?php

namespace App\Repositories\User;

interface UserRepositoryInterface
{
    public function findById($userId);

    public function findByListId($listUserId);

    public function create($attributes);

    public function update($id, $attributes);

    public function updateProfile($attributes = []);

    public function handleChangePermissions($userId, $role);

    public function getListUser($role = null, $keyword = null);

    public function getListUserByRole($role);

    public function getRole($userId);

    public function delete($userId);

    /********* ADMIN *********/
    public function countAllTeacherAndStudent();

    public function getAdmin();
}
