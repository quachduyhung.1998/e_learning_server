<?php

namespace App\Policies;

use App\Models\Test;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TestPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Test $test)
    {
        return $user->id === $test->lesson->course->user_id || $user->role === User::ROLE_SUPER_ADMIN;
    }

    public function delete(User $user,  Test $test)
    {
        return $user->id === $test->lesson->course->user_id || $user->role === User::ROLE_SUPER_ADMIN;
    }
}
