<?php

namespace App\Policies;

use App\Models\ChatMessage;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ChatMessagePolicy
{
    use HandlesAuthorization;

    public function update(User $user, ChatMessage $chat_message)
    {
        return $user->id === $chat_message->user_id;
    }

    public function delete(User $user, ChatMessage $chat_message)
    {
        return $user->id === $chat_message->user_id;
    }
}
