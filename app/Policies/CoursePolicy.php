<?php

namespace App\Policies;

use App\Models\Course;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class CoursePolicy
{
    use HandlesAuthorization;

    public function viewMyCourse(User $user)
    {
        // if ($user->role !== User::ROLE_TEACHER && $user->role !== User::ROLE_SUPER_ADMIN) {
        //     $this->deny([
        //         'status' => 403,
        //         'message' => 'Bạn không có quyền thực hiện hành động này!!!'
        //     ]);
        // }
        return $user->role === User::ROLE_TEACHER || $user->role === User::ROLE_SUPER_ADMIN;
    }

    public function create(User $user, Course $course)
    {
        return $user->id === $course->user_id || $user->role === User::ROLE_SUPER_ADMIN;
    }

    public function update(User $user, Course $course)
    {
        return $user->id === $course->user_id || $user->role === User::ROLE_SUPER_ADMIN;
    }

    public function delete(User $user, Course $course)
    {
        return $user->id === $course->user_id || $user->role === User::ROLE_SUPER_ADMIN;
    }
}
