<?php

namespace App\Policies;

use App\Models\Course;
use App\Models\Lesson;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LessonPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Lesson $lesson)
    {
        return $user->id === $lesson->course->user_id || $user->role === User::ROLE_SUPER_ADMIN;
    }

    public function delete(User $user, Lesson $lesson)
    {
        return $user->id === $lesson->course->user_id || $user->role === User::ROLE_SUPER_ADMIN;
    }
}
