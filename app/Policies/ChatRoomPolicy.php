<?php

namespace App\Policies;

use App\Models\ChatRoom;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ChatRoomPolicy
{
    use HandlesAuthorization;

    public function showMember(User $user, ChatRoom $chat_room)
    {
        $list_member = $chat_room->members;
        foreach ($list_member as $member) {
            if ($member->id === $user->id) {
                return true;
            }
        }
        return false;
    }

    public function create(User $user, ChatRoom $chat_room)
    {
        $list_member = $chat_room->members;
        foreach ($list_member as $member) {
            if ($member->id === $user->id) {
                return true;
            }
        }
        return false;
    }

    public function delete(User $user, ChatRoom $chat_room)
    {
        return $user->id === $chat_room->user_id;
    }
}
