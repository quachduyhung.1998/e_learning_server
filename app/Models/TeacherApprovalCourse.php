<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherApprovalCourse extends Model
{
    use HasFactory;

    const STATUS_APPROVED = 0;
    const STATUS_NOT_APPROVED = 1;
    
    protected $fillable = [
        "user_id",
        "course_id",
        "status",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }
}
