<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use PhpParser\Builder\Class_;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    const ROLE_STUDENT = 0;
    const ROLE_TEACHER = 1;
    const ROLE_ADMIN = 2;
    const ROLE_SUPER_ADMIN = 3;
    const POSITION_NAME = ['Học viên', 'Giáo viên', 'Admin', 'Super Admin'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        "name",
        "email",
        "password",
        "role",
        "user_id",
        "contact",
        "gender",
        "birthday",
        "city",
        "district",
        "wards",
        "avatar",
        "status",
        "fcm_token",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function format()
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'role' => self::POSITION_NAME[$this->role],
            'created_by' => $this->createdBy(),
            'created_at' => $this->created_at->format('H:i:s - d/m/Y'),
        ];
    }

    public function isSuperAdmin()
    {
        if ($this->role === self::ROLE_SUPER_ADMIN) {
            return true;
        }
        return false;
    }

    public function isAdmin()
    {
        if ($this->role === self::ROLE_ADMIN) {
            return true;
        }
        return false;
    }
    
    public function isTeacher()
    {
        if ($this->role === self::ROLE_TEACHER) {
            return true;
        }
        return false;
    }

    public function isStudent()
    {
        if ($this->role === self::ROLE_STUDENT) {
            return true;
        }
        return false;
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class);
    }

    public function courses()
    {
        return $this->hasMany(Course::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function classes()
    {
        return $this->hasMany(Classes::class);
    }

    public function inClasses()
    {
        return $this->belongsToMany(Classes::class, 'student_classes', 'user_id', 'class_id');
    }

    public function approvalCourse()
    {
        return $this->hasMany(TeacherApprovalCourse::class);
    }

    public function viewHistory()
    {
        return $this->hasMany(UserViewHistory::class);
    }

    public function workedTest()
    {
        return $this->belongsToMany(Test::class, 'student_answers', 'user_id', 'test_id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function rooms()
    {
        return $this->belongsToMany(ChatRoom::class, 'chat_members', 'user_id', 'chat_room_id');
    }

    public function messages()
    {
        return $this->hasMany(ChatMessage::class);
    }

    public function getListCourseId()
    {
        $list_course_id = [];
        foreach (auth()->user()->inClasses as $class) {
            foreach ($class->courses as $course) {
                $list_course_id[] = $course->id;
            }
        }

        return $list_course_id;
    }
    
    public function getListTestWorked()
    {
        $list_test_id_worked = [];
        foreach (auth()->user()->workedTest as $test) {
            $list_test_id_worked[] = $test->id;
        }

        return $list_test_id_worked;
    }
}
