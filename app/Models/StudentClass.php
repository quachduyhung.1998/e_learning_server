<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentClass extends Model
{
    use HasFactory;
    
    protected $fillable = [
        "class_id",
        "user_id",
        "status",
    ];
    
    // public function student()
    // {
    //     return $this->belongsTo(User::class);
    // }
    
    // public function class()
    // {
    //     return $this->belongsTo(Classes::class);
    // }
}
