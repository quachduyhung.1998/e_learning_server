<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    const STATUS_DRAFT = 0;
    const STATUS_COMPLETE = 1;
    const STATUS_PUBLIC = 2;

    const SEQUENCE = 1;
    const NOT_SEQUENCE = 0;

    protected $fillable = [
        "name",
        "slug",
        "description",
        "image",
        "course_category_id",
        "user_id",
        "total_lesson",
        "is_sequence",
        "status",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(CourseCategory::class, 'course_category_id', 'id');
    }
    
    public function lessons()
    {
        return $this->hasMany(Lesson::class);
    }
    
    public function tests()
    {
        return $this->hasMany(Test::class);
    }

    public function classes()
    {
        return $this->belongsToMany(Classes::class, 'class_assigned_courses', 'course_id', 'class_id');
    }

    public function teacherapproval()
    {
        return $this->belongsToMany(User::class, 'teacher_approval_courses', 'course_id', 'user_id');
    }

    public function format($course, $percent_progress = 0)
    {
        $approver = null;
        foreach ($course->teacherapproval as $teacher) {
            $approver = [
                'id' => $teacher->id,
                'name' => $teacher->name,
                'email' => $teacher->email,
                'avatar' => $teacher->avatar,
                'role' => $teacher->role,
            ];
        }

        return [
            'id' => $course->id,
            'name' => $course->name,
            'slug' => $course->slug,
            'description' => $course->description ?? '',
            'image' => $course->image ?? '',
            'total_lesson' => $course->total_lesson,
            'is_sequence' => $course->is_sequence,
            'course_category_id' => $course->course_category_id,
            'course_category' => $course->category->name,
            'percent_progress' => $percent_progress,
            'created_by' => [
                'id' => $course->user->id,
                'name' => $course->user->name,
                'avatar' => $course->user->avatar
            ],
            'approver' => $approver,
            'status' => $course->status, 
            'created_at' => $course->created_at,
            'updated_at' => $course->updated_at,
        ];
    }
}
