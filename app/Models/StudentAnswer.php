<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentAnswer extends Model
{
    use HasFactory;

    const IS_WRONG = 0;
    const IS_RIGHT = 1;

    protected $fillable = [
        "user_id",
        "test_id",
        "question_id",
        "answers",
        'is_right',
        'mark_achieved',
        'number_time_worked',
        "status",
    ];

    public function student()
    {
        return $this->belongsTo(User::class);
    }

    public function test()
    {
        return $this->belongsTo(Test::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
