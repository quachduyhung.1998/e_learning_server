<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "user_id",
        "course_category_id",
        "type_question",
        "mark",
        "answers",
        "status",
    ];
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function category()
    {
        return $this->belongsTo(CourseCategory::class);
    }
    
    public function format($question)
    {
        return [
            'id' => $question->id,
            'name' => $question->name,
            'course_category_id' => $question->course_category_id,
            'type_question' => $question->type_question,
            'answers' => json_decode($question->answers, true),
            'mark' => $question->mark,
            'created_at' => $question->created_at,
            'updated_at' => $question->updated_at,
        ];
    }
}
