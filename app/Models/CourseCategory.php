<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseCategory extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "slug",
        "description",       
        "course_category_id",
        "status",
    ];

    public function childrenCategories()
    {
        return $this->hasMany(CourseCategory::class);
    }

    public function courses()
    {
        return $this->hasMany(Course::class);
    }
    
    public function questions()
    {
        return $this->hasMany(Question::class);
    }
}
