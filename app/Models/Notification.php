<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;
    
    const TYPE_COURSE = 1;
    const TYPE_CLASS = 2;
    const TYPE_TEST = 3;
    const TYPE_POST = 4;
    const TYPE_COURSE_PENDING_APPORVAL = 5;

    const STATUS_UNREAD = 0;
    const STATUS_READED = 1;

    protected $fillable = [
        "user_id",
        "type",
        "content_id",
        "message",
        "url",
        "status",
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
