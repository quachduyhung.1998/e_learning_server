<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserViewStatiscs extends Model
{
    use HasFactory;
    
    protected $fillable = [
        "date",
        "view",
    ];
}
