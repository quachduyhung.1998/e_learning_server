<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;

    protected $fillable = [
        "name",
        "slug",
        "description",   
        "lesson_id",     
        "course_id",     
        "list_question_id",
        "total_question",
        "total_mark",    
        "time_work",     
        "is_required",   
        "status",
        "min_mark_achieved",
        "time_start",
        "time_end"
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }
}
