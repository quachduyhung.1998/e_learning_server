<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TestResult extends Model
{
    use HasFactory;

    protected $fillable = [
        "user_id",
        "test_id",
        "mark_achieved",
        'total_answer_right',
        'number_time_worked',
        "time_start",
        "time_finish",
        "total_time_work",
    ];
}