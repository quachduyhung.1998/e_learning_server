<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    use HasFactory;

    const TYPE_NORMAL = 0;
    const TYPE_VIDEO = 1;

    protected $fillable = [
        "name",
        "slug",
        "description",
        "image",
        "content",
        "order",
        "course_id",
        "status",
        "type"
    ];

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function test()
    {
        return $this->hasOne(Test::class);
    }
}
