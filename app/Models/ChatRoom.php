<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatRoom extends Model
{
    use HasFactory;
    
    const ROOM_TYPE_PRIVATE = 0; // chat 1 vs 1
    const ROOM_TYPE_GROUP = 1; // chat nhóm
    
    protected $fillable = [
        "name",
        "type",
        "user_id",
        "total_member",
        'list_user_deleted'
    ];

    public function administrator()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    // public function members()
    // {
    //     return $this->hasMany(ChatMember::class, 'id', 'chat_room_id');
    // }
    
    public function messages()
    {
        return $this->hasMany(ChatMessage::class);
    }
    
    public function members()
    {
        return $this->belongsToMany(User::class, 'chat_members', 'chat_room_id', 'user_id');
    }
}
