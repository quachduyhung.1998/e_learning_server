<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    use HasFactory;
    
    protected $fillable = [
        "name",
        "slug",
        "user_id",
        "total_student",
        "status",
    ];
    
    public function teacher()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    public function students()
    {
        return $this->belongsToMany(User::class, 'student_classes', 'class_id', 'user_id');
    }
    
    // public function studentInClass()
    // {
    //     return $this->hasMany(StudentClass::class);
    // }

    public function courses()
    {
        return $this->belongsToMany(Course::class, 'class_assigned_courses', 'class_id', 'course_id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'class_id', 'id');
    }

    public function format($class, $teacher)
    {
        return [
            'id' => $class->id,
            'name' => $class->name,
            'slug' => $class->slug,
            'total_student' => $class->total_student,
            'teacher' => [
                'id' => $teacher->id,
                'name' => $teacher->name,
                'avatar' => $teacher->avatar
            ],
            'status' => $class->status, 
            'created_at' => $class->created_at,
            'updated_at' => $class->updated_at,
        ];
    }

}
