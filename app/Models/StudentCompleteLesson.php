<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentCompleteLesson extends Model
{
    use HasFactory;
    
    protected $fillable = [
        "user_id",
        "lesson_id",
        "status",
    ];

    public function student()
    {
        return $this->belongsTo(User::class);
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class);
    }
}
