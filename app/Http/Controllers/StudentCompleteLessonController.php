<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentCompleteLessonRequest;
use App\Models\Course;
use App\Models\StudentProgressCourse;
use App\Repositories\Lesson\LessonRepositoryInterface;
use App\Repositories\StudentCompleteLesson\StudentCompleteLessonRepositoryInterface;
use Illuminate\Http\Request;

class StudentCompleteLessonController extends Controller
{
    private $studentCompleteLessonRepository;
    private $lessonRepository;

    public function __construct(StudentCompleteLessonRepositoryInterface $studentCompleteLessonRepository, LessonRepositoryInterface $lessonRepository)
    {
        $this->studentCompleteLessonRepository = $studentCompleteLessonRepository;
        $this->lessonRepository = $lessonRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentCompleteLessonRequest $request)
    {
        $lesson_id = $request->input('lesson_id');

        $lesson = $this->lessonRepository->findById($lesson_id);
        if (!$lesson) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài học này không có trên hệ thống'
            ], 404);
        }
                
        // khóa học bắt học lần lượt
        $course = $lesson->course;
        if ($course->is_sequence === Course::SEQUENCE) {
            // kiểm tra bài học có bài kiểm tra nào không
            $test = $lesson->test;
            if ($test) {
                $list_test_id_worked = request()->user()->getListTestWorked();
                if (!in_array($test->id, $list_test_id_worked)) {
                    return response()->json([
                        'status' => 403,
                        'message' => 'Bạn chưa làm bài tập/bài kiểm tra trong bài học này'
                    ], 403);
                }
            }
        }

        $this->studentCompleteLessonRepository->create($lesson_id);

        // update tiến trình học của học viên trong khóa học
        $total_lesson_in_course = (int) $course->total_lesson;

        $list_lesson_id = [];
        foreach ($course->lessons as $item) {
            $list_lesson_id[] = $item->id;
        }

        $total_lesson_complete = (int) $this->studentCompleteLessonRepository->countLesonCompleteByIdUser($list_lesson_id);
        $percent_complete = $total_lesson_complete / $total_lesson_in_course * 100;
        $student_progress_exists = StudentProgressCourse::query()
            ->where('user_id', auth()->user()->id)
            ->where('course_id', $course->id)
            ->first();
        if ($student_progress_exists) {
            $student_progress_exists->update([
                'progress' => $percent_complete
            ]);
        } else {
            StudentProgressCourse::create([
                'user_id' => auth()->user()->id,
                'course_id' => $course->id,
                'progress' => $percent_complete
            ]);
        }

        $next_lesson = null;
        $order_current = $lesson->order;
        if ($order_current != null) {
            $next_lesson = $course->lessons()->where('order', '>', $order_current)->orderBy('order', 'asc')->take(1)->get();
        } else {
            $next_lesson = $course->lessons()->where('id', '>', $lesson->id)->orderBy('order', 'asc')->take(1)->get(); 
        }

        return response()->json([
            'status' => 200,
            'data' => [
                'next_lesson_id' => count($next_lesson) == 0 ? null : $next_lesson[0]->id
            ]
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
