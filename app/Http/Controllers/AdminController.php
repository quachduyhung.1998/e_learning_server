<?php

namespace App\Http\Controllers;

use App\Models\UserViewHistory;
use App\Models\UserViewStatiscs;
use App\Repositories\Classes\ClassRepositoryInterface;
use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\UserViewHistory\UserViewHistoryRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    private $courseRepository;
    private $classRepository;
    private $userRepository;
    private $userViewHistoryRepository;

    public function __construct(
        CourseRepositoryInterface $courseRepository,
        ClassRepositoryInterface $classRepository,
        UserRepositoryInterface $userRepository,
        UserViewHistoryRepositoryInterface $userViewHistoryRepository
    ) {
        $this->courseRepository = $courseRepository;
        $this->classRepository = $classRepository;
        $this->userRepository = $userRepository;
        $this->userViewHistoryRepository = $userViewHistoryRepository;
    }

    public function dashboard(Request $request)
    {
        $total_course = $total_class = $total_teacher = $total_student = 0;

        $total_course = $this->courseRepository->countAll();
        $total_class = $this->classRepository->countAll();
        $count_user = $this->userRepository->countAllTeacherAndStudent();
        $total_teacher = $count_user['total_teacher'];
        $total_student = $count_user['total_student'];

        // thống kê lượt truy cập theo thời gian
        $statisc = [];

        $case = $request->input('type') !== null ? strtoupper($request->input('type')) : '';
        $start = $request->input('startDate');
        $end = $request->input('endDate');
        switch ($case) {
            case 'DAY':
                $statisc = UserViewStatiscs::select(
                    DB::raw("(DATE_FORMAT(date, '%d-%m-%Y')) as time"),
                    'view as value'
                )
                    ->whereBetween('date', [$start, $end])
                    ->orderBy('date')
                    ->get();

                // for ($i = strtotime($start); $i <= strtotime($end); $i+86400) {
                //     $exists = false;
                //     foreach ($data as $item) {
                //         $exists = true;
                //         if ($item['time'] == date('d-m-Y', strtotime($i))) {
                //             $statisc[] = [
                //                 'time' => $item['time'],
                //                 'value' => $item['value']
                //             ];
                //         }
                //         break;
                //     }
                //     if (!$exists) {
                //         $statisc[] = [
                //             'time' => date('d-m-Y', strtotime($i)),
                //             'value' => 0
                //         ];
                //     }
                // }
                break;
            case 'MONTH':
                $statisc = UserViewStatiscs::select(
                    DB::raw("(DATE_FORMAT(date, '%m-%Y')) as time"),
                    DB::raw("(SUM(view)) as value")
                )
                    ->whereBetween('date', [$start . '-01', $end . '-31'])
                    ->orderBy('date')
                    ->groupBy(DB::raw("DATE_FORMAT(date, '%Y-%m')"))
                    ->get();
                break;
            case 'YEAR':
                $statisc = UserViewStatiscs::select(
                    DB::raw("(DATE_FORMAT(date, '%Y')) as time"),
                    DB::raw("(SUM(view)) as value")
                )
                    ->whereBetween('date', [$start . '-01-01', $end . '12-31'])
                    ->orderBy('date')
                    ->groupBy(DB::raw("DATE_FORMAT(date, '%Y')"))
                    ->get();
                break;
            default:
                $last_6_day = Carbon::now()->subDays(6)->format('Y-m-d');
                $today = date('Y-m-d');
                $data = UserViewHistory::query()
                    ->whereBetween('created_at', [$last_6_day, $today])
                    ->orderBy('created_at')
                    ->get()
                    ->groupBy(function ($value) {
                        return Carbon::parse($value->created_at)->format('d-m-Y');
                    });

                $list_date = [
                    Carbon::now()->subDays(6)->format('d-m-Y'),
                    Carbon::now()->subDays(5)->format('d-m-Y'),
                    Carbon::now()->subDays(4)->format('d-m-Y'),
                    Carbon::now()->subDays(3)->format('d-m-Y'),
                    Carbon::now()->subDays(2)->format('d-m-Y'),
                    Carbon::now()->subDays(1)->format('d-m-Y'),
                    Carbon::now()->format('d-m-Y'),
                ];
                foreach ($list_date as $date) {
                    $exists = false;
                    foreach ($data as $key => $value) {
                        if ($date == $key) {
                            $exists = true;
                            $list_unique_user_id = [];
                            foreach ($value as $item) {
                                if (!in_array($item->user_id, $list_unique_user_id)) {
                                    $list_unique_user_id[] = $item->user_id;
                                }
                            }
                            $statisc[] = [
                                'time' => $date,
                                'value' => count($list_unique_user_id)
                            ];
                            break;
                        }
                    }
                    if (!$exists) {
                        $statisc[] = [
                            'time' => $date,
                            'value' => 0
                        ];
                    }
                }
                break;
        }

        // top 10 khóa học có nhiều lượt học nhất
        $list_course_statisc = [];
        $courses = $this->courseRepository->getAll();
        foreach ($courses as $course) {
            $lessons = $course->lessons;
            $list_lesson_id = [];
            foreach ($lessons as $lesson) {
                $list_lesson_id[] = $lesson->id;
            }

            $total_student_learn = $this->userViewHistoryRepository->countViewLesson($list_lesson_id);
            if ($total_student_learn > 0) {
                $list_course_statisc[] = [
                    'id' => $course->id,
                    'name' => $course->name,
                    'description' => $course->description ?? '',
                    'image' => $course->image ?? '',
                    'total_lesson' => $course->total_lesson,
                    'is_sequence' => $course->is_sequence,
                    'course_category_id' => $course->course_category_id,
                    'course_category' => $course->category->name,
                    'created_by' => [
                        'id' => $course->user->id,
                        'name' => $course->user->name,
                        'email' => $course->user->email,
                        'avatar' => $course->user->avatar,
                        'role' => $course->user->role,
                    ],
                    'status' => $course->status, 
                    'created_at' => $course->created_at,
                    'updated_at' => $course->updated_at,
                    'value' => $total_student_learn
                ];
            }
        }

        $value = array_column($list_course_statisc, 'value');
        array_multisort($value, SORT_DESC, $list_course_statisc);

        return response()->json([
            'status' => 200,
            'data' => [
                'total_course' => $total_course,
                'total_class' => $total_class,
                'total_teacher' => $total_teacher,
                'total_student' => $total_student,
                'statisc' => $statisc,
                'course_most_popular' => $list_course_statisc
            ]
        ], 200);
    }
}