<?php

namespace App\Http\Controllers;

use App\Models\StudentClass;
use App\Models\StudentProgressCourse;
use App\Models\User;
use App\Repositories\Course\CourseRepositoryInterface;
use Illuminate\Http\Request;

class StudentProgressCourseController extends Controller
{
    private $courseRepository;

    public function __construct(CourseRepositoryInterface $courseRepository)
    {
        $this->courseRepository = $courseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($course_id, Request $request)
    {
        $course = $this->courseRepository->findById($course_id);
        if (!$course) {
            return response()->json([
                'status' => 404,
                'message' => 'Khóa học này không có trên hệ thống'
            ], 404);
        }

        // kiểm tra xem giáo viên có quản lý lớp này hay không
        if (request()->user()->isTeacher()) {
            $classes = $course->classes;
            $permisson = false;
            foreach ($classes as $class) {
                if (auth()->user()->id === $class->user_id) {
                    $permisson = true;
                    break;
                }
            }

            if (!$permisson) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không có quyển thực hiện hành động này'
                ], 403);
            }
        }

        $list_user_exists = false;
        $list_user_id = [];
        if (isset($request['class_id']) && $request['class_id'] != '') {
            $list_user_exists = true;
            $list_user_in_class = StudentClass::select(['user_id'])->where('class_id', $request['class_id'])->get();
            foreach ($list_user_in_class as $item) {
                $list_user_id[] = $item->user_id;
            }
        }

        $data = [];

        $query = StudentProgressCourse::where('course_id', $course_id);
        if ($list_user_exists) {
            $query->whereIn('user_id', $list_user_id);
        }

        $list_user_progress = $query->orderBy('id', 'desc')->get();

        foreach ($list_user_progress as $item) {
            $user = $item->user;
            $data_item = [
                'user_id' => $item->user_id,
                'name' => $user->name,
                'email' => $user->email,
                'role' => User::POSITION_NAME[$user->role],
                'progress' => $item->progress
            ];

            $data[] = $data_item;
        }

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
