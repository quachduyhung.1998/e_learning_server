<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Http\Requests\PostRequest;
use App\Models\Like;
use App\Models\Notification;
use App\Repositories\Classes\ClassRepositoryInterface;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\Post\PostRepositoryInterface;
use App\Services\NotificationService;
use App\Services\ToolService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class PostController extends Controller
{
    private $postRepository;
    private $classRepository;
    private $notificationRepository;

    public function __construct(
        PostRepositoryInterface $postRepository,
        ClassRepositoryInterface $classRepository,
        NotificationRepositoryInterface $notificationRepository
    )
    {
        $this->postRepository = $postRepository;
        $this->classRepository = $classRepository;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllPost()
    {
        $list_class = false;

        if (request()->user()->isSuperAdmin() || request()->user()->isAdmin()) {
            $list_class = $this->classRepository->getAllClass();
        }
        elseif (request()->user()->isTeacher()) {
            $list_class = auth()->user()->classes;
        }
        elseif (request()->user()->isStudent()) {
            $list_class = auth()->user()->inClasses;
        }

        if (!$list_class) {
            return response()->json([
                'status' => 200,
                'data' => []
            ], 200);
        }

        $list_class_id = [];
        foreach ($list_class as $class) {
            $list_class_id[] = $class->id;
        }

        $posts = $this->postRepository->getAllPostWithClass($list_class_id);
        $data = $posts;

        $list_post = [];
        foreach ($posts as $post) {
            $detail_post = $post;

            // get author of post
            $author_post = $this->postRepository->findAuthorById($post->user_id);
            $detail_post['created_by'] = $author_post;
            unset($detail_post['user_id']);

            // get comment (lấy 10 comment gần đây)
            $last_10_comment = [];
            $comments = $this->postRepository->getTenCommentOfPost($post->id);
            foreach ($comments as $comment) {
                $detail_comment = $comment;

                // get author of comment
                $author_comment = $this->postRepository->findAuthorById($comment->user_id);
                $detail_comment['created_by'] = $author_comment;
                unset($detail_comment['user_id']);
                unset($detail_comment['comment_id']);

                // get all reply
                $list_reply = [];
                $all_reply = $this->postRepository->getAllReply($post->id, $comment->id);
                foreach ($all_reply as $reply) {
                    $detail_reply = $reply;

                    // get author of reply
                    $author_reply = $this->postRepository->findAuthorById($reply->user_id);
                    $detail_reply['created_by'] = $author_reply;
                    unset($detail_reply['user_id']);
                    unset($detail_reply['comment_id']);

                    $list_reply[] = $detail_reply;
                }

                $detail_comment['list_reply'] = $list_reply;
                $last_10_comment[] = $detail_comment;
            }

            $detail_post['last_10_comment'] = $last_10_comment;

            $list_post[] = $detail_post;
        }

        // $data['data'] = $list_post;

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllPostInClass($class_id)
    {
        $class = $this->classRepository->findById($class_id);
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học không tồn tại trên hệ thống'
            ], 404);
        }

        if (request()->user()->isTeacher() && auth()->user()->id !== $class->user_id) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không phải là giáo viên quản lý lớp học này'
            ], 403);
        }

        if (request()->user()->isStudent()) {
            $check_student_in_class = false;
            $list_class = auth()->user()->inClasses;
            foreach ($list_class as $item) {
                if ($item->id == $class_id) {
                    $check_student_in_class = true;
                    break;
                }
            }

            if (!$check_student_in_class) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không phải là học sinh trong lớp học này'
                ], 403);
            }
        }

        $data = [];
        foreach ($class->posts as $post) {
            $detail_post = $post;

            // check your like post
            $detail_post['liked'] = false;
            $like_post_exists = Like::query()
                ->where('user_id', auth()->user()->id)
                ->where('post_id', $post->id)
                ->whereNull('comment_id')
                ->first();
            if ($like_post_exists) {
                $detail_post['liked'] = true;
            }

            // get author of post
            $author_post = $this->postRepository->findAuthorById($post->user_id);
            $detail_post['created_by'] = $author_post;
            unset($detail_post['user_id']);

            // get comment (lấy 10 comment gần đây)
            $last_10_comment = [];
            $comments = $this->postRepository->getTenCommentOfPost($post->id);
            foreach ($comments as $comment) {
                $detail_comment = $comment;

                // check your like comment
                $detail_comment['liked'] = false;
                $like_comment_exists = Like::query()
                    ->where('user_id', auth()->user()->id)
                    ->where('post_id', $post->id)
                    ->where('comment_id', $comment->id)
                    ->first();
                if ($like_comment_exists) {
                    $detail_comment['liked'] = true;
                }

                // get author of comment
                $author_comment = $this->postRepository->findAuthorById($comment->user_id);
                $detail_comment['created_by'] = $author_comment;
                unset($detail_comment['user_id']);
                unset($detail_comment['comment_id']);

                // get all reply
                $list_reply = [];
                $all_reply = $this->postRepository->getAllReply($post->id, $comment->id);
                foreach ($all_reply as $reply) {
                    $detail_reply = $reply;

                    // check your like reply
                    $detail_reply['liked'] = false;
                    $like_reply_exists = Like::query()
                        ->where('user_id', auth()->user()->id)
                        ->where('post_id', $post->id)
                        ->where('comment_id', $reply->id)
                        ->first();
                    if ($like_reply_exists) {
                        $detail_reply['liked'] = true;
                    }

                    // get author of reply
                    $author_reply = $this->postRepository->findAuthorById($reply->user_id);
                    $detail_reply['created_by'] = $author_reply;
                    unset($detail_reply['user_id']);
                    unset($detail_reply['comment_id']);

                    $list_reply[] = $detail_reply;
                }

                $detail_comment['list_reply'] = $list_reply;
                $last_10_comment[] = $detail_comment;
            }

            $detail_post['last_10_comment'] = $last_10_comment;

            $data[] = $detail_post;
        }

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storePost(PostRequest $request)
    {
        $class = $this->classRepository->findById($request->class_id);
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học này không tồn tại trên hệ thống'
            ], 404);
        }

        if (request()->user()->isTeacher() && auth()->user()->id !== $class->user_id) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không phải là giáo viên quản lý lớp học này'
            ], 403);
        }

        if (request()->user()->isStudent()) {
            $check_student_in_class = false;
            $list_class = auth()->user()->inClasses;
            foreach ($list_class as $item) {
                if ($item->id == $request->class_id) {
                    $check_student_in_class = true;
                    break;
                }
            }

            if (!$check_student_in_class) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không phải là học sinh trong lớp học này'
                ], 403);
            }
        }

        $attr = [
            'class_id' => (int) $request->input('class_id'),
            'content' => $request->input('content'),
        ];
        $post = $this->postRepository->createPost($attr);
        if (!$post) {
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }

        $post = $this->postRepository->findPostById($post->id);
        $post['created_by'] = $this->postRepository->findAuthorById($post->user_id);
        unset($post->user_id);

        // send notif
        $body = '"'.auth()->user()->name.'" đã đăng một bài viết mới trong "'.$class->name.'": '.$request->input('content');
        // $list_user_id = [];
        // $list_token_fcm = [];
        // nếu là không phải là teacher thì gửi thông báo cho teacher
        if (!request()->user()->isTeacher()) {
            if ($class->teacher) {
                // $list_token_fcm[] = $class->teacher->fcm_token;
                // $list_user_id[] = $class->teacher->id;
                $notif = $this->notificationRepository->create([
                    'user_id' => $class->teacher->id,
                    'type' => Notification::TYPE_CLASS,
                    'content_id' => $class['id'],
                    'message' => $body,
                    'url' => '/post?classId='.$class->id
                ]);
                if (isset($class->teacher->fcm_token) && $class->teacher->fcm_token != '' && $class->teacher->fcm_token !== null) {
                    NotificationService::sendOne($class->teacher->fcm_token, [
                        'title' => "E-learning thông báo",
                        'body' => ToolService::subWord($body),
                        'icon' => 'https://hdev.info/images/favicon.png',
                        'click_action' => 'https://e-learning.hdev.info/post?classId='.$class->id,
                        'notification_id' => $notif ? $notif->id : false
                    ]);
                }
            }
        }
        $students = $class->students;
        if ($students) {
            foreach ($students as $student) {
                if ($student && $student->id !== auth()->user()->id) {
                    // $list_token_fcm[] = $student->fcm_token;
                    // $list_user_id[] = $student->id;
                    $notif = $this->notificationRepository->create([
                        'user_id' => $student->id,
                        'type' => Notification::TYPE_CLASS,
                        'content_id' => $class['id'],
                        'message' => $body,
                        'url' => '/post?classId='.$class->id
                    ]);
                    if (isset($student->fcm_token) && $student->fcm_token != '' && $student->fcm_token !== null) {
                        NotificationService::sendOne($student->fcm_token, [
                            'title' => "E-learning thông báo",
                            'body' => ToolService::subWord($body),
                            'icon' => 'https://hdev.info/images/favicon.png',
                            'click_action' => 'https://e-learning.hdev.info/post?classId='.$class->id,
                            'notification_id' => $notif ? $notif->id : false
                        ]);
                    }
                }
            }
        }
        // if (!empty($list_token_fcm)) {
        //     $body = '"'.auth()->user()->name.'" đã đăng một bài viết mới trong "'.$class->name.'": '.$request->input('content');
        //     NotificationService::sendMulti($list_token_fcm, [
        //         'title' => "E-learning thông báo",
        //         'body' => ToolService::subWord($body),
        //         'icon' => 'https://hdev.info/images/favicon.png',
        //         'click_action' => 'https://e-learning.hdev.info/'
        //     ]);
        //     $this->notificationRepository->createMany($list_user_id, [
        //         'type' => Notification::TYPE_POST,
        //         'content_id' => $post->id,
        //         'message' => $body
        //     ]);
        // }

        return response()->json([
            'status' => 201,
            'message' => 'Đăng bài viết thành công',
            'data' => $post
        ], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $post_id
     * @return \Illuminate\Http\Response
     */
    public function updatePost($post_id, Request $request)
    {
        $fields = $request->validate(
            [
                'content' => 'required'
            ],
            [
                'content.required' => 'Bạn chưa nhập nội dung cho bài viết',
            ]
        );

        $post = $this->postRepository->findPostById($post_id);
        if (!$post) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài viết này không tồn tại trên hệ thống'
            ], 404);
        }

        $response = Gate::inspect('update', $post);
        if ($response->allowed()) {
            $post = $this->postRepository->updatePost($post_id, $fields['content']);
            if (!$post) {
                return response()->json([
                    'status' => 500,
                    'message' => 'Lỗi máy chủ'
                ], 500);
            }

            $post['created_by'] = $this->postRepository->findAuthorById($post->user_id);
            unset($post->user_id);

            return response()->json([
                'status' => 200,
                'message' => 'Cập nhật bài viết thành công',
                'data' => $post
            ], 200);
        }

        return response()->json([
            'status' => 403,
            'message' => 'Đây không phải là bài viết của bạn'
        ], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $post_id
     * @return \Illuminate\Http\Response
     */
    public function destroyPost($post_id)
    {
        $post = $this->postRepository->findPostById($post_id);
        if (!$post) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài viết này không tồn tại trên hệ thống'
            ], 404);
        }

        $teacher = $post->class->teacher;
        $response = Gate::inspect('delete', $post);
        if ($response->allowed() || auth()->user()->id === $teacher->id) {
            $post_delete = $this->postRepository->deletePost($post_id);
            if (!$post_delete) {
                return response()->json([
                    'status' => 500,
                    'message' => 'Lỗi máy chủ'
                ], 500);
            }

            // send notif
            if (auth()->user()->id != $post->user_id) {
                $author = $post->user;
                if ($author) {
                    $body = '"'.auth()->user()->name.'" vừa xóa một bài viết của bạn trong "'.$post->class->name.'": '.$post->content;
                    $notif = $this->notificationRepository->create([
                        'user_id' => $author->id,
                        'type' => Notification::TYPE_CLASS,
                        'content_id' => $post->id,
                        'message' => $body,
                        'url' => '/post?classId='.$post->class->id
                    ]);
                    if (isset($author->fcm_token) && $author->fcm_token != '' && $author->fcm_token !== null) {
                        NotificationService::sendOne($author->fcm_token, [
                            'title' => "E-learning thông báo",
                            'body' => ToolService::subWord($body),
                            'icon' => 'https://hdev.info/images/favicon.png',
                            'click_action' => 'https://e-learning.hdev.info/post?classId='.$post->class->id,
                            'notification_id' => $notif ? $notif->id : false
                        ]);
                    }
                }
            }

            return response()->json([
                'status' => 200,
                'message' => 'Xóa bài viết thành công'
            ], 200);
        }

        return response()->json([
            'status' => 403,
            'message' => 'Đây không phải là bài viết của bạn'
        ], 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $post_id
     * @return \Illuminate\Http\Response
     */
    public function showPost($post_id)
    {
        $post = $this->postRepository->findPostById($post_id);
        if (!$post) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài viết này không tồn tại trên hệ thống'
            ], 404);
        }

        $class = $post->class;
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học này không tồn tại trên hệ thống'
            ], 404);
        }

        if (request()->user()->isTeacher() && auth()->user()->id !== $class->user_id) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không phải là giáo viên quản lý lớp học này'
            ], 403);
        }

        if (request()->user()->isStudent()) {
            $check_student_in_class = false;
            $list_class = auth()->user()->inClasses;
            foreach ($list_class as $item) {
                if ($item->id == $class->id) {
                    $check_student_in_class = true;
                    break;
                }
            }

            if (!$check_student_in_class) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không phải là học sinh trong lớp học này'
                ], 403);
            }
        }

        $data = $post;
        unset($data['class']);

        // check your like post
        $data['liked'] = false;
        $like_post_exists = Like::query()
            ->where('user_id', auth()->user()->id)
            ->where('post_id', $post->id)
            ->whereNull('comment_id')
            ->first();
        if ($like_post_exists) {
            $data['liked'] = true;
        }

        // get author of post
        $author_post = $this->postRepository->findAuthorById($post->user_id);
        $data['created_by'] = $author_post;
        unset($data['user_id']);

        // get comment (lấy 10 comment gần đây)
        $last_10_comment = [];
        $comments = $this->postRepository->getTenCommentOfPost($post->id);
        foreach ($comments as $comment) {
            $detail_comment = $comment;

            // check your like comment
            $detail_comment['liked'] = false;
            $like_comment_exists = Like::query()
                ->where('user_id', auth()->user()->id)
                ->where('post_id', $post->id)
                ->where('comment_id', $comment->id)
                ->first();
            if ($like_comment_exists) {
                $detail_comment['liked'] = true;
            }

            // get author of comment
            $author_comment = $this->postRepository->findAuthorById($comment->user_id);
            $detail_comment['created_by'] = $author_comment;
            unset($detail_comment['user_id']);
            unset($detail_comment['comment_id']);

            // get all reply
            $list_reply = [];
            $all_reply = $this->postRepository->getAllReply($post->id, $comment->id);
            foreach ($all_reply as $reply) {
                $detail_reply = $reply;

                // check your like comment
                $detail_reply['liked'] = false;
                $like_reply_exists = Like::query()
                    ->where('user_id', auth()->user()->id)
                    ->where('post_id', $post->id)
                    ->where('comment_id', $reply->id)
                    ->first();
                if ($like_reply_exists) {
                    $detail_reply['liked'] = true;
                }

                // get author of reply
                $author_reply = $this->postRepository->findAuthorById($reply->user_id);
                $detail_reply['created_by'] = $author_reply;
                unset($detail_reply['user_id']);
                unset($detail_reply['comment_id']);

                $list_reply[] = $detail_reply;
            }

            $detail_comment['list_reply'] = $list_reply;
            $last_10_comment[] = $detail_comment;
        }

        $data['last_10_comment'] = $last_10_comment;

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeComment(CommentRequest $request)
    {
        $post = $this->postRepository->findPostById($request->input('post_id'));
        if (!$post) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài viết này không tồn tại trên hệ thống'
            ], 404);
        }

        $class = $post->class;
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học này không tồn tại trên hệ thống'
            ], 404);
        }

        if (request()->user()->isTeacher() && auth()->user()->id !== $class->user_id) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không phải là giáo viên quản lý lớp học này'
            ], 403);
        }

        if (request()->user()->isStudent()) {
            $check_student_in_class = false;
            $list_class = auth()->user()->inClasses;
            foreach ($list_class as $item) {
                if ($item->id == $class->id) {
                    $check_student_in_class = true;
                    break;
                }
            }

            if (!$check_student_in_class) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không phải là học sinh trong lớp học này'
                ], 403);
            }
        }

        $attr = [
            'post_id' => (int) $request->input('post_id'),
            'content' => $request->input('content'),
            'type' => $request->input('type'),
        ];

        $reply_comemnt = false;
        if ($request->input('comment_id') && $request->input('comment_id') != '') {
            $reply_comemnt = $this->postRepository->findCommentById($request->input('comment_id'));
            if (!$reply_comemnt) {
                return response()->json([
                    'status' => 404,
                    'message' => 'Bình luận bạn trả lời không tồn tại trên hệ thống'
                ], 404);
            }
            $attr['comment_id'] = (int) $request->input('comment_id');
        }

        $comment = $this->postRepository->createComment($attr);
        if (!$comment) {
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }

        // update total comment in post
        $this->postRepository->updateTotalCommentForPost($post->id, 'plus');
        // update total comment for comment
        if ($reply_comemnt) {
            $this->postRepository->updateTotalReplyForComment($reply_comemnt->id, 'plus');
        }

        // send notif
        $author = $post->user;
        if ($author && $author->id !== auth()->user()->id) {
            $body = '"'.auth()->user()->name.'" đã bình luận về bài viết của bạn trong "'.$class->name.'": '.$request->input('content');
            if ($reply_comemnt && $reply_comemnt->user) {
                if ($reply_comemnt->user->id !== $author->id) {
                    $notif = $this->notificationRepository->create([
                        'user_id' => $author->id,
                        'type' => Notification::TYPE_CLASS,
                        'content_id' => $post->id,
                        'message' => $body,
                        'url' => '/post?classId='.$class->id
                    ]);
                    if (isset($author->fcm_token) && $author->fcm_token != '' && $author->fcm_token !== null) {
                        NotificationService::sendOne($author->fcm_token, [
                            'title' => "E-learning thông báo",
                            'body' => ToolService::subWord($body),
                            'icon' => 'https://hdev.info/images/favicon.png',
                            'click_action' => 'https://e-learning.hdev.info/post?classId='.$class->id,
                            'notification_id' => $notif ? $notif->id : false
                        ]);
                    }
                }
            } else {
                $notif = $this->notificationRepository->create([
                    'user_id' => $author->id,
                    'type' => Notification::TYPE_CLASS,
                    'content_id' => $post->id,
                    'message' => $body,
                    'url' => '/post?classId='.$class->id
                ]);
                if (isset($author->fcm_token) && $author->fcm_token != '' && $author->fcm_token !== null) {
                    NotificationService::sendOne($author->fcm_token, [
                        'title' => "E-learning thông báo",
                        'body' => ToolService::subWord($body),
                        'icon' => 'https://hdev.info/images/favicon.png',
                        'click_action' => 'https://e-learning.hdev.info/post?classId='.$class->id,
                        'notification_id' => $notif ? $notif->id : false
                    ]);
                }
            }
        }
        if ($reply_comemnt && $reply_comemnt->user && $reply_comemnt->user->id !== auth()->user()->id) {
            $body = '"'.auth()->user()->name.'" đã trả lời bình luận của bạn về bài viết trong "'.$class->name.'": '.$request->input('content');
            $notif = $this->notificationRepository->create([
                'user_id' => $reply_comemnt->user->id,
                'type' => Notification::TYPE_CLASS,
                'content_id' => $post->id,
                'message' => $body,
                'url' => '/post?classId='.$class->id
            ]);
            if (isset($reply_comemnt->user->fcm_token) && $reply_comemnt->user->fcm_token != '' && $reply_comemnt->user->fcm_token !== null) {
                NotificationService::sendOne($reply_comemnt->user->fcm_token, [
                    'title' => "E-learning thông báo",
                    'body' => ToolService::subWord($body),
                    'icon' => 'https://hdev.info/images/favicon.png',
                    'click_action' => 'https://e-learning.hdev.info/post?classId='.$class->id,
                    'notification_id' => $notif ? $notif->id : false
                ]);
            }
        }

        $comment = $this->postRepository->findCommentById($comment->id);
        $comment['created_by'] = $this->postRepository->findAuthorById($comment->user_id);
        unset($comment->user_id);

        return response()->json([
            'status' => 201,
            'message' => 'Bình luận thành công',
            'data' => $comment
        ], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $comment_id
     * @return \Illuminate\Http\Response
     */
    public function updateComment($comment_id, Request $request)
    {
        $fields = $request->validate(
            [
                'content' => 'required',
                'type' => 'required',
            ],
            [
                'content.required' => 'Thiếu tham số content',
                'type.required' => 'Thiếu tham số type',
            ]
        );

        $comment = $this->postRepository->findCommentById($comment_id);
        if (!$comment) {
            return response()->json([
                'status' => 404,
                'message' => 'Bình luận này không tồn tại trên hệ thống'
            ], 404);
        }

        $response = Gate::inspect('update', $comment);
        if ($response->allowed()) {
            $comment = $this->postRepository->updateComment($comment_id, $fields['content'], $fields['type']);
            if (!$comment) {
                return response()->json([
                    'status' => 500,
                    'message' => 'Lỗi máy chủ'
                ], 500);
            }

            $comment = $this->postRepository->findCommentById($comment->id);
            $comment['created_by'] = $this->postRepository->findAuthorById($comment->user_id);
            unset($comment->user_id);

            return response()->json([
                'status' => 200,
                'message' => 'Cập nhật bình luận thành công',
                'data' => $comment
            ], 200);
        }

        return response()->json([
            'status' => 403,
            'message' => 'Đây không phải là bình luận của bạn'
        ], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $comment_id
     * @return \Illuminate\Http\Response
     */
    public function destroyComment($comment_id)
    {
        $comment = $this->postRepository->findCommentById($comment_id);
        if (!$comment) {
            return response()->json([
                'status' => 404,
                'message' => 'Bình luận này không tồn tại trên hệ thống'
            ], 404);
        }

        $teacher = $comment->post->class->teacher;
        $response = Gate::inspect('delete', $comment);
        if ($response->allowed() || auth()->user()->id === $teacher->id) {
            $comment_delete = $this->postRepository->deleteComment($comment_id);
            if (!$comment_delete) {
                return response()->json([
                    'status' => 500,
                    'message' => 'Lỗi máy chủ'
                ], 500);
            }

            // update total comment in post
            $this->postRepository->updateTotalCommentForPost($comment->post->id, 'minus');
            // update total comment for comment
            if (isset($comment->comment_id) && $comment->comment_id !== null) {
                $this->postRepository->updateTotalReplyForComment($comment->comment_id, 'minus');
            }

            // send notif
            if (auth()->user()->id != $comment->user_id) {
                $author = $comment->user;
                $body = '"'.auth()->user()->name.'" vừa xóa một bình luận của bạn trong "'.$comment->post->class->name.'": '.$comment->content;
                $notif = $this->notificationRepository->create([
                    'user_id' => $author->id,
                    'type' => Notification::TYPE_CLASS,
                    'content_id' => $comment->post->id,
                    'message' => $body,
                    'url' => '/post?classId='.$comment->post->class->id
                ]);
                if ($author && isset($author->fcm_token) && $author->fcm_token != '' && $author->fcm_token !== null) {
                    NotificationService::sendOne($author->fcm_token, [
                        'title' => "E-learning thông báo",
                        'body' => ToolService::subWord($body),
                        'icon' => 'https://hdev.info/images/favicon.png',
                        'click_action' => 'https://e-learning.hdev.info/post?classId='.$comment->post->class->id,
                        'notification_id' => $notif ? $notif->id : false
                    ]);
                }
            }

            return response()->json([
                'status' => 200,
                'message' => 'Xóa bình luận thành công'
            ], 200);
        }

        return response()->json([
            'status' => 403,
            'message' => 'Đây không phải là bình luận của bạn'
        ], 403);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllCommentInPost($post_id)
    {
        $post = $this->postRepository->findPostById($post_id);
        if (!$post) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài viết này không tồn tại trên hệ thống'
            ], 404);
        }

        $class = $post->class;
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học này không tồn tại trên hệ thống'
            ], 404);
        }

        if (request()->user()->isTeacher() && auth()->user()->id !== $class->user_id) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không phải là giáo viên quản lý lớp học này'
            ], 403);
        }

        if (request()->user()->isStudent()) {
            $check_student_in_class = false;
            $list_class = auth()->user()->inClasses;
            foreach ($list_class as $item) {
                if ($item->id == $class->id) {
                    $check_student_in_class = true;
                    break;
                }
            }

            if (!$check_student_in_class) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không phải là học sinh trong lớp học này'
                ], 403);
            }
        }

        $data = [];

        // get all comment
        $comments = $this->postRepository->getAllCommentOfPost($post->id);
        foreach ($comments as $comment) {
            $detail_comment = $comment;

            // get author of comment
            $author_comment = $this->postRepository->findAuthorById($comment->user_id);
            $detail_comment['created_by'] = $author_comment;
            unset($detail_comment['user_id']);
            unset($detail_comment['comment_id']);

            // get all reply
            $list_reply = [];
            $all_reply = $this->postRepository->getAllReply($post->id, $comment->id);
            foreach ($all_reply as $reply) {
                $detail_reply = $reply;

                // get author of reply
                $author_reply = $this->postRepository->findAuthorById($reply->user_id);
                $detail_reply['created_by'] = $author_reply;
                unset($detail_reply['user_id']);
                unset($detail_reply['comment_id']);

                $list_reply[] = $detail_reply;
            }

            $detail_comment['list_reply'] = $list_reply;
            $data[] = $detail_comment;
        }

        $data = ToolService::handleCustomPaginationComment($comments, $data);

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLikePost(Request $request)
    {
        $fields = $request->validate(
            [
                'post_id' => 'required'
            ],
            [
                'post_id.required' => 'Thiếu tham số post_id',
            ]
        );

        $post = $this->postRepository->findPostById($fields['post_id']);
        if (!$post) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài viết này không tồn tại trên hệ thống'
            ], 404);
        }

        $class = $post->class;
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học này không tồn tại trên hệ thống'
            ], 404);
        }

        if (request()->user()->isTeacher() && auth()->user()->id !== $class->user_id) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không phải là giáo viên quản lý lớp học này'
            ], 403);
        }

        if (request()->user()->isStudent()) {
            $check_student_in_class = false;
            $list_class = auth()->user()->inClasses;
            foreach ($list_class as $item) {
                if ($item->id == $class->id) {
                    $check_student_in_class = true;
                    break;
                }
            }

            if (!$check_student_in_class) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không phải là học sinh trong lớp học này'
                ], 403);
            }
        }

        // check like, nếu đã like thì bỏ like
        $find_like = $this->postRepository->findLikeByPostId($post->id);
        if ($find_like) {
            $this->postRepository->deleteLike($find_like->id);
            // update total like in comment
            $this->postRepository->updateTotalLikeForPost($post->id, 'minus');
            return response()->json([
                'status' => 200,
                'message' => 'Unliked successfully'
            ], 200);
        }

        $attr = [
            'post_id' => (int) $fields['post_id'],
        ];

        $like = $this->postRepository->createLike($attr);
        if (!$like) {
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }

        // update total like in post
        $this->postRepository->updateTotalLikeForPost($post->id, 'plus');

        // send notif
        $author = $post->user;
        if ($author && $author->id !== auth()->user()->id) {
            $body = '"'.auth()->user()->name.'" đã bày tỏ cảm xúc về bài viết của bạn trong "'.$class->name.'": '.$post->content;
            $notif = $this->notificationRepository->create([
                'user_id' => $author->id,
                'type' => Notification::TYPE_CLASS,
                'content_id' => $post->id,
                'message' => $body,
                'url' => '/post?classId='.$class->id
            ]);
            if (isset($author->fcm_token) && $author->fcm_token != '' && $author->fcm_token !== null) {
                NotificationService::sendOne($author->fcm_token, [
                    'title' => "E-learning thông báo",
                    'body' => ToolService::subWord($body),
                    'icon' => 'https://hdev.info/images/favicon.png',
                    'click_action' => 'https://e-learning.hdev.info/post?classId='.$class->id,
                    'notification_id' => $notif ? $notif->id : false
                ]);
            }
        }

        $like = $this->postRepository->findLikeById($like->id);
        $like['created_by'] = $this->postRepository->findAuthorById($like->user_id);
        unset($like->user_id);

        return response()->json([
            'status' => 201,
            'message' => 'Like successfully',
            'data' => $like
        ], 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeLikeComment(Request $request)
    {
        $fields = $request->validate(
            [
                'comment_id' => 'required'
            ],
            [
                'comment_id.required' => 'Thiếu tham số comment_id',
            ]
        );

        $comment = $this->postRepository->findCommentById($fields['comment_id']);
        if (!$comment) {
            return response()->json([
                'status' => 404,
                'message' => 'Bình luận này không tồn tại trên hệ thống'
            ], 404);
        }

        $class = $comment->post->class;
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học này không tồn tại trên hệ thống'
            ], 404);
        }

        if (request()->user()->isTeacher() && auth()->user()->id !== $class->user_id) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không phải là giáo viên quản lý lớp học này'
            ], 403);
        }

        if (request()->user()->isStudent()) {
            $check_student_in_class = false;
            $list_class = auth()->user()->inClasses;
            foreach ($list_class as $item) {
                if ($item->id == $class->id) {
                    $check_student_in_class = true;
                    break;
                }
            }

            if (!$check_student_in_class) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không phải là học sinh trong lớp học này'
                ], 403);
            }
        }

        // check like, nếu đã like thì bỏ like
        $find_like = $this->postRepository->findLikeByPostIdAndCommentId($comment->post_id, $fields['comment_id']);
        if ($find_like) {
            $this->postRepository->deleteLike($find_like->id);
            // update total like in comment
            $this->postRepository->updateTotalLikeForComment($comment->id, 'minus');
            return response()->json([
                'status' => 200,
                'message' => 'Unliked successfully'
            ], 200);
        }

        $attr = [
            'post_id' => $comment->post_id,
            'comment_id' => (int) $fields['comment_id'],
        ];

        $like = $this->postRepository->createLike($attr);
        if (!$like) {
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }

        // update total like in comment
        $this->postRepository->updateTotalLikeForComment($comment->id, 'plus');

        // send notif
        $author = $comment->user;
        if ($author && $author->id !== auth()->user()->id) {
            $body = '"'.auth()->user()->name.'" đã bày tỏ cảm xúc về bình luận của bạn trong "'.$class->name.'": '.$comment->content;
            $notif = $this->notificationRepository->create([
                'user_id' => $author->id,
                'type' => Notification::TYPE_CLASS,
                'content_id' => $comment->post_id,
                'message' => $body,
                'url' => '/post?classId='.$class->id
            ]);
            if (isset($author->fcm_token) && $author->fcm_token != '' && $author->fcm_token !== null) {
                NotificationService::sendOne($author->fcm_token, [
                    'title' => "E-learning thông báo",
                    'body' => ToolService::subWord($body),
                    'icon' => 'https://hdev.info/images/favicon.png',
                    'click_action' => 'https://e-learning.hdev.info/post?classId='.$class->id,
                    'notification_id' => $notif ? $notif->id : false
                ]);
            }
        }

        $like = $this->postRepository->findLikeById($like->id);
        $like['created_by'] = $this->postRepository->findAuthorById($like->user_id);
        unset($like->user_id);

        return response()->json([
            'status' => 201,
            'message' => 'Like successfully',
            'data' => $like
        ], 201);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllLikeOfPost($post_id)
    {
        $post = $this->postRepository->findPostById($post_id);
        if (!$post) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài viết này không tồn tại trên hệ thống'
            ], 404);
        }

        $class = $post->class;
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học này không tồn tại trên hệ thống'
            ], 404);
        }

        if (request()->user()->isTeacher() && auth()->user()->id !== $class->user_id) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không phải là giáo viên quản lý lớp học này'
            ], 403);
        }

        if (request()->user()->isStudent()) {
            $check_student_in_class = false;
            $list_class = auth()->user()->inClasses;
            foreach ($list_class as $item) {
                if ($item->id == $class->id) {
                    $check_student_in_class = true;
                    break;
                }
            }

            if (!$check_student_in_class) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không phải là học sinh trong lớp học này'
                ], 403);
            }
        }

        $data = [];

        // get all comment
        $likes = $this->postRepository->getAllLikeOfPost($post->id);
        foreach ($likes as $like) {
            $detail_like = $like;

            // get author of comment
            $author_comment = $this->postRepository->findAuthorById($like->user_id);
            $detail_like['created_by'] = $author_comment;
            unset($detail_like['user_id']);
            unset($detail_like['post_id']);
            unset($detail_like['comment_id']);

            $data[] = $detail_like;
        }

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllLikeOfComment($comment_id)
    {
        $comment = $this->postRepository->findCommentById($comment_id);
        if (!$comment) {
            return response()->json([
                'status' => 404,
                'message' => 'Bình luận này không tồn tại trên hệ thống'
            ], 404);
        }

        $post = $this->postRepository->findPostById($comment->post_id);
        if (!$post) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài viết này không tồn tại trên hệ thống'
            ], 404);
        }

        $class = $post->class;
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học này không tồn tại trên hệ thống'
            ], 404);
        }

        if (request()->user()->isTeacher() && auth()->user()->id !== $class->user_id) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không phải là giáo viên quản lý lớp học này'
            ], 403);
        }

        if (request()->user()->isStudent()) {
            $check_student_in_class = false;
            $list_class = auth()->user()->inClasses;
            foreach ($list_class as $item) {
                if ($item->id == $class->id) {
                    $check_student_in_class = true;
                    break;
                }
            }

            if (!$check_student_in_class) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không phải là học sinh trong lớp học này'
                ], 403);
            }
        }

        $data = [];

        // get all comment
        $likes = $this->postRepository->getAllLikeOfComment($post->id, $comment->id);
        foreach ($likes as $like) {
            $detail_like = $like;

            // get author of comment
            $author_comment = $this->postRepository->findAuthorById($like->user_id);
            $detail_like['created_by'] = $author_comment;
            unset($detail_like['user_id']);
            unset($detail_like['comment_id']);

            $data[] = $detail_like;
        }

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }
}
