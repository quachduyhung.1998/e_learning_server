<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\Notification;
use App\Models\User;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Services\NotificationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    private $userRepository;
    private $notificationRepository;

    public function __construct(UserRepositoryInterface $userRepository, NotificationRepositoryInterface $notificationRepository)
    {
        $this->userRepository = $userRepository;
        $this->notificationRepository = $notificationRepository;
    }

    public function index(Request $request)
    {
        $role = $request->role >= 0 ? $request->role : null;
        $keyword = $request->keyword ? $request->keyword : null;
        $list_user = $this->userRepository->getListUser($role, $keyword);
        return response()->json([
            'status' => 200,
            'data' => $list_user
        ]);
    }

    public function store(UserRequest $request)
    {

        if ((int) $request->validated()['role'] === User::ROLE_SUPER_ADMIN) {
            return response()->json([
                'status' => 403,
                'message' => 'Ứng dụng đã có Super Admin!!!'
            ], 403);
        }

        if ((int) $request->validated()['role'] === User::ROLE_ADMIN) {
            if (request()->user()->isSuperAdmin()) {
                $user = $this->userRepository->create($request->validated());
            } else {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không có quyền truy cập vào nội dung này!!!'
                ], 403);
            }
        } else {
            $user = $this->userRepository->create($request->validated());
        }
        if (!$user) {
            return response()->json([
                'status' => 500,
                'message' => 'Đã có lỗi không mong muốn xảy ra, vui lòng thử lại sau!'
            ], 500);
        }

        return response()->json([
            'status' => 201,
            'message' => 'Thêm mới thành công!'
        ], 201);
    }

    public function show($id)
    {
        $user = $this->userRepository->findById($id);

        if (!$user) {
            return response()->json([
                'status' => 404,
                'message' => 'Người dùng này hiện không có trên hệ thống!'
            ], 404);
        }

        return response()->json([
            'status' => 200,
            'data' => $user
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $fields = $request->validate(
            [
                'name' => 'required|string',
                'email' => 'required|string|email',
                'role' => 'required'
            ],
            [
                'name.required' => 'Bạn chưa nhập họ tên',
                'email.required' => 'Bạn chưa nhập email',
                'email.email' => 'Email không đúng định dạng',
                'role.required' => 'Bạn chưa chọn chức vụ',
            ]
        );

        $user = $this->userRepository->findById($id);
        if (!$user) {
            return response()->json([
                'status' => 404,
                'message' => 'Người dùng này không có trên hệ thống'
            ], 404);
        }

        $attr = [];

        if (isset($fields['name']) && $fields['name'] !== null) {
            $attr['name'] = $fields['name'];
        }
        if (isset($fields['email']) && $fields['email'] !== null) {
            $user_exists = User::where('id', '!=', $user->id)->where('email', $fields['email'])->exists();
            if ($user_exists) {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => [
                        'email' => [
                            'Email này đã được sử dụng'
                        ]
                    ]
                ], 422);
            } else {
                $attr['email'] = $fields['email'];
            }
        }
        if (isset($fields['role']) && $fields['role'] !== null) {
            $attr['role'] = $fields['role'];
        }
        if (isset($fields['avatar']) && $fields['avatar'] !== null) {
            $attr['avatar'] = $fields['avatar'];
        }
        if (isset($fields['contact']) && $fields['contact'] !== null) {
            $attr['contact'] = $fields['contact'];
        }
        if (isset($fields['gender']) && $fields['gender'] !== null) {
            $attr['gender'] = (int) $fields['gender'];
        }
        if (isset($fields['birthday']) && $fields['birthday'] !== null) {
            $attr['birthday'] = $fields['birthday'];
        }
        if (isset($fields['city']) && $fields['city'] !== null) {
            $attr['city'] = $fields['city'];
        }
        if (isset($fields['district']) && $fields['district'] !== null) {
            $attr['district'] = $fields['district'];
        }
        if (isset($fields['wards']) && $fields['wards'] !== null) {
            $attr['wards'] = $fields['wards'];
        }

        $user = $this->userRepository->update($id, $attr);
        if (!$user) {
            return response()->json([
                'status' => 400,
                'data' => 'Fail'
            ], 400);
        }

        return response()->json([
            'status' => 200,
            'data' => $user
        ], 200);
    }

    public function updateProfile(Request $request)
    {
        $fields = $request->validate(
            [
                'name' => 'required|string',
                'email' => 'required|string|email',
            ],
            [
                'name.required' => 'Bạn chưa nhập họ tên',
                'email.required' => 'Bạn chưa nhập email',
                'email.email' => 'Email không đúng định dạng'
            ]
        );

        $attr = [];

        if (isset($fields['name']) && $fields['name'] !== null) {
            $attr['name'] = $fields['name'];
        }
        if (isset($fields['email']) && $fields['email'] !== null) {
            $user_exists = User::where('id', '!=', auth()->user()->id)->where('email', $fields['email'])->exists();
            if ($user_exists) {
                return response()->json([
                    'message' => 'The given data was invalid.',
                    'errors' => [
                        'email' => [
                            'Email này đã được sử dụng'
                        ]
                    ]
                ], 422);
            } else {
                $attr['email'] = $fields['email'];
            }
        }
        if (isset($fields['avatar']) && $fields['avatar'] !== null) {
            $attr['avatar'] = $fields['avatar'];
        }
        if (isset($fields['contact']) && $fields['contact'] !== null) {
            $attr['contact'] = $fields['contact'];
        }
        if (isset($fields['gender']) && $fields['gender'] !== null) {
            $attr['gender'] = (int) $fields['gender'];
        }
        if (isset($fields['birthday']) && $fields['birthday'] !== null) {
            $attr['birthday'] = $fields['birthday'];
        }
        if (isset($fields['city']) && $fields['city'] !== null) {
            $attr['city'] = $fields['city'];
        }
        if (isset($fields['district']) && $fields['district'] !== null) {
            $attr['district'] = $fields['district'];
        }
        if (isset($fields['wards']) && $fields['wards'] !== null) {
            $attr['wards'] = $fields['wards'];
        }

        $user = $this->userRepository->updateProfile($attr);
        if (!$user) {
            return response()->json([
                'status' => 400,
                'data' => 'Fail'
            ], 400);
        }

        return response()->json([
            'status' => 200,
            'data' => $user
        ], 200);
    }

    public function destroy($id)
    {
        $role_of_user = $this->userRepository->getRole($id);

        if (!$role_of_user && $role_of_user !== 0) {
            return response()->json([
                'status' => 404,
                'message' => 'Người dùng này hiện không có trên hệ thống!'
            ], 404);
        }

        if ($role_of_user === User::ROLE_SUPER_ADMIN) {
            return response()->json([
                'status' => 403,
                'message' => 'Không thể xóa Super Admin!!!'
            ], 403);
        }

        if ($role_of_user === User::ROLE_ADMIN) {
            if (request()->user()->isSuperAdmin()) {
                $user_deleted = $this->userRepository->delete($id);
            } else {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không có quyền xóa người dùng này!!!'
                ], 403);
            }
        } else {
            $user_deleted = $this->userRepository->delete($id);
        }

        if (!$user_deleted) {
            return response()->json([
                'status' => 500,
                'message' => 'Đã có lỗi không mong muốn xảy ra, vui lòng thử lại sau!'
            ], 500);
        }

        return response()->json([
            'status' => 200,
            'message' => 'Xóa thành công!'
        ], 200);
    }

    public function changePassword(Request $request)
    {
        $fields = $request->validate(
            [
                'password' => 'required',
                'new_password' => 'required|string|min:8',
                'new_password_confirmation' => 'required|same:new_password',
            ],
            [
                'password.required' => 'Bạn chưa nhập mật khẩu cũ',
                'new_password.required' => 'Bạn chưa nhập mật khẩu mới',
                'new_password_confirmation.required' => 'Bạn chưa nhập mật khẩu xác nhận',
                'new_password_confirmation.same' => 'Mật khẩu không trùng khớp',
            ]
        );

        $user = request()->user();
        if (!Hash::check($fields['password'], $user->password)) {
            return response()->json([
                'status' => 401,
                'message' => 'Sai thông tin đăng nhập'
            ], 401);
        }

        $user->password = Hash::make($fields['new_password']);
        if ($user->save()) {
            // xóa hết token cũ
            $user->tokens()->delete();
        }
        // đăng ký token mới
        $token = $user->createToken('elearningtoken')->plainTextToken;

        return response()->json([
            'status' => 200,
            'message' => 'Đổi mật khẩu thành công',
            'access_token' => $token
        ]);
    }

    public function changePermissions(Request $request, $id)
    {
        $request->validate(
            [
                'role' => 'required'
            ],
            [
                'role.required' => 'Bạn chưa chọn quyền để gán!'
            ]
        );

        $user = $this->userRepository->findById($id);
        if (!$user) {
            return response()->json([
                'status' => 404,
                'message' => 'Người dùng này không có trên hệ thống'
            ], 404);
        }

        if ((int) $request->role === User::ROLE_SUPER_ADMIN) {
            return response()->json([
                'status' => 403,
                'message' => 'Không thể gán quyền Super Admin cho người dùng này!!!'
            ], 403);
        }

        if ((int) $request->role === User::ROLE_ADMIN) {
            if (request()->user()->isSuperAdmin()) {
                $this->userRepository->handleChangePermissions((int) $id, $request->role);
            } else {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không có quyền truy cập vào nội dung này!!!'
                ], 403);
            }
        } else {
            $this->userRepository->handleChangePermissions((int) $id, $request->role);
        }

        // push notif
        if ($user) {
            $body = 'Bạn vừa được Admin gán quyền trở thành "'.User::POSITION_NAME[$request->role].'"';
            $notif = $this->notificationRepository->create([
                'user_id' => $user->id,
                'message' => $body,
                'url' => '/'
            ]);
            if (isset($user->fcm_token) && $user->fcm_token != '' && $user->fcm_token !== null) {
                NotificationService::sendOne($user->fcm_token, [
                    'title' => "Xin chào $user->name",
                    'body' => $body,
                    'icon' => 'https://hdev.info/images/favicon.png',
                    'click_action' => 'https://e-learning.hdev.info/',
                    'notification_id' => $notif ? $notif->id : false
                ]);
            }
        }

        return response()->json([
            'status' => 200,
            'message' => 'Gán quyền thành công!'
        ], 200);
    }

    public function getListUserWithPermissions($role)
    {
        $list_user = $this->userRepository->getListUserByRole($role);
        return response()->json([
            'status' => 200,
            'data' => $list_user
        ], 200);
    }

    public function saveToken(Request $request)
    {
        // $fields = json_decode($request->getContent(), true);
        $fields = $request->json()->all();

        $err = false;
        if (!isset($fields['user_id'])) {
            $err['user_id'] = 'Thiếu user_id';
        }
        if (!isset($fields['fcm_token'])) {
            $err['fcm_token'] = 'Thiếu fcm_token';
        }
        if ($err) {
            return response()->json($err, 500);
        }

        $user = User::where('id', $fields['user_id'])->first();
        if (!$user) {
            return response()->json([
                'message' => 'User không có trong hệ thống'
            ]);
        }

        $current_token = $user->fcm_token;
        if ($current_token === $fields['fcm_token']) {
            return;
        }

        $user->fcm_token = $fields['fcm_token'];
        $user->save();

        NotificationService::welcome($fields['fcm_token'], $user->name);

        return response()->json([
            'message' => 'Token successfully stored.'
        ]);
    }
}
