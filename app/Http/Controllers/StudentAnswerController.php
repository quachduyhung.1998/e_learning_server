<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentAnswerRequest;
use App\Models\StudentClass;
use App\Models\StudentAnswer;
use App\Models\TestResult;
use App\Models\User;
use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\Lesson\LessonRepositoryInterface;
use App\Repositories\Question\QuestionRepositoryInterface;
use App\Repositories\StudentAnswer\StudentAnswerRepositoryInterface;
use App\Repositories\Test\TestRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentAnswerController extends Controller
{
    private $studentAnswerRepository;
    private $testRepository;
    private $questionRepository;
    private $courseRepository;
    private $lessonRepository;

    public function __construct(
        StudentAnswerRepositoryInterface $studentAnswerRepository,
        TestRepositoryInterface $testRepository,
        QuestionRepositoryInterface $questionRepository,
        CourseRepositoryInterface $courseRepository,
        LessonRepositoryInterface $lessonRepository
    )
    {   
        $this->studentAnswerRepository = $studentAnswerRepository;
        $this->testRepository = $testRepository;
        $this->questionRepository = $questionRepository;
        $this->courseRepository = $courseRepository;
        $this->lessonRepository = $lessonRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentAnswerRequest $request)
    {
        try {
            $test = $this->testRepository->findById($request->input('test_id'));
            if (!$test) {
                return response()->json([
                    'status' => 404,
                    'message' => 'Bài kiểm tra này không có trên hệ thống'
                ], 404);
            }

            // lấy lần làm bài trc đó của student, nếu k có => lần đầu
            $time_worked = $this->studentAnswerRepository->getTimeWorkedTest($test->id);
            $time_worked_current = $time_worked + 1;
            
            $data = [];
            $mark_achieved = 0;
            $total_answer_right = 0;
            $list_answer = json_decode($request->answers, true);
            foreach ($list_answer as $answer) {
                /***** lấy điểm của bài test student vừa làm *****/
                $question = $this->questionRepository->findQuestion($answer['question_id']);
                if ($question) {
                    $all_answer_of_question = json_decode($question->answers, true);

                    // kiểm tra xem có bao nhiêu câu trả lời đúng
                    $count_right_answer_of_question = 0;
                    foreach ($all_answer_of_question as $answer_item) {
                        if ($answer_item['right'] == 1) $count_right_answer_of_question++;
                    }
                    
                    // mỗi đáp án đúng được số điểm = tổng điểm của câu hỏi / số đáp án đúng
                    $mark_of_answer_right = $question->mark / $count_right_answer_of_question;

                    // số điểm đạt được của câu trả lời này
                    $is_right = false;
                    $mark_this_answer = 0;
                    
                    // kiểm tra xem có bao nhiêu câu trả lời đúng trong phần trả lời của student
                    $arr_answer_of_student = str_replace(']', '', str_replace('[', '', $answer['key']));
                    $arr_answer_of_student = explode(',', $arr_answer_of_student);
                    foreach ($arr_answer_of_student as $key) {
                        // lấy vị trí của câu trả lời
                        $index_answer = array_search(trim($key), array_column($all_answer_of_question, 'key'));
                        if ($all_answer_of_question[$index_answer]['right'] == 1) {
                            $is_right = true;
                            $mark_this_answer += $mark_of_answer_right;
                            $mark_achieved += $mark_of_answer_right;
                        }
                    }
                    if ($is_right) {
                        $total_answer_right++;
                    }

                    $data[] = [
                        'user_id' => auth()->user()->id,
                        'test_id' => $test->id,
                        'question_id' => (int) $answer['question_id'],
                        'answers' => (string) $answer['key'],
                        'is_right' => $is_right ? StudentAnswer::IS_RIGHT : StudentAnswer::IS_WRONG, // 0-sai, 1-đúng
                        'mark_achieved' => $mark_this_answer,
                        'number_time_worked' => $time_worked_current,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                }
            }
            // lưu câu trả lời của student
            $this->studentAnswerRepository->create($data);
            
            // lưu lại tổng điểm của user vừa làm tại lần này
            $save_result_student_worked = TestResult::create([
                'user_id' => auth()->user()->id,
                'test_id' => $test->id,
                'mark_achieved' => $mark_achieved,
                'total_answer_right' => $total_answer_right,
                'number_time_worked' => $time_worked_current,
                'time_start' => $request->input('time_start'),
                'time_finish' => $request->input('time_finish'),
                'total_time_work' => strtotime($request->input('time_finish')) - strtotime($request->input('time_start')),
            ]);
            if (!$save_result_student_worked) {
                return response()->json([
                    'status' => 500,
                    'message' => 'Lỗi server'
                ], 500);
            }

            return response()->json([
                'status' => 200,
                'data' => [
                    'mark_achieved' => $mark_achieved,
                    'min_mark_achieved' => $test->min_mark_achieved,
                    'total_mark' => $test->total_mark,
                    'time_start' => $request->input('time_start'),
                    'time_finish' => $request->input('time_finish'),
                    'total_time_work' => strtotime($request->input('time_finish')) - strtotime($request->input('time_start')),
                    'number_time_worked' => $time_worked_current,
                ]
            ], 200);

        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     * show chi tiết bài test đã làm, từng câu hỏi, câu trả lời, số điểm
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($test_id, Request $request)
    {
        // $test_id, $number_time_worked
        $fields = $request->validate(
            [
                'number_time_worked' => 'required'
            ],
            [
                'number_time_worked.required' => 'Thiếu tham số number_time_worked (lần làm bài test thứ n )'
            ]
        );
        $number_time_worked = $fields['number_time_worked'];
        $user_id = (isset($request['user_id']) && $request['user_id'] != '') ? $request['user_id'] : auth()->user()->id;

        $test = $this->testRepository->findById($test_id);
        if (!$test) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài kiểm tra này không có trên hệ thống'
            ], 404);
        }
        if ($test->list_question_id == '') {
            return response()->json([
                'status' => 404,
                'message' => 'Bài kiểm tra này chưa có câu hỏi nào'
            ], 404);
        }
        
        $data = $test;

        // kết quả làm bài
        $result_worked = TestResult::query()
            ->where('user_id', $user_id)
            ->where('test_id', $test_id)
            ->where('number_time_worked', $number_time_worked)
            ->first();
        if (!$result_worked) {
            return response()->json([
                'status' => 404,
                'message' => 'Bạn chưa làm bài kiểm tra này'
            ], 404);
        }
            
        $data['result'] = [
            'mark_achieved' => $result_worked->mark_achieved,
            'total_answer_right' => $result_worked->total_answer_right,
            'number_time_worked' => $result_worked->number_time_worked,
            'time_start' => $result_worked->time_start,
            'time_finish' => $result_worked->time_finish,
            'total_time_work' => $result_worked->total_time_work,
        ];

        $list_question = [];

        // lấy tất cả các câu hỏi trong bài test
        $questions = $this->questionRepository->getAllQuestionOfTest(json_decode($test->list_question_id));
        foreach ($questions as $question) {
            $detail_question = $question;
            $answer = $this->studentAnswerRepository->findOneDetail($test_id, $question['id'], $number_time_worked, $user_id);
            if (!$answer) {
                return response()->json([
                    'status' => 404,
                    'message' => 'Bạn chưa làm bài kiểm tra này'
                ], 404);
            }

            $arr_answer_of_student = str_replace(']', '', str_replace('[', '', $answer->answers));
            $arr_answer_of_student = explode(',', $arr_answer_of_student);

            $arr_answer = [];
            foreach ($arr_answer_of_student as $item) {
                $arr_answer[] = trim($item);
            }
            
            $detail_question['student_answer'] = [
                'key' => $arr_answer,
                'is_right' => $answer->is_right,
                'mark_achieved' => $answer->mark_achieved
            ];
            $list_question[] = $detail_question;
        }

        $data['list_question'] = $list_question;

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * get all test worked of course (lấy tất cả các bài test đã làm trong khóa học, chỉ lấy lần cuối làm bài)
     */
    public function getAllTestWorkedInCourse($course_id, Request $request)
    {
        $course = $this->courseRepository->findById($course_id);
        if (!$course) {
            return response()->json([
                'status' => 404,
                'message' => 'Khóa học này không có trên hệ thống'
            ], 404);
        }

        // kiểm tra xem giáo viên có quản lý lớp này hay không
        if (request()->user()->isTeacher()) {
            $classes = $course->classes;
            $permisson = false;
            foreach ($classes as $class) {
                if (auth()->user()->id === $class->user_id) {
                    $permisson = true;
                    break;
                }
            }
            
            if (!$permisson) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không có quyển thực hiện hành động này'
                ], 403);
            }
        }

        $data = [];

        $tests = $course->tests()->orderBy('id', 'desc')->get();
        foreach ($tests as $test) {
            $user_id = auth()->user()->id;
            if ($request->input('user_id') !== null) {
                $user_id = (int) $request->input('user_id');
            }

            $number_time_worked = $this->studentAnswerRepository->getTimeWorkedTest($test->id, $user_id);

            $result_worked = TestResult::query()
                ->where('user_id', $user_id)
                ->where('test_id', $test->id)
                ->where('number_time_worked', $number_time_worked)
                ->first();
            if ($result_worked) {
                $test_info = $test;
                $test_info['result'] = [
                    'mark_achieved' => $result_worked->mark_achieved,
                    'total_answer_right' => $result_worked->total_answer_right,
                    'number_time_worked' => $result_worked->number_time_worked,
                    'time_start' => $result_worked->time_start,
                    'time_finish' => $result_worked->time_finish,
                    'total_time_work' => $result_worked->total_time_work,
                ];

                $data[] = $test_info;
            }
        }

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * get all test worked of lesson (lấy tất cả số lần làm bài test trong bài học)
     */
    public function getAllTestWorkedInLesson($lesson_id)
    {
        $lesson = $this->lessonRepository->findById($lesson_id);
        if (!$lesson) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài học này không có trên hệ thống'
            ], 404);
        }
        $test = $lesson->test;

        $data = [];

        $list_result_worked = TestResult::query()
            ->where('user_id', auth()->user()->id)
            ->where('test_id', $test->id)
            ->orderBy('number_time_worked', 'asc')
            ->get();
        if ($list_result_worked) {
            foreach ($list_result_worked as $result) {
                $data[] = [
                    'test_id' => $test->id,
                    'mark_achieved' => $result->mark_achieved,
                    'total_answer_right' => $result->total_answer_right,
                    'number_time_worked' => $result->number_time_worked,
                    'time_start' => $result->time_start,
                    'time_finish' => $result->time_finish,
                    'total_time_work' => $result->total_time_work,
                ];
            }
        }

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * get all user worked test
     */
    public function getAllUserWorked($test_id, Request $request)
    {
        $test = $this->testRepository->findById($test_id);
        if (!$test) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài kiểm tra này không có trên hệ thống'
            ], 404);
        }

        // kiểm tra xem giáo viên có quản lý lớp này hay không
        if (request()->user()->isTeacher()) {
            $classes = $test->course->classes;
            $permisson = false;
            foreach ($classes as $class) {
                if (auth()->user()->id === $class->user_id) {
                    $permisson = true;
                    break;
                }
            }
            
            if (!$permisson) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không có quyển thực hiện hành động này'
                ], 403);
            }
        }

        $list_user_exists = false;
        $list_user_id = [];
        if (isset($request['class_id']) && $request['class_id'] != '') {
            $list_user_exists = true;
            $list_user_in_class = StudentClass::select(['user_id'])->where('class_id', $request['class_id'])->get();
            foreach ($list_user_in_class as $item) {
                $list_user_id[] = $item->user_id;
            }
        }

        $data = [];

        $query = DB::table('test_results')->where('test_id', $test_id);
        if ($list_user_exists) {            
            $query->whereIn('user_id', $list_user_id);
        }        

        $list_result = $query->groupBy('user_id')
            ->orderBy('id', 'desc')
            ->get(['user_id', DB::raw('MAX(number_time_worked) as number_time_worked')]);

        foreach ($list_result as $result) {
            $user = User::where('id', $result->user_id)->first();
            $data_item = [
                'name' => $user->name,
                'email' => $user->email,
                'role' => User::POSITION_NAME[$user->role],
            ];

            $test_result = TestResult::query()
                ->where('test_id', $test_id)
                ->where('user_id', $result->user_id)
                ->where('number_time_worked', $result->number_time_worked)
                ->first();

            $data_item['result'] = $test_result;
            
            $data[] = $data_item;
        }

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }
}
