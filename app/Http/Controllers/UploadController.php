<?php

namespace App\Http\Controllers;

use App\Services\ToolService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UploadController extends Controller
{
    public function uploadImage(Request $request)
    {
        $field = $request->validate(
            [
                'image' => 'required|mimes:jpg,png,jpeg|max:3048',
            ],
            [
                'image.required' => 'Bạn chưa chọn ảnh',
                'image.mimes' => 'Chỉ hỗ trợ các hình ảnh có đuôi jpg, jpeg, png',
                'image.max' => 'Kích thước ảnh quá lớn',
            ]
        );

        $imgUrl = ToolService::handleUploadImage($field['image']);
        if (!$imgUrl) {
            return response()->json([
                'status' => 200,
                'message' => 'Upload ảnh thất bại'
            ]);
        }

        return response()->json([
            'status' => 200,
            'data' => [
                'url' => $imgUrl
            ]
        ]);
    }
}