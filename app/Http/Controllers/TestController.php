<?php

namespace App\Http\Controllers;

use App\Http\Requests\TestRequest;
use App\Models\Notification;
use App\Repositories\Lesson\LessonRepositoryInterface;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\Question\QuestionRepositoryInterface;
use App\Repositories\Test\TestRepositoryInterface;
use App\Services\NotificationService;
use App\Services\ToolService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class TestController extends Controller
{
    private $testRepository;
    private $lessonRepository;
    private $questionRepository;
    private $notificationRepository;

    public function __construct(
        TestRepositoryInterface $testRepository,
        LessonRepositoryInterface $lessonRepository,
        QuestionRepositoryInterface $questionRepository,
        NotificationRepositoryInterface $notificationRepository
    )
    {
        $this->testRepository = $testRepository;
        $this->lessonRepository = $lessonRepository;
        $this->questionRepository = $questionRepository;
        $this->notificationRepository = $notificationRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TestRequest $request)
    {
        $lesson = $this->lessonRepository->findById($request->input('lesson_id'));
        if (!$lesson) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài học này hiện không có trên hệ thống'
            ], 404);
        }
        
        $course = $lesson->course;
        if (!$course) {
            return response()->json([
                'status' => 404,
                'message' => 'Khóa học này hiện không có trên hệ thống'
            ], 404);
        }
        
        // CoursePolicy
        $response = Gate::inspect('create', $course);

        if (!$response->allowed()) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }

        $attr = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'lesson_id' => $lesson->id,
            'course_id' => $lesson->course->id,
            'list_question_id' => $request->input('list_question_id'),
            'time_work' => $request->input('time_work'),
            'is_required' => (int) $request->input('is_required'),
            'time_start' => $request->input('time_start'),
            'time_end' => $request->input('time_end'),
        ];

        $test = $this->testRepository->create($attr);
        if (!$test) {
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }
        
        $list_question = $this->questionRepository->findByListId(json_decode($test->list_question_id, true));
        $test['list_question'] = $list_question;

        // send notif
        $body = 'Bạn có 1 bài kiểm tra mới trong khóa học "'.$course->name.'": '.$request->input('name');
        if ($request->input('time_start') !== null && $request->input('time_end') !== null) {
            $body = 'Bạn có 1 bài kiểm tra mới trong khóa học "'.$course->name.'": '.$request->input('name').' (thời gian làm bài từ "'.date('H:i:s \n\gà\y d/m/Y', strtotime($request->input('time_start'))).' đến '.date('H:i:s \n\gà\y d/m/Y', strtotime($request->input('time_end'))).'")';
        }
        $classes = $course->classes;
        // $list_user_id = [];
        // $list_token_fcm = [];
        foreach ($classes as $class) {
            if ($class && $class->students) {
                foreach ($class->students as $student) {
                    if ($student) {
                        // $list_token_fcm[] = $student->fcm_token;
                        // $list_user_id[] = $student->id;
                        $notif = $this->notificationRepository->create([
                            'user_id' => $student->id,
                            'type' => Notification::TYPE_COURSE,
                            'content_id' => $class['id'],
                            'message' => $body,
                            'url' => '/student/course/'.$course->id.'/content?instanceId='.$lesson->id.'&type=lesson'
                        ]);
                        if (isset($student->fcm_token) && $student->fcm_token != '' && $student->fcm_token !== null) {
                            NotificationService::sendOne($student->fcm_token, [
                                'title' => "Xin chào $student->name",
                                'body' => ToolService::subWord($body),
                                'icon' => 'https://hdev.info/images/favicon.png',
                                'click_action' => 'https://e-learning.hdev.info/student/course/'.$course->id.'/content?instanceId='.$lesson->id.'&type=lesson',
                                'notification_id' => $notif ? $notif->id : false
                            ]);
                        }
                    }
                }
            }
        }
        // if (!empty($list_token_fcm)) {
        //     $body = 'Bạn có 1 bài kiểm tra mới trong khóa học "'.$course->name.'", thời gian làm bài từ "'.date('H:i:s \n\gà\y d/m/Y', strtotime($request->input('time_start'))).' đến '.date('H:i:s \n\gà\y d/m/Y', strtotime($request->input('time_end'))).'"';
        //     NotificationService::sendMulti($list_token_fcm, [
        //         'title' => "E-learning thông báo",
        //         'body' => ToolService::subWord($body),
        //         'icon' => 'https://hdev.info/images/favicon.png',
        //         'click_action' => 'https://e-learning.hdev.info/'        
        //     ]);
        //     $this->notificationRepository->createMany($list_user_id, [
        //         'type' => Notification::TYPE_TEST,
        //         'content_id' => $test->id,
        //         'message' => $body
        //     ]);
        // }

        return response()->json([
            'status' => 200,
            'message' => 'Tạo bài kiểm tra thành công',
            'data' => $test
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $test = $this->testRepository->findById($id);
        if (!$test) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài kiểm này hiện không có trên hệ thống'
            ], 404);
        }

        return response()->json([
            'status' => 200,
            'data' => $test
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $test = $this->testRepository->findById($id);
        if (!$test) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài kiểm tra này hiện không có trên hệ thống'
            ], 404);
        }
        
        $response = Gate::inspect('update', $test);
        if (!$response->allowed()) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }
        
        $attr = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'list_question_id' => $request->input('list_question_id'),
            'time_work' => $request->input('time_work'),
            'is_required' => (int) $request->input('is_required'),
            'time_start' => $request->input('time_start'),
            'time_end' => $request->input('time_end'),
        ];

        $test_updated = $this->testRepository->update($id, $attr);
        if (!$test_updated) {
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }
        
        $list_question = $this->questionRepository->findByListId(json_decode($test->list_question_id, true));
        $test_updated['list_question'] = $list_question;

        return response()->json([
            'status' => 200,
            'message' => 'Cập nhật bài kiểm tra thành công',
            'data' => $test_updated
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $test = $this->testRepository->findById($id);
        if (!$test) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài kiểm này hiện không có trên hệ thống'
            ], 404);
        }
        
        $response = Gate::inspect('delete', $test);
        if (!$response->allowed()) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }
        
        $deleted = $this->testRepository->delete($id);

        if (!$deleted) {
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }

        return response()->json([
            'status' => 200,
            'message' => 'Xóa bài kiểm tra thành công'
        ], 200);
    }

    /**
     * get all question of test
     */
    public function getAllQuestionOfTest($test_id)
    {
        $test = $this->testRepository->findById($test_id);
        if (!$test) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài kiểm này hiện không có trên hệ thống'
            ], 404);
        }

        // nếu là sinh viên
        // kiểm tra sinh viên này trong lớp nào và lớp này có được gán khóa học này vào không
        if (request()->user()->isStudent()) {
            $list_course_id = request()->user()->getListCourseId();
            if (!in_array($test->course_id, $list_course_id)) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không có quyền thực hiện hành động này'
                ], 403);
            }

            if (isset($test->time_start) && $test->time_start !== null && strtotime($test->time_start) > time()) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Chưa đến thời gian cho phép làm bài kiểm tra'
                ], 403);
            }
            if (isset($test->time_end) && $test->time_end !== null && strtotime($test->time_end) < time()) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Đã quá thời gian cho phép làm bài kiểm tra'
                ], 403);
            }
        }

        $questions = [];
        if ($test->list_question_id && $test->list_question_id != null) {
            $questions = $this->questionRepository->getAllQuestionOfTest(json_decode($test->list_question_id));
        }

        return response()->json([
            'status' => 200,
            'data' => $questions
        ], 200);
    }
}
