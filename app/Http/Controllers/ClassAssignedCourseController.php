<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClassAssignedCourseRequest;
use App\Models\Notification;
use App\Repositories\ClassAssignedCourse\ClassAssignedCourseRepositoryInterface;
use App\Repositories\Classes\ClassRepositoryInterface;
use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Services\NotificationService;
use App\Services\ToolService;
use Illuminate\Http\Request;

class ClassAssignedCourseController extends Controller
{
    private $classAssignedCourseRepository;
    private $classRepository;
    private $courseRepository;
    private $notificationRepository;

    public function __construct(
        ClassAssignedCourseRepositoryInterface $classAssignedCourseRepository,
        ClassRepositoryInterface $classRepository,
        CourseRepositoryInterface $courseRepository,
        NotificationRepositoryInterface $notificationRepository
    ){
        $this->classAssignedCourseRepository = $classAssignedCourseRepository;
        $this->classRepository = $classRepository;
        $this->courseRepository = $courseRepository;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = [];
        if ($request->input('course_name') && $request->input('course_name') != '') {
            $filter['course_name'] = $request->input('course_name');
        }
        if ($request->input('class_name') && $request->input('class_name') != '') {
            $filter['class_name'] = $request->input('class_name');
        }

        $datas = $this->classAssignedCourseRepository->getAll($filter);

        return response()->json([
            'status' => 200,
            'data' => $datas
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClassAssignedCourseRequest $request)
    {
        $class = $this->classRepository->findById($request->input('class_id'));
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học không tồn tại trên hệ thống'
            ], 404);
        }

        $teacher = $class->teacher;
        if (!$teacher) {
            return response()->json([
                'status' => 200,
                'message' => 'Lớp học chưa có giáo viên quản lý'
            ], 200);
        }

        $course = $this->courseRepository->findById($request->input('course_id'));
        if (!$course) {
            return response()->json([
                'status' => 404,
                'message' => 'Khóa học không tồn tại trên hệ thống'
            ], 404);
        }

        $check_assigned = $this->classAssignedCourseRepository->create($class->id, $course->id);
        if (!$check_assigned) {
            return response()->json([
                'status' => 200,
                'message' => 'Khóa học này đã có trong lớp học'
            ], 200);
        }

        // send notif
        $body = 'Lớp học "'.$class->name.'" của bạn vừa có thêm một khóa học mới là "'.$course->name.'"';
        if ($teacher) {
            $notif =  $this->notificationRepository->create([
                'user_id' => $teacher->id,
                'type' => Notification::TYPE_COURSE,
                'content_id' => $course->id,
                'message' => $body,
                'url' => '/teacher/course/'.$course->id
            ]);
            if (isset($teacher->fcm_token) && $teacher->fcm_token != '' && $teacher->fcm_token !== null) {
                NotificationService::sendOne($teacher->fcm_token, [
                    'title' => "E-learning thông báo",
                    'body' => ToolService::subWord($body),
                    'icon' => 'https://hdev.info/images/favicon.png',
                    'click_action' => 'https://e-learning.hdev.info/teacher/course/'.$course->id,
                    'notification_id' => $notif ? $notif->id : false
                ]);
            }
        }
        
        // $list_token_fcm = [];
        // $list_user_id = [];
        $students = $class->students;
        if ($students) {
            foreach ($students as $student) {
                if ($student) {
                    // $list_token_fcm[] = $student->fcm_token;
                    // $list_user_id[] = $student->id;

                    $notif =  $this->notificationRepository->create([
                        'user_id' => $student->id,
                        'type' => Notification::TYPE_COURSE,
                        'content_id' => $course->id,
                        'message' => $body,
                        'url' => '/student/course/'.$course->id
                    ]);
                    if (isset($student->fcm_token) && $student->fcm_token != '' && $student->fcm_token !== null) {
                        NotificationService::sendOne($student->fcm_token, [
                            'title' => "E-learning thông báo",
                            'body' => ToolService::subWord($body),
                            'icon' => 'https://hdev.info/images/favicon.png',
                            'click_action' => 'https://e-learning.hdev.info/student/course/'.$course->id,
                            'notification_id' => $notif ? $notif->id : false
                        ]);
                    }
                }
            }
        }

        // if (!empty($list_token_fcm)) {
        //     NotificationService::sendMulti($list_token_fcm, [
        //         'title' => "E-learning thông báo",
        //         'body' => ToolService::subWord($body),
        //         'icon' => 'https://hdev.info/images/favicon.png',
        //         'click_action' => 'https://e-learning.hdev.info/student/course/'.$course->id
        //     ]);
        //     $this->notificationRepository->createMany($list_user_id, [
        //         'type' => Notification::TYPE_COURSE,
        //         'content_id' => $course->id,
        //         'message' => $body
        //     ]);
        // }

        return response()->json([
            'status' => 201,
            'message' => 'Gán thành công'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClassAssignedCourseRequest $request)
    {
        $class = $this->classRepository->findById($request->input('class_id'));
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học không tồn tại trên hệ thống'
            ], 404);
        }

        $teacher = $class->teacher;
        if (!$teacher) {
            return response()->json([
                'status' => 200,
                'message' => 'Lớp học chưa có giáo viên quản lý'
            ], 200);
        }

        $course = $this->courseRepository->findById($request->input('course_id'));
        if (!$course) {
            return response()->json([
                'status' => 404,
                'message' => 'Khóa học không tồn tại trên hệ thống'
            ], 404);
        }

        $deleted = $this->classAssignedCourseRepository->delete($request->input('class_id'), $request->input('course_id'));
        if (!$deleted) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học này chưa được gán vào khóa học'
            ], 404);
        }

        // send notif
        $body = 'Khóa học "'.$course->name.'" vừa bị xóa khỏi lớp học "'.$class->name.'" của bạn';

        // $list_user_id = [];
        // $list_token_fcm = [];
        if ($teacher) {
            // $list_token_fcm[] = $teacher->fcm_token;
            // $list_user_id[] = $teacher->id;
            
            $notif =  $this->notificationRepository->create([
                'user_id' => $teacher->id,
                'type' => Notification::TYPE_CLASS,
                'content_id' => $course->id,
                'message' => $body,
                'url' => '/post?classId='.$class->id
            ]);
            if (isset($teacher->fcm_token) && $teacher->fcm_token != '' && $teacher->fcm_token !== null) {
                NotificationService::sendOne($teacher->fcm_token, [
                    'title' => "E-learning thông báo",
                    'body' => ToolService::subWord($body),
                    'icon' => 'https://hdev.info/images/favicon.png',
                    'click_action' => 'https://e-learning.hdev.info/post?classId='.$class->id,
                    'notification_id' => $notif ? $notif->id : false
                ]);
            }
        }

        $students = $class->students;
        if ($students) {
            foreach ($students as $student) {
                if ($student) {
                    // $list_token_fcm[] = $student->fcm_token;
                    // $list_user_id[] = $student->id;
                    
                    $notif =  $this->notificationRepository->create([
                        'user_id' => $student->id,
                        'type' => Notification::TYPE_CLASS,
                        'content_id' => $course->id,
                        'message' => $body,
                        'url' => '/post?classId='.$class->id
                    ]);
                    if (isset($student->fcm_token) && $student->fcm_token != '' && $student->fcm_token !== null) {
                        NotificationService::sendOne($student->fcm_token, [
                            'title' => "E-learning thông báo",
                            'body' => ToolService::subWord($body),
                            'icon' => 'https://hdev.info/images/favicon.png',
                            'click_action' => 'https://e-learning.hdev.info/post?classId='.$class->id,
                            'notification_id' => $notif ? $notif->id : false
                        ]);
                    }
                }
            }
        }

        // if (!empty($list_token_fcm)) {
        //     $body = 'Khóa học "'.$course->name.'" vừa bị xóa khỏi lớp học "'.$class->name.'" của bạn';
        //     NotificationService::sendMulti($list_token_fcm, [
        //         'title' => "E-learning thông báo",
        //         'body' => ToolService::subWord($body),
        //         'icon' => 'https://hdev.info/images/favicon.png',
        //         'click_action' => 'https://e-learning.hdev.info/'
        //     ]);
        //     $this->notificationRepository->createMany($list_user_id, [
        //         'type' => Notification::TYPE_COURSE,
        //         'content_id' => $course->id,
        //         'message' => $body
        //     ]);
        // }
        
        return response()->json([
            'status' => 200,
            'message' => 'Hủy gán lớp học thành công'
        ], 200);
    }
}
