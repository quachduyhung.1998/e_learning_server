<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    public function verify(User $user)
    {
        if ($user->status === 1) {
            abort(404);            
        }
        $user->status = 1;
        $user->save();
        return view('emails.verified_successfully');
    }
}
