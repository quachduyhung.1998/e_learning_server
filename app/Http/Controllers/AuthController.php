<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Mail\ForgotPassword;
use App\Mail\VerifyEmail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register(UserRequest $request)
    {
        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
        
        try {
            // send mail verify
            Mail::to($user)->send(new VerifyEmail($user));
        } catch (\Throwable $e) {
            return $e->getMessage();
        }

        return response()->json([
            'status' => 201,
            'message' => 'Đăng ký thành công, kiểm tra trong hộp thư đến của email bạn đã đăng ký tại E-learning để có thể xác thực tài khoản'
        ], 201);
    }
    
    public function login(Request $request)
    {
        $fields = $request->validate(
            [
                'email' => 'required',
                'password' => 'required',
            ],
            [
                'email.required' => 'Bạn chưa nhập email',
                'password.required' => 'Bạn chưa nhập mật khẩu'
            ]
        );

        // $credentials = request(['email', 'password']);
        // if (!auth()->attempt($credentials)) {
        //     return response()->json([
        //         'status' => 401,
        //         'message' => 'Sai thông tin đăng nhập'
        //     ], 401);
        // }

        $user = User::where('email', $fields['email'])->first();
        if (!$user || !Hash::check($fields['password'], $user->password)) {
            return response()->json([
                'status' => 401,
                'message' => 'Sai thông tin đăng nhập'
            ], 401);
        }

        if ($user->status == 0) {
            return response()->json([
                'status' => 403,
                'message' => 'Tài khoản của bạn chưa được xác thực, kiểm tra trong hộp thư đến của Email bạn đã đăng ký tại E-learning để có thể xác thực tài khoản'
            ], 403);
        }
        
        $token = $user->createToken('elearningtoken')->plainTextToken;

        return response()->json([
            'status' => 200,
            'message' => 'Đăng nhập thành công',
            'user' => $user,
            'access_token' => $token
        ]);
    }

    public function logout()
    {
        // Get user who requested the logout
        // $user = request()->user(); //or Auth::user()
        // Revoke current user token
        // $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();

        request()->user()->currentAccessToken()->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Đăng xuất thành công'
        ]);
    }

    public function forgotPassword(Request $request)
    {
        $fields = $request->validate(
            [
                'email' => 'required|email',
            ],
            [
                'email.required' => 'Bạn chưa nhập email',
                'email.email' => 'Email không đúng định dạng',
            ]
        );

        $user = User::where('email', $fields['email'])->first();
        if (!$user) {
            return response()->json([
                'status' => 404,
                'message' => 'Email của bạn chưa tồn tại trên hệ thống!'
            ], 404);
        }

        $random_password = Str::random(10);
        $user->password = Hash::make($random_password);
        $user->save();

        // xóa hết token cũ
        $user->tokens()->delete();

        try {
            // send mail reset password
            Mail::to($user)->send(new ForgotPassword($user, $random_password));
        } catch (\Throwable $e) {
            return $e->getMessage();
        }

        return response()->json([
            'status' => 200,
            'message' => 'Một email vừa được gửi đến Email của bạn, kiểm tra trong hộp thư đến và làm theo hướng dẫn để lấy lại mật khẩu'
        ], 200);
    }
}
