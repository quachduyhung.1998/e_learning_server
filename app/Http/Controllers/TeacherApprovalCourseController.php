<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeacherApprovalCourseRequest;
use App\Models\Notification;
use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\TeacherApprovalCourse\TeacherApprovalCourseRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Services\NotificationService;
use App\Services\ToolService;
use Illuminate\Http\Request;

class TeacherApprovalCourseController extends Controller
{
    private $teacherApprovalCourseRepository;
    private $userRepository;
    private $courseRepository;
    private $notificationRepository;

    public function __construct(TeacherApprovalCourseRepositoryInterface $teacherApprovalCourseRepository,
        UserRepositoryInterface $userRepository,
        CourseRepositoryInterface $courseRepository,
        NotificationRepositoryInterface $notificationRepository
    ) {
        $this->teacherApprovalCourseRepository = $teacherApprovalCourseRepository;
        $this->userRepository = $userRepository;
        $this->courseRepository = $courseRepository;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = [];
        if ($request->input('course_name') && $request->input('course_name') != '') {
            $filter['course_name'] = $request->input('course_name');
        }
        if ($request->input('user_name') && $request->input('user_name') != '') {
            $filter['user_name'] = $request->input('user_name');
        }

        $datas = $this->teacherApprovalCourseRepository->getAll($filter);

        return response()->json([
            'status' => 200,
            'data' => $datas
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TeacherApprovalCourseRequest $request)
    {
        if (!empty($request->input('user_id'))) {
            $course = $this->courseRepository->findById($request->input('course_id'));
            if (!$course) {
                return response()->json([
                    'status' => 404,
                    'message' => 'Khóa học này không có trên hệ thống'
                ], 404);
            }

            $created = $this->teacherApprovalCourseRepository->create($request->input('course_id'), $request->input('user_id'));
            if (!$created) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Khóa học này đã có người kiểm duyệt'
                ], 403);
            }

            // push notif
            $user = $this->userRepository->findById($request->input('user_id'));
            if ($user) {
                $body = 'Bạn vừa được Admin mời vào kiểm duyệt khóa học "'.$course->name.'", mời bạn truy cập hệ thống để đánh giá và xác minh khóa học!';
                $notif = $this->notificationRepository->create([
                    'user_id' => $user->id,
                    'type' => Notification::TYPE_COURSE_PENDING_APPORVAL,
                    'content_id' => $course->id,
                    'message' => $body,
                    'url' => '/teacher#approve-course'
                ]);
                if (isset($user->fcm_token) && $user->fcm_token != '' && $user->fcm_token !== null) {
                    NotificationService::sendOne($user->fcm_token, [
                        'title' => "Xin chào $user->name",
                        'body' => ToolService::subWord($body),
                        'icon' => 'https://hdev.info/images/favicon.png',
                        'click_action' => 'https://e-learning.hdev.info/teacher#approve-course',
                        'notification_id' => $notif ? $notif->id : false
                    ]);
                }
            }


            return response()->json([
                'status' => 201,
                'message' => 'Thêm người kiểm duyệt vào khóa học thành công'
            ], 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->teacherApprovalCourseRepository->findById($id);
        if (!$data) {
            return response()->json([
                'status' => 404,
                'message' => 'Khóa học này chưa có người kiểm duyệt'
            ], 404);
        }
        
        $data->delete();
        return response()->json([
            'status' => 200,
            'message' => 'Xóa người kiểm duyệt thành công thành công'
        ], 200);
    }
}
