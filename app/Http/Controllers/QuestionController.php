<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionRequest;
use App\Repositories\Question\QuestionRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class QuestionController extends Controller
{
    private $questionRepository;

    public function __construct(QuestionRepositoryInterface $questionRepository)
    {
        $this->questionRepository = $questionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(QuestionRequest $request)
    {
        $checkQuestionExists = $this->questionRepository->findByName($request->input('name'));
        if (count($checkQuestionExists) > 0) {
            return response()->json([
                'status' => 403,
                'message' => 'Câu hỏi này đã tồn tại trên hệ thống'
            ], 403);
        }

        $attr = [
            'name' => $request->input('name'),
            'course_category_id' => $request->input('course_category_id'),
            'type_question' => $request->input('type_question'),
            'answers' => $request->input('answers'),
            'mark' => $request->input('mark'),
        ];

        $question = $this->questionRepository->create($attr);
        if (!$question) {
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }

        return response()->json([
            'status' => 200,
            'message' => 'Tạo câu hỏi thành công',
            'data' => $question->format($question)
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $question = $this->questionRepository->findById($id);
        if (!$question) {
            return response()->json([
                'status' => 404,
                'message' => 'Câu hỏi này chưa tồn tại trên hệ thống'
            ], 404);
        }
        
        return response()->json([
            'status' => 200,
            'data' => $question->format($question)
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(QuestionRequest $request, $id)
    {
        $checkQuestionExists = $this->questionRepository->findById($id);
        if (!$checkQuestionExists) {
            return response()->json([
                'status' => 404,
                'message' => 'Câu hỏi này chưa tồn tại trên hệ thống'
            ], 404);
        }

        // policy
        $response = Gate::inspect('update', $checkQuestionExists);
        if (!$response->allowed()) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }

        $attr = [
            'name' => $request->input('name'),
            'course_category_id' => $request->input('course_category_id'),
            'type_question' => $request->input('type_question'),
            'answers' => $request->input('answers'),
            'mark' => $request->input('mark'),
        ];

        $question = $this->questionRepository->update($id, $attr);
        if (!$question) {
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }

        return response()->json([
            'status' => 200,
            'message' => 'Cập nhật câu hỏi thành công',
            'data' => $question->format($question)
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkQuestionExists = $this->questionRepository->findById($id);
        if (!$checkQuestionExists) {
            return response()->json([
                'status' => 404,
                'message' => 'Câu hỏi này chưa tồn tại trên hệ thống'
            ], 404);
        }

        // policy
        $response = Gate::inspect('update', $checkQuestionExists);
        if (!$response->allowed()) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }

        $deleted = $this->questionRepository->delete($id);
        if (!$deleted) {
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }

        return response()->json([
            'status' => 200,
            'message' => 'Xóa câu hỏi thành công'
        ], 200);
    }
}
