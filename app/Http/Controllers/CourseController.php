<?php

namespace App\Http\Controllers;

use App\Http\Requests\CourseRequest;
use App\Models\Course;
use App\Models\Notification;
use App\Models\TestResult;
use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\StudentCompleteLesson\StudentCompleteLessonRepositoryInterface;
use App\Repositories\TeacherApprovalCourse\TeacherApprovalCourseRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Repositories\UserViewHistory\UserViewHistoryRepositoryInterface;
use App\Services\NotificationService;
use App\Services\ToolService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class CourseController extends Controller
{
    private $courseRepository;
    private $userViewHistoryRepository;
    private $teacherApprovalCourseRepository;
    private $studentCompleteLessonRepository;
    private $notificationRepository;
    private $userRepository;

    public function __construct(
        CourseRepositoryInterface $courseRepository,
        UserViewHistoryRepositoryInterface $userViewHistoryRepository,
        TeacherApprovalCourseRepositoryInterface $teacherApprovalCourseRepository,
        StudentCompleteLessonRepositoryInterface $studentCompleteLessonRepository,
        NotificationRepositoryInterface $notificationRepository,
        UserRepositoryInterface $userRepository
    )
    {
        $this->courseRepository = $courseRepository;
        $this->userViewHistoryRepository = $userViewHistoryRepository;
        $this->teacherApprovalCourseRepository = $teacherApprovalCourseRepository;
        $this->studentCompleteLessonRepository = $studentCompleteLessonRepository;
        $this->notificationRepository = $notificationRepository;
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        $courses = $this->courseRepository->getAll($request->name, $request->category);
        if (count($courses) === 0) {
            return response()->json([
                'status' => 200,
                'data' => []
            ], 200);
        }

        $list_course = [];
        foreach ($courses as $course) {
            $percent_progress = $this->courseRepository->getProgressOfUser($course->id);
            $list_course[] = $course->format($course, $percent_progress);
        }

        return response()->json([
            'status' => 200,
            'data' => ToolService::handleCustomPagination($courses, $list_course)
        ], 200);
    }

    public function store(CourseRequest $request)
    {
        $attr = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'image' => $request->input('image'),
            'course_category_id' => $request->input('course_category_id'),
            'is_sequence' => $request->input('is_sequence')
        ];

        $course = $this->courseRepository->create($attr);
        if (!$course) {
            // xóa ảnh vừa upload
            ToolService::handleDeleteImage(last(explode('/', $request->input('image'))));
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }

        return response()->json([
            'status' => 200,
            'message' => 'Tạo khóa học thành công',
            'data' => $course->format($course)
        ], 201);
    }

    public function show($id)
    {
        $course = $this->courseRepository->findById($id);
        if (!$course) {
            return response()->json([
                'status' => 404,
                'message' => 'Khóa học này không có trên hệ thống'
            ], 404);
        }

        // nếu là sinh viên
        // kiểm tra sinh viên này trong lớp nào và lớp này có được gán khóa học này vào không
        if (request()->user()->isStudent()) {
            $list_course_id = request()->user()->getListCourseId();
            if (!in_array($course->id, $list_course_id)) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không có quyền thực hiện hành động này'
                ], 403);
            }

            // lưu user view history
            $this->userViewHistoryRepository->saveUserViewCourse($course->id);
        }

        $percent_progress = $this->courseRepository->getProgressOfUser($course->id);
        $data = $course->format($course, $percent_progress);
        $data['lessons'] = $course->lessons()->orderBy('order', 'asc')->get();
        for ($i = 0; $i < count($data['lessons']); $i++) {
            $test = $data['lessons'][$i]->test;

            $data['lessons'][$i]['test'] = $test;

            if (request()->user()->isStudent()) {
                $data['lessons'][$i]['is_learned'] = false;
                // check xem user này đã học bài học này chưa
                $is_learned = $this->studentCompleteLessonRepository->findByLessonId($data['lessons'][$i]->id);
                if ($is_learned) {
                    $data['lessons'][$i]['is_learned'] = true;
                }

                // check xem user này đã làm bài test này chưa
                if ($test) {
                    $data['lessons'][$i]['test']['is_learned'] = false;
                    $result_worked = TestResult::query()
                        ->where('user_id', auth()->user()->id)
                        ->where('test_id', $test->id)
                        ->first();
                    if ($result_worked) {
                        $data['lessons'][$i]['test']['is_learned'] = true;
                    }
                }
            }
        };

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    public function update(CourseRequest $request, $id)
    {
        $course = $this->courseRepository->findById($id);
        if ($course) {
            $response = Gate::inspect('update', $course);

            if ($response->allowed()) {
                $attr = [
                    'name' => $request->input('name'),
                    'description' => $request->input('description'),
                    'image' => $request->input('image'),
                    'course_category_id' => $request->input('course_category_id'),
                    'is_sequence' => $request->input('is_sequence')
                ];

                if ($request->input('image') == null || $course->image != $request->input('image')) {
                    // xóa ảnh cũ
                    ToolService::handleDeleteImage(last(explode('/', $course->image)));
                }

                $course_updated = $this->courseRepository->update($id, $attr);
                if (!$course_updated) {
                    return response()->json([
                        'status' => 500,
                        'message' => 'Lỗi máy chủ'
                    ], 500);
                }

                return response()->json([
                    'status' => 200,
                    'message' => 'Cập nhật khóa học thành công',
                    'data' => $course_updated->format($course_updated)
                ], 201);
            }

            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }

        return response()->json([
            'status' => 404,
            'message' => 'Khóa học này hiện không có trên hệ thống'
        ], 404);
    }

    public function getAllMyCourse(Request $request)
    {
        // nếu là sinh viên
        // kiểm tra sinh viên này trong lớp nào và lớp này có được gán khóa học này vào không
        if (request()->user()->isStudent()) {
            $list_course_id = request()->user()->getListCourseId();
            $courses = $this->courseRepository->getAllCourseOfTeacher(
                $request->only(['name', 'category', 'status']),
                true,
                $list_course_id
            );
        }
        else {
            $courses = $this->courseRepository->getAllCourseOfTeacher($request->only(['name', 'category', 'status']));
        }

        if (count($courses) === 0) {
            return response()->json([
                'status' => 200,
                'data' => []
            ], 200);
        }

        $list_course = [];
        foreach ($courses as $course) {
            $percent_progress = $this->courseRepository->getProgressOfUser($course->id);
            $list_course[] = $course->format($course, $percent_progress);
        }

        return response()->json([
            'status' => 200,
            'data' => ToolService::handleCustomPagination($courses, $list_course)
        ], 200);
    }

    public function getAllCoursePendingApproval(Request $request)
    {
        $list_course_approval = $this->teacherApprovalCourseRepository->getAllCourseApproval();
        if (empty($list_course_approval)) {
            return response()->json([
                'status' => 200,
                'data' => []
            ], 200);
        }

        $list_course = [];
        $list_course_id = [];
        foreach ($list_course_approval as $course_approval) {
            $course = $this->courseRepository->findById($course_approval->course_id);
            if ($course) {
                $list_course_id[] = $course_approval->course_id;
                $list_course[] = [
                    'course_id' => $course_approval->course_id,
                    'course_name' => $course->name,
                    'course_description' => $course->description,
                    'course_image' => $course->image,
                    'course_category' => $course->course_category_id,
                    'total_lesson' => $course->total_lesson,
                    'created_at' => $course->created_at,
                    'created_by' => [
                        'id' => $course->user->id,
                        'name' => $course->user->name,
                        'email' => $course->user->email,
                        'role' => $course->user->role,
                    ],
                    'teacher_approval' => [
                        'id' => $course_approval->user->id,
                        'name' => $course_approval->user->name,
                        'email' => $course_approval->user->email,
                        'role' => $course_approval->user->role,
                    ],
                    'request_approval_at' => $course_approval->created_at
                ];
            }
        }

        if (request()->user()->isAdmin() || request()->user()->isSuperAdmin()) {
            $list_course_complete = Course::whereNotIn('id', $list_course_id)->where('status', Course::STATUS_COMPLETE)->orderBy('updated_at', 'desc')->get();
            if ($list_course_complete) {
                foreach ($list_course_complete as $course) {
                    $course_approval = $this->teacherApprovalCourseRepository->findByIdCourse($course->id);
                    $list_course[] = [
                        'course_id' => $course->id,
                        'course_name' => $course->name,
                        'course_description' => $course->description,
                        'course_image' => $course->image,
                        'course_category' => $course->course_category_id,
                        'total_lesson' => $course->total_lesson,
                        'created_at' => $course->created_at,
                        'created_by' => [
                            'id' => $course->user->id,
                            'name' => $course->user->name,
                            'email' => $course->user->email,
                            'role' => $course->user->role,
                        ],
                        'teacher_approval' => empty($course_approval)
                            ? null
                            : [
                                'id' => $course_approval->user->id,
                                'name' => $course_approval->user->name,
                                'email' => $course_approval->user->email,
                                'role' => $course_approval->user->role,
                            ]
                        ,
                        'request_approval_at' => $course->updated_at
                    ];
                }
            }
        }

        return response()->json([
            'status' => 200,
            'data' => $list_course
        ], 200);
    }

    public function getAllCourseApproved()
    {
        $list_course_approval = $this->teacherApprovalCourseRepository->getAllCourseApproved();
        if (empty($list_course_approval)) {
            return response()->json([
                'status' => 200,
                'data' => []
            ], 200);
        }

        $list_course = [];
        foreach ($list_course_approval as $course_approval) {
            $course = $this->courseRepository->findById($course_approval->course_id);
            if ($course) {
                $list_course[] = [
                    'course_id' => $course_approval->course_id,
                    'course_name' => $course->name,
                    'course_description' => $course->description,
                    'course_category' => $course->course_category_id,
                    'total_lesson' => $course->total_lesson,
                    'created_by' => [
                        'id' => $course->user->id,
                        'name' => $course->user->name,
                        'email' => $course->user->email,
                        'role' => $course->user->role,
                    ],
                    'request_approval_at' => $course_approval->created_at,
                    'approval_at' => $course_approval->updated_at,
                ];
            }
        }

        return response()->json([
            'status' => 200,
            'data' => $list_course
        ], 200);
    }

    public function getAllTest($id)
    {
        $course = $this->courseRepository->findById($id);
        if (!$course) {
            return response()->json([
                'status' => 404,
                'message' => 'Khóa học này hiện không có trên hệ thống'
            ], 404);
        }

        return response()->json([
            'status' => 200,
            'data' => $course->tests
        ], 200);
    }

    public function updateStatusComplete($id)
    {
        $course = $this->courseRepository->findById($id);
        if (!$course) {
            return response()->json([
                'status' => 404,
                'message' => 'Khóa học này hiện không có trên hệ thống'
            ], 404);
        }

        if ($course->status === Course::STATUS_DRAFT) {
            $response = Gate::inspect('update', $course);
            if ($response->allowed()) {
                if ($this->courseRepository->handleUpdateStatusComplete($id)) {
                    $admin = $this->userRepository->getAdmin();
                    if ($admin) {
                        $body = auth()->user()->name.' vừa yêu cầu phê duyệt khóa học: '.$course->name;
                        $notif = $this->notificationRepository->create([
                            'user_id' => $admin->id,
                            'type' => Notification::TYPE_COURSE_PENDING_APPORVAL,
                            'content_id' => $course->id,
                            'message' => $body,
                            'url' => '/admin/reviewing-courses'
                        ]);
                        if (isset($admin->fcm_token) && $admin->fcm_token != '' && $admin->fcm_token !== null) {
                            NotificationService::sendOne($admin->fcm_token, [
                                'title' => "Xin chào $admin->name",
                                'body' => ToolService::subWord($body),
                                'icon' => 'https://hdev.info/images/favicon.png',
                                'click_action' => 'https://e-learning.hdev.info/admin/reviewing-courses',
                                'notification_id' => $notif ? $notif->id : false
                            ]);
                        }
                    }

                    return response()->json([
                        'status' => 200,
                        'message' => 'Cập nhật trạng thái thành công'
                    ], 200);
                }
            }

            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        } else {
            return response()->json([
                'status' => 200,
                'message' => 'Khóa học này đã được phê duyệt'
            ], 200);
        }
    }

    public function updateStatusPublic($id)
    {
        $course = $this->courseRepository->findById($id);
        if (!$course) {
            return response()->json([
                'status' => 404,
                'message' => 'Khóa học này hiện không có trên hệ thống'
            ], 404);
        }

        $checkPermission = $this->courseRepository->checkPermission($id);
        if (!$checkPermission) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }

        if ($this->courseRepository->handleUpdateStatusPublic($id)) {
            $this->teacherApprovalCourseRepository->updateStatusApproved($id);

            // push notif
            $user = $course->user;
            if ($user) {
                $body = 'Khóa học "'.$course->name.'" của bạn đã được phê duyệt, giờ đây mọi người đều có thể nhìn thấy khóa học của bạn trên hệ thống!';
                $notif = $this->notificationRepository->create([
                    'user_id' => $user->id,
                    'type' => Notification::TYPE_COURSE,
                    'content_id' => $course->id,
                    'message' => $body,
                    'url' => '/teacher/course/'.$course->id
                ]);
                if (isset($user->fcm_token) && $user->fcm_token != '' && $user->fcm_token !== null) {
                    NotificationService::sendOne($user->fcm_token, [
                        'title' => "Xin chào $user->name",
                        'body' => ToolService::subWord($body),
                        'icon' => 'https://hdev.info/images/favicon.png',
                        'click_action' => 'https://e-learning.hdev.info/teacher/course/'.$course->id,
                        'notification_id' => $notif ? $notif->id : false
                    ]);
                }
            }

            return response()->json([
                'status' => 200,
                'message' => 'Cập nhật trạng thái thành công'
            ], 200);
        }
    }

    public function updateStatusSequence($id)
    {
        $course = $this->courseRepository->findById($id);
        if ($course) {
            $response = Gate::inspect('update', $course);

            if ($response->allowed()) {
                if ($this->courseRepository->handleUpdateStatusSequence($id)) {
                    return response()->json([
                        'status' => 200,
                        'message' => 'Cập nhật thành công'
                    ], 200);
                }
            }

            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }

        return response()->json([
            'status' => 404,
            'message' => 'Khóa học này hiện không có trên hệ thống'
        ], 404);
    }

    public function destroy($id)
    {
        $course = $this->courseRepository->findById($id);
        if ($course) {
            // $this->authorize('delete', $course);
            $response = Gate::inspect('delete', $course);

            if ($response->allowed()) {
                $deleted = $this->courseRepository->delete($id);

                if (!$deleted) {
                    return response()->json([
                        'status' => 500,
                        'message' => 'Lỗi máy chủ'
                    ], 500);
                }

                return response()->json([
                    'status' => 200,
                    'message' => 'Xóa khóa học thành công'
                ], 200);
            }

            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }

        return response()->json([
            'status' => 404,
            'message' => 'Khóa học này hiện không có trên hệ thống'
        ], 404);
    }
}
