<?php

namespace App\Http\Controllers;

use App\Http\Requests\LessonRequest;
use App\Models\TestResult;
use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\Lesson\LessonRepositoryInterface;
use App\Repositories\StudentAnswer\StudentAnswerRepositoryInterface;
use App\Repositories\UserViewHistory\UserViewHistoryRepositoryInterface;
use App\Services\ToolService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class LessonController extends Controller
{
    private $lessonRepository;
    private $courseRepository;
    private $userViewHistoryRepository;
    private $studentAnswerRepository;

    public function __construct(
        LessonRepositoryInterface $lessonRepository,
        CourseRepositoryInterface $courseRepository,
        UserViewHistoryRepositoryInterface $userViewHistoryRepository,
        StudentAnswerRepositoryInterface $studentAnswerRepository
    )
    {
        $this->lessonRepository = $lessonRepository;
        $this->courseRepository = $courseRepository;
        $this->userViewHistoryRepository = $userViewHistoryRepository;
        $this->studentAnswerRepository = $studentAnswerRepository;
    }

    public function index()
    {
        //
    }

    public function store(LessonRequest $request)
    {
        $course = $this->courseRepository->findById($request->input('course_id'));
        if ($course) {
            // CoursePolicy
            $response = Gate::inspect('create', $course);

            if ($response->allowed()) {
                $attr = [
                    'name' => $request->input('name'),
                    'content' => $request->input('content'),
                    'description' => $request->input('description'),
                    'image' => $request->input('image'),
                    'order' => $request->input('order'),
                    'course_id' => $course->id,
                    'type' => $request->input('type'),
                ];

                $lesson = $this->lessonRepository->create($attr);
                if (!$lesson) {
                    // xóa ảnh vừa upload
                    ToolService::handleDeleteImage(last(explode('/', $request->input('image'))));
                    return response()->json([
                        'status' => 500,
                        'message' => 'Lỗi máy chủ'
                    ], 500);
                }

                $this->courseRepository->handleUpdateTotalLesson($course->id, 'plus');
                return response()->json([
                    'status' => 201,
                    'message' => 'Tạo bài học thành công',
                    'data' => $lesson
                ], 201);
            }

            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }
        
        return response()->json([
            'status' => 404,
            'message' => 'Khóa học này hiện không có trên hệ thống'
        ], 404);
    }

    public function show($id)
    {
        $lesson = $this->lessonRepository->findById($id);
        if (!$lesson) {
            return response()->json([
                'status' => 404,
                'message' => 'Bài học này không có trên hệ thống'
            ], 404);
        }

        // nếu là sinh viên
        // kiểm tra sinh viên này trong lớp nào và lớp này có được gán khóa học này vào không
        if (request()->user()->isStudent()) {
            $list_course_id = request()->user()->getListCourseId();
            if (!in_array($lesson->course_id, $list_course_id)) {
                return response()->json([
                    'status' => 403,
                    'message' => 'Bạn không có quyền thực hiện hành động này'
                ], 403);
            }
            
            // lưu user view history
            $this->userViewHistoryRepository->saveUserViewLesson($lesson->id);
        }

        $data = $lesson;
        $test = $lesson->test;

        $number_time_worked = $this->studentAnswerRepository->getTimeWorkedTest($test['id']);
        
        $result_worked = TestResult::query()
            ->where('user_id', auth()->user()->id)
            ->where('test_id', $test['id'])
            ->where('number_time_worked', $number_time_worked)
            ->first();
        if ($result_worked) {
            $test['result'] = [
                'mark_achieved' => $result_worked->mark_achieved,
                'total_answer_right' => $result_worked->total_answer_right,
                'number_time_worked' => $result_worked->number_time_worked,
                'time_start' => $result_worked->time_start,
                'time_finish' => $result_worked->time_finish,
                'total_time_work' => $result_worked->total_time_work,
            ];
        }

        $data['test'] = $test; 

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    public function update(LessonRequest $request, $id)
    {
        $lesson = $this->lessonRepository->findById($id);
        if ($lesson) {
            $response = Gate::inspect('update', $lesson);

            if ($response->allowed()) {
                $attr = [
                    'name' => $request->input('name'),
                    'description' => $request->input('description'),
                    'content' => $request->input('content'),
                    'image' => $request->input('image'),
                    'order' => $request->input('order'),
                    'type' => $request->input('type'),
                ];

                if ($request->input('image') == null || $lesson->image != $request->input('image')) {
                    // xóa ảnh cũ
                    ToolService::handleDeleteImage(last(explode('/', $lesson->image)));
                }

                $lesson_updated = $this->lessonRepository->update($id, $attr);
                if (!$lesson_updated) {
                    return response()->json([
                        'status' => 500,
                        'message' => 'Lỗi máy chủ'
                    ], 500);
                }

                return response()->json([
                    'status' => 200,
                    'message' => 'Cập nhật bài học thành công',
                    'data' => $lesson_updated
                ], 200);
            }

            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }

        return response()->json([
            'status' => 404,
            'message' => 'Bài học này hiện không có trên hệ thống'
        ], 404);
    }

    public function changeOrder(Request $request, $id)
    {
        $request->validate(
            [
                'order' => 'required',
            ],
            [
                'order.required' => 'Thiếu tham số order',
            ]
        );

        $lesson = $this->lessonRepository->findById($id);
        if ($lesson) {
            $response = Gate::inspect('update', $lesson);

            if ($response->allowed()) {
                $order = $request->input('order');
                $changed = $this->lessonRepository->handleChangeOrder($id, $order);
                if (!$changed) {
                    return response()->json([
                        'status' => 500,
                        'message' => 'Lỗi máy chủ'
                    ], 500);
                }

                return response()->json([
                    'status' => 200,
                    'message' => 'Cập nhật thứ tự bài học thành công',
                    'data' => $changed
                ], 200);
            }

            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }

        return response()->json([
            'status' => 404,
            'message' => 'Bài học này hiện không có trên hệ thống'
        ], 404);
    }

    public function destroy($id)
    {
        $lesson = $this->lessonRepository->findById($id);
        if ($lesson) {
            // $this->authorize('delete', $course);
            $response = Gate::inspect('delete', $lesson);

            if ($response->allowed()) {
                $deleted = $this->lessonRepository->delete($id);

                if (!$deleted) {
                    return response()->json([
                        'status' => 500,
                        'message' => 'Lỗi máy chủ'
                    ], 500);
                }

                $this->courseRepository->handleUpdateTotalLesson($lesson->course->id, 'minus');
                return response()->json([
                    'status' => 200,
                    'message' => 'Xóa bài học thành công'
                ], 200);
            }

            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền thực hiện hành động này!!!'
            ], 403);
        }

        return response()->json([
            'status' => 404,
            'message' => 'Khóa học này hiện không có trên hệ thống'
        ], 404);
    }
}
