<?php

namespace App\Http\Controllers;

use App\Events\ChatEvent;
use App\Http\Requests\ChatGroupRequest;
use App\Http\Requests\ChatPrivateRequest;
use App\Models\ChatMember;
use App\Models\ChatMessage;
use App\Models\ChatRoom;
use App\Models\User;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        $rooms = auth()->user()->rooms;
        if (empty($rooms)) {
            return response()->json([
                'status' => 200,
                'data' => null
            ], 200);
        }

        $data = [];
        foreach ($rooms as $room) {
            $message_format = null;

            $message = ChatMessage::query()
                ->where('chat_room_id', $room->id)
                ->orderBy('id', 'desc')
                ->first();
            
            if ($message) {
                $sender = [
                    'id' => $message->user->id,
                    'name' => $message->user->name,
                    'email' => $message->user->email,
                    'avatar' => $message->user->avatar,
                    'role' => $message->user->role
                ];
                $message_format = [
                    'id' => $message->id,
                    'chat_room_id' => $message->chat_room_id,
                    'message' => $message->message,
                    'type' => $message->type ? ($message->type === null ? 0 : 1) : 0,
                    'sender' => $sender,
                    'created_at' => $message->created_at,
                    'updated_at' => $message->updated_at,
                ];
            }
            
            $is_group = true;
            $send_to_user = false;
            if ($room->type === ChatRoom::ROOM_TYPE_PRIVATE) {
                $is_group = false;
                $members = $room->members;
                foreach ($members as $member) {
                    if ($member->id != auth()->user()->id) {
                        $send_to_user = $member;
                    }
                }
                if ($send_to_user) {
                    $send_to_user = [
                        'id' => $send_to_user->id,
                        'name' => $send_to_user->name,
                        'email' => $send_to_user->email,
                        'avatar' => $send_to_user->avatar,
                        'role' => $send_to_user->role
                    ];
                }
            }

            $data[] = [
                'is_group' => $is_group,
                'room_id' => $room->id,
                'room_name' => $room->name,
                'room_type' => $room->type,
                'room_created_at' => $room->created_at,
                'room_updated_at' => $room->updated_at,
                'send_to_user' => $send_to_user ?: null,
                'last_message' => $message_format
            ];
        }
        
        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function chatPrivate(ChatPrivateRequest $request)
    {
        if (auth()->user()->id == $request->send_to_user_id) {
            return;
        }

        $send_to_user = User::find($request->send_to_user_id);
        if (!$send_to_user) {
            return response()->json([
                'status' => 404,
                'message' => 'User này không có trong hệ thống'
            ], 404);
        }

        $sender = auth()->user();
        $chat_room = false;

        $list_room_id_of_sender = [];
        $list_room_of_sender = auth()->user()->rooms;
        if (count($list_room_of_sender) > 0) {
            foreach ($list_room_of_sender as $room) {
                $list_room_id_of_sender[] = $room->id;
            }
        }

        // nếu người gửi chưa có phòng chat nào thì tạo phòng mới
        if (empty($list_room_id_of_sender)) {
            $chat_room = new ChatRoom();
            $chat_room->name = '';
            $chat_room->type = ChatRoom::ROOM_TYPE_PRIVATE;
            $chat_room->save();

            self::insertMemberChatPrivate([$send_to_user->id, $sender->id], $chat_room->id);
        }
        else { // check phòng chat
            $list_room_id_of_receiver = [];
            $list_room_of_receiver = $send_to_user->rooms;
            if (count($list_room_of_receiver) > 0) {
                foreach ($list_room_of_receiver as $room) {
                    $list_room_id_of_receiver[] = $room->id;
                }
            }
            // nếu người nhận chưa có phòng chat nào thì tạo phòng mới
            if (empty($list_room_id_of_receiver)) {
                $chat_room = new ChatRoom();
                $chat_room->name = '';
                $chat_room->type = ChatRoom::ROOM_TYPE_PRIVATE;
                $chat_room->save();

                self::insertMemberChatPrivate([$send_to_user->id, $sender->id], $chat_room->id);
            }
            else { // check phòng chat
                // lấy các room id trùng nhau trong 2 mảng
                $list_room_id = array_intersect($list_room_id_of_sender, $list_room_id_of_receiver);
                if (empty($list_room_id)) {
                    $chat_room = new ChatRoom();
                    $chat_room->name = '';
                    $chat_room->type = ChatRoom::ROOM_TYPE_PRIVATE;
                    $chat_room->save();

                    self::insertMemberChatPrivate([$send_to_user->id, $sender->id], $chat_room->id);
                }
                else {
                    // lấy danh sách phòng chat riêng
                    $list_room_private = ChatRoom::whereIn('id', $list_room_id)->where('type', ChatRoom::ROOM_TYPE_PRIVATE)->get();
                    if (empty($list_room_private) || !$list_room_private || count($list_room_private) == 0) {
                        $chat_room = new ChatRoom();
                        $chat_room->name = '';
                        $chat_room->type = ChatRoom::ROOM_TYPE_PRIVATE;
                        $chat_room->save();

                        self::insertMemberChatPrivate([$send_to_user->id, $sender->id], $chat_room->id);
                    }
                    else {
                        if (count($list_room_private) == 1) {
                            $chat_room = isset($list_room_private[0]) ? $list_room_private[0] : false;
                        } else {
                            // dd($list_room_private);
                            foreach ($list_room_private as $room) {
                                $members = $room->members;
                                if (count($members) == 2) {
                                    $total_id_member = $members[0]->user_id + $members[1]->user_id;
                                    $total_id_sender_and_receiver = $request->send_to_user_id + $sender->id;
                                    if ($total_id_member === $total_id_sender_and_receiver) {
                                        $chat_room = $room;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if ($chat_room && !empty($chat_room)) {
            $chat_message = new ChatMessage();
            $chat_message->user_id = $sender->id;
            $chat_message->chat_room_id = $chat_room->id;
            $chat_message->message = $request->message;
            $chat_message->type = (int) $request->type;
            $chat_message->save();
            try {
                $data = [
                    'message' => $request->message,
                    'type' => (int) $request->type,
                    'room' => null,
                    'sender' => [
                        'id' => $sender->id,
                        'name' => $sender->name,
                        'email' => $sender->email,
                        'avatar' => $sender->avatar,
                        'role' => $sender->role,
                    ],
                ];
                broadcast(new ChatEvent($send_to_user, $data));
                // broadcast(new ChatEvent($send_to_user, $request->message))->toOthers();

                return response()->json([
                    'status' => 200,
                    'message' => 'Đã gửi',
                    'data' => $chat_room
                ], 200);
            } catch (\Throwable $th) {
                throw $th;
            }
        } else {
            return response()->json([
                'status' => 500,
                'message' => 'Fail'
            ], 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeGroup(Request $request)
    {
        $fields = $request->validate(
            [
                'name' => 'required',
                'list_member' => 'string'
            ],
            [
                'name.required' => 'Bạn chưa nhập tên nhóm chat',
                'list_member.string' => 'list_member phải là 1 chuỗi',
            ]
        );
        
        $data = [
            'name' => $fields['name'],
            'type' => ChatRoom::ROOM_TYPE_GROUP,
            'user_id' => auth()->user()->id
        ];
        
        try {
            $chat_room = ChatRoom::create($data);
            if ($chat_room) {
                $created_by = ChatMember::create([
                    'user_id' => auth()->user()->id,
                    'chat_room_id' => $chat_room->id
                ]);
                if ($created_by) {
                    $list_member_saved[] = [
                        'id' => auth()->user()->id,
                        'name' => auth()->user()->name,
                        'email' => auth()->user()->email,
                        'avatar' => auth()->user()->avatar,
                        'role' => auth()->user()->role,
                    ];
                }

                if ($request->input('list_member') != '') {
                    $list_member_saved = [];
                    $total_member = 0;
                    $list_member = json_decode($request->input('list_member'), true);
                    foreach ($list_member as $member_id) {
                        $find_member = User::find($member_id);
                        if ($find_member) {
                            $check_exist_member = ChatMember::query()
                                ->where('chat_room_id', $chat_room->id)
                                ->where('user_id', $member_id)
                                ->first();
                            
                            if (!$check_exist_member) {                            
                                $member = ChatMember::create([
                                    'user_id' => $member_id,
                                    'chat_room_id' => $chat_room->id
                                ]);
                                if ($member) {
                                    $total_member++;
                                    $list_member_saved[] = [
                                        'id' => $member->user->id,
                                        'name' => $member->user->name,
                                        'email' => $member->user->email,
                                        'avatar' => $member->user->avatar,
                                        'role' => $member->user->role,
                                    ];
                                }
                            }
                        }
                    }
                    
                    $chat_room->total_member = $total_member;
                    $chat_room->save();
                    
                    unset($chat_room['user_id']);
                    $chat_room['administrator'] = [
                        'id' => auth()->user()->id,
                        'name' => auth()->user()->name,
                        'email' => auth()->user()->email,
                        'avatar' => auth()->user()->avatar,
                        'role' => auth()->user()->role,
                    ];
                    $chat_room['list_member'] = $list_member_saved;
                }
                return response()->json([
                    'status' => 201,
                    'message' => 'Tạo nhóm chat thành công',
                    'data' => $chat_room
                ], 201);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function chatGroup(ChatGroupRequest $request)
    {
        $sender = auth()->user();
        $chat_room = ChatRoom::find($request->input('room_id'));
        if ($chat_room && !empty($chat_room)) {
            $chat_message = new ChatMessage();
            $chat_message->user_id = $sender->id;
            $chat_message->chat_room_id = $chat_room->id;
            $chat_message->message = $request->message;
            $chat_message->type = (int) $request->type;
            $chat_message->save();
            try {
                $list_member = [];
                $list_member_in_group = $chat_room->members;
                foreach ($list_member_in_group as $member) {
                    if ($member->id === $chat_room->administrator->id) {
                        $list_member[] = [
                            'is_administrator' => true,
                            'id' => $member->id,
                            'name' => $member->name,
                            'email' => $member->email,
                            'avatar' => $member->avatar,
                            'role' => $member->role,
                        ];
                    } else {
                        $list_member[] = [
                            'id' => $member->id,
                            'name' => $member->name,
                            'email' => $member->email,
                            'avatar' => $member->avatar,
                            'role' => $member->role,
                        ];
                    }
                }
                $data = [
                    'message' => $request->message,
                    'type' => (int) $request->type,
                    'room' => [
                        'id' => $chat_room->id,
                        'name' => $chat_room->name,
                        'total_member' => $chat_room->total_member,
                        'administrator' => [
                            'id' => $chat_room->administrator->id,
                            'name' => $chat_room->administrator->name,
                            'email' => $chat_room->administrator->email,
                            'avatar' => $chat_room->administrator->avatar,
                            'role' => $chat_room->administrator->role,
                        ],
                        'members' => $list_member
                    ],
                    'sender' => [
                        'id' => $sender->id,
                        'name' => $sender->name,
                        'email' => $sender->email,
                        'avatar' => $sender->avatar,
                        'role' => $sender->role,
                    ],
                ];

                if (!empty($list_member)) {
                    foreach ($list_member_in_group as $member) {
                        if ($member->id != auth()->user()->id) {
                            broadcast(new ChatEvent($member, $data));
                            // broadcast(new ChatEvent($member, $data))->toOthers();
                        }
                    }
                }

                return response()->json([
                    'status' => 200,
                    'message' => 'Đã gửi'
                ], 200);
            } catch (\Throwable $th) {
                throw $th;
            }
        }
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addMember($room_id, Request $request)
    {
        $fields = $request->validate(
            [
                'user_id' => 'required',
            ],
            [
                'user_id.required' => 'Thiếu tham số user_id',
            ]
        );

        $chat_room = ChatRoom::find($room_id);
        if (!$chat_room) {
            return response()->json([
                'status' => 404,
                'message' => 'Nhóm chat này không có trên hệ thống'
            ], 404);
        }

        if ($chat_room->type == ChatRoom::ROOM_TYPE_PRIVATE) {
            return response()->json([
                'status' => 403,
                'message' => 'Đây không phải là một group chat'
            ], 403);
        }

        $response = Gate::inspect('create', $chat_room);
        if ($response->allowed()) {
            $check_exist_member = ChatMember::where('chat_room_id', $room_id)->where('user_id', $fields['user_id'])->first();
            if ($check_exist_member) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Thành viên này đã có trong nhóm'
                ], 200);
            }

            ChatMember::create([
                'user_id' => $fields['user_id'],
                'chat_room_id' => $room_id
            ]);

            $chat_room->total_member = $chat_room->total_member + 1;
            $chat_room->save();

            return response()->json([
                'status' => 200,
                'message' => 'Thêm thành công'
            ], 200);
        }

        return response()->json([
            'status' => 403,
            'message' => 'Bạn không phải thành viên trong nhóm nên không có quyền thêm'
        ], 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showMember($room_id)
    {
        $chat_room = ChatRoom::find($room_id);
        if (!$chat_room) {
            return response()->json([
                'status' => 404,
                'message' => 'Nhóm chat này không có trên hệ thống'
            ], 404);
        }

        if ($chat_room->type == ChatRoom::ROOM_TYPE_PRIVATE) {
            return response()->json([
                'status' => 403,
                'message' => 'Đây không phải là một group chat'
            ], 403);
        }

        $response = Gate::inspect('showMember', $chat_room);
        if ($response->allowed()) {
            $members = [];
            $list_member = $chat_room->members;
            foreach ($list_member as $member) {
                if ($member->id === $chat_room->administrator->id) {
                    $members[] = [
                        'is_administrator' => true,
                        'id' => $member->id,
                        'name' => $member->name,
                        'email' => $member->email,
                        'avatar' => $member->avatar,
                        'role' => $member->role,
                    ];
                } else {
                    $members[] = [
                        'id' => $member->id,
                        'name' => $member->name,
                        'email' => $member->email,
                        'avatar' => $member->avatar,
                        'role' => $member->role,
                    ];
                }
            }

            return response()->json([
                'status' => 200,
                'data' => [
                    'id' => $chat_room->id,
                    'name' => $chat_room->name,
                    'total_member' => $chat_room->total_member,
                    'created_at' => $chat_room->created_at,
                    'updated_at' => $chat_room->updated_at,
                    'administrator' => [
                        'id' => $chat_room->administrator->id,
                        'name' => $chat_room->administrator->name,
                        'email' => $chat_room->administrator->email,
                        'avatar' => $chat_room->administrator->avatar,
                        'role' => $chat_room->administrator->role,
                    ],
                    'members' => $members,
                ]
            ], 200);
        }

        return response()->json([
            'status' => 403,
            'message' => 'Bạn không ở trong nhóm nên không có quyền xem thành viên trong nhóm này'
        ], 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showMessage($room_id, Request $request)
    {
        $room = ChatRoom::find($room_id);
        if (empty($room)) {
            return response()->json([
                'status' => 200,
                'data' => null
            ], 200);
        }

        $page = $request->input('page') ?: 1;
        $limit = 50;
        $offset = $limit * ($page - 1);

        $list_message = [];
        $messages = ChatMessage::query()
            ->where('chat_room_id', $room->id)
            ->orderBy('id', 'desc')
            ->offset($offset)
            ->limit($limit)
            ->get();
        
        foreach ($messages as $message) {
            $sender = [
                'id' => $message->user->id,
                'name' => $message->user->name,
                'email' => $message->user->email,
                'avatar' => $message->user->avatar,
                'role' => $message->user->role
            ];
            $list_message[] = [
                'id' => $message->id,
                'chat_room_id' => $message->chat_room_id,
                'message' => $message->message,
                'type' => $message->type ? ($message->type === null ? 0 : 1) : 0,
                'sender' => $sender,
                'created_at' => $message->created_at,
                'updated_at' => $message->updated_at,
            ];
        }
        
        $is_group = true;
        $send_to_user = false;
        if ($room->type === ChatRoom::ROOM_TYPE_PRIVATE) {
            $is_group = false;
            $members = $room->members;
            foreach ($members as $member) {
                if ($member->id != auth()->user()->id) {
                    $send_to_user = $member;
                }
            }
            if ($send_to_user) {
                $send_to_user = [
                    'id' => $send_to_user->id,
                    'name' => $send_to_user->name,
                    'email' => $send_to_user->email,
                    'avatar' => $send_to_user->avatar,
                    'role' => $send_to_user->role
                ];
            }
        }

        $data = [
            'is_group' => $is_group,
            'room_id' => $room->id,
            'room_name' => $room->name,
            'room_type' => $room->type,
            'room_created_at' => $room->created_at,
            'room_updated_at' => $room->updated_at,
            'send_to_user' => $send_to_user ?: null,
            'list_message' => $list_message
        ];
        
        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function leaveGroup($room_id)
    {
        $chat_room = ChatRoom::find($room_id);
        if (!$chat_room) {
            return response()->json([
                'status' => 404,
                'message' => 'Nhóm chat này không có trên hệ thống'
            ], 404);
        }

        $chat_member = ChatMember::where('chat_room_id', $room_id)->where('user_id', auth()->user()->id)->first();
        if (!$chat_member) {
            return response()->json([
                'status' => 404,
                'message' => 'Bạn không có trong nhóm chat này'
            ], 404);
        }

        $chat_member->delete();
        
        $chat_room->total_member = $chat_room->total_member - 1;
        $chat_room->save();

        return response()->json([
            'status' => 200,
            'message' => 'Bạn đã rời nhóm'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeMember($room_id, Request $request)
    {
        $fields = $request->validate(
            [
                'user_id' => 'required',
            ],
            [
                'user_id.required' => 'Thiếu tham số user_id',
            ]
        );

        $chat_room = ChatRoom::find($room_id);
        if (!$chat_room) {
            return response()->json([
                'status' => 404,
                'message' => 'Nhóm chat này không có trên hệ thống'
            ], 404);
        }

        $response = Gate::inspect('delete', $chat_room);
        if ($response->allowed()) {
            $chat_member = ChatMember::where('chat_room_id', $room_id)->where('user_id', $fields['user_id'])->first();
            if (!$chat_member) {
                return response()->json([
                    'status' => 404,
                    'message' => 'Người này không có trong nhóm chat'
                ], 404);
            }
    
            $chat_member->delete();
            
            $chat_room->total_member = $chat_room->total_member - 1;
            $chat_room->save();

            return response()->json([
                'status' => 200,
                'message' => "Xóa thành công"
            ], 200);
        }

        return response()->json([
            'status' => 403,
            'message' => 'Bạn không có quyền xóa thành viên trong nhóm'
        ], 403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyRoom($room_id)
    {
        $chat_room = ChatRoom::find($room_id);
        if (!$chat_room) {
            return response()->json([
                'status' => 404,
                'message' => 'Kênh chat này không có trên hệ thống'
            ], 404);
        }

        $chat_room->delete();

        return response()->json([
            'status' => 200,
            'message' => 'Xóa kênh chat thành công'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyMessage($message_id)
    {
        $chat_message = ChatMessage::find($message_id);
        if (!$chat_message) {
            return response()->json([
                'status' => 404,
                'message' => 'Tin nhắn này không có trên hệ thống'
            ], 404);
        }

        $response = Gate::inspect('delete', $chat_message);
        if ($response->allowed()) {
            $chat_message->delete();
            return response()->json([
                'status' => 200,
                'message' => 'Xóa tin nhắn thành công'
            ], 200);
        }

        return response()->json([
            'status' => 403,
            'message' => 'Đây không phải là tin nhắn của bạn!!!'
        ], 403);
    }

    private static function insertMemberChatPrivate($list_user_id, $chat_room_id)
    {
        if (!empty($list_user_id)) {
            $data = [];
            foreach ($list_user_id as $user_id) {
                $data[] = [
                    'user_id' => $user_id,
                    'chat_room_id' => $chat_room_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
            }
            if (!empty($data)) {
                ChatMember::insert($data);
                return true;
            }
        }

        return false;
    }

    public function getFriendSuggest()
    {
        $data = [];
        $arr_user_id_check = [];
        $users = [];
        $classes = [];
        
        if (request()->user()->isSuperAdmin() || request()->user()->isAdmin()) {
            $users = User::query()
                ->where('id', '!=', auth()->user()->id)
                ->where('status', 1)
                ->get();
        }

        if (request()->user()->isTeacher()) {
            $classes = auth()->user()->classes;
            $users = User::query()
                ->where('id', '!=', auth()->user()->id)
                ->whereIn('role', [User::ROLE_SUPER_ADMIN, User::ROLE_ADMIN, User::ROLE_TEACHER])
                ->where('status', 1)
                ->get();
        }
        
        if (request()->user()->isStudent()) {
            $classes = auth()->user()->inClasses;
        }
        
        foreach ($classes as $class) {
            if (request()->user()->isStudent()) {
                $teacher = $class->teacher;
                if ($teacher) {
                    if (!in_array($teacher->id, $arr_user_id_check)) {
                        $data[] = [
                            'id' => $teacher->id,
                            'name' => $teacher->name,
                            'email' => $teacher->email,
                            'avatar' => $teacher->avatar,
                            'role' => $teacher->role
                        ];
                        $arr_user_id_check[] = $teacher->id;
                    }
                }
            }
            $students = $class->students;
            foreach ($students as $student) {
                if (auth()->user()->id != $student->id && !in_array($student->id, $arr_user_id_check)) {
                    $data[] = [
                        'id' => $student->id,
                        'name' => $student->name,
                        'email' => $student->email,
                        'avatar' => $student->avatar,
                        'role' => $student->role
                    ];
                    $arr_user_id_check[] = $student->id;
                }
            }
        }

        foreach ($users as $user) {
            if (auth()->user()->id != $user->id && !in_array($user->id, $arr_user_id_check)) {
                $data[] = [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'avatar' => $user->avatar,
                    'role' => $user->role
                ];
                $arr_user_id_check[] = $user->id;
            }
        }

        shuffle($data);

        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }
}
