<?php

namespace App\Http\Controllers;

use App\Repositories\Notification\NotificationRepositoryInterface;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    private $notificationRepository;

    public function __construct(NotificationRepositoryInterface $notificationRepository){
        $this->notificationRepository = $notificationRepository;
    }

    public function getAll()
    {
        $notifications = $this->notificationRepository->getAll();
        
        return response()->json([
            'status' => 200,
            'data' => $notifications
        ], 200);
    }

    public function updateStatusReaded($notification_id)
    {
        // $fields = $request->validate(
        //     [
        //         'notification_id' => 'required'
        //     ],
        //     [
        //         'notification_id.required' => 'Thiếu tham số notification_id',
        //     ]
        // );

        $update = $this->notificationRepository->updateStatusReaded($notification_id);
        if (!$update) {
            return response()->json([
                'status' => 200,
                'message' => 'Fail'
            ], 200);
            
        }
        
        return response()->json([
            'status' => 200,
            'message' => 'Successfully'
        ], 200);
    }
}
