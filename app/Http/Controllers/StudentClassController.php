<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentClassRequest;
use App\Models\Notification;
use App\Repositories\Classes\ClassRepositoryInterface;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\StudentClass\StudentClassRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Services\NotificationService;
use App\Services\ToolService;
use Illuminate\Http\Request;

class StudentClassController extends Controller
{
    private $studentClassRepository;
    private $classRepository;
    private $userRepository;
    private $notificationRepository;

    public function __construct(
        StudentClassRepositoryInterface $studentClassRepository,
        ClassRepositoryInterface $classRepository,
        UserRepositoryInterface $userRepository,
        NotificationRepositoryInterface $notificationRepository
    )
    {
        $this->studentClassRepository = $studentClassRepository;
        $this->classRepository = $classRepository;
        $this->userRepository = $userRepository;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentClassRequest $request)
    {
        $class = $this->classRepository->findById($request->input('class_id'));
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học không tồn tại trên hệ thống'
            ], 404);
        }

        $teacher = $class->teacher;
        if (!$teacher) {
            return response()->json([
                'status' => 200,
                'message' => 'Lớp học chưa có giáo viên quản lý'
            ], 200);
        }

        $total_insert = 0;

        $students = json_decode($request->user_id);
        if (is_array($students)) {
            foreach ($students as $user_id) {
                $created = $this->studentClassRepository->create($class->id, $user_id);
                if ($created) {
                    $total_insert++;

                    // send notif
                    $user = $this->userRepository->findById($user_id);
                    if ($user) {
                        $body = 'Bạn vừa được thêm vào lớp học "'.$class->name.'" do giáo viên "'.$teacher->name.'" quản lý';
                        $notif = $this->notificationRepository->create([
                            'user_id' => $user->id,
                            'type' => Notification::TYPE_CLASS,
                            'content_id' => $class->id,
                            'message' => $body,
                            'url' => '/post?classId='.$class->id
                        ]);
                        if (isset($user->fcm_token) && $user->fcm_token != '' && $user->fcm_token !== null) {
                            NotificationService::sendOne($user->fcm_token, [
                                'title' => "Xin chào $user->name",
                                'body' => ToolService::subWord($body),
                                'icon' => 'https://hdev.info/images/favicon.png',
                                'click_action' => 'https://e-learning.hdev.info/post?classId='.$class->id,
                                'notification_id' => $notif ? $notif->id : false
                            ]);
                        }
                    }
                }
            }
        } else {
            $created = $this->studentClassRepository->create($class->id, $students);
            if ($created) {
                $total_insert++;
                
                // send notif
                $user = $this->userRepository->findById($students);
                if ($user) {
                    $body = 'Bạn vừa được thêm vào lớp học "'.$class->name.'" do giáo viên "'.$teacher->name.'" quản lý';
                    $notif = $this->notificationRepository->create([
                        'user_id' => $user->id,
                        'type' => Notification::TYPE_CLASS,
                        'content_id' => $class->id,
                        'message' => $body,
                        'url' => '/post?classId='.$class->id
                    ]);
                    if (isset($user->fcm_token) && $user->fcm_token != '' && $user->fcm_token !== null) {
                        NotificationService::sendOne($user->fcm_token, [
                            'title' => "Xin chào $user->name",
                            'body' => ToolService::subWord($body),
                            'icon' => 'https://hdev.info/images/favicon.png',
                            'click_action' => 'https://e-learning.hdev.info/post?classId='.$class->id,
                            'notification_id' => $notif ? $notif->id : false
                        ]);
                    }
                }
            }
        }
        
        if ($total_insert > 0) {
            $this->classRepository->updateTotalStudent($class->id, $total_insert, 'plus');
        }

        return response()->json([
            'status' => 201,
            'message' => 'Thêm sinh viên vào lớp học thành công'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentClassRequest $request)
    {
        $class = $this->classRepository->findById($request->input('class_id'));
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học không tồn tại trên hệ thống'
            ], 404);
        }

        $deleted = $this->studentClassRepository->delete($request->input('class_id'), $request->input('user_id'));
        if (!$deleted) {
            return response()->json([
                'status' => 404,
                'message' => 'Sinh viên này không có trong lớp'
            ], 404);
        }

        $this->classRepository->updateTotalStudent($request->input('class_id'), 1, 'minus');

        // send notif
        $user = $this->userRepository->findById($request->input('user_id'));
        if ($user) {
            $body = 'Bạn vừa bị xóa khỏi lớp học "'.$class->name.'"';
            $notif = $this->notificationRepository->create([
                'user_id' => $user->id,
                'message' => $body,
                'url' => '/'
            ]);
            if (isset($user->fcm_token) && $user->fcm_token != '' && $user->fcm_token !== null) {
                NotificationService::sendOne($user->fcm_token, [
                    'title' => "Xin chào $user->name",
                    'body' => ToolService::subWord($body),
                    'icon' => 'https://hdev.info/images/favicon.png',
                    'click_action' => 'https://e-learning.hdev.info/',
                    'notification_id' => $notif ? $notif->id : false
                ]);
            }
        }
        
        return response()->json([
            'status' => 200,
            'message' => 'Xóa sinh viên thành công'
        ], 200);
    }
}
