<?php

namespace App\Http\Controllers;

use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\CourseCategory\CourseCategoryRepositoryInterface;
use App\Repositories\Question\QuestionRepositoryInterface;
use App\Services\ToolService;
use Illuminate\Http\Request;

class CourseCategoryController extends Controller
{
    private $courseCategoryRepository;
    private $courseRepository;
    private $questionRepository;

    public function __construct(
        CourseCategoryRepositoryInterface $courseCategoryRepository,
        QuestionRepositoryInterface $questionRepository,
        CourseRepositoryInterface $courseRepository    
    )
    {
        $this->courseCategoryRepository = $courseCategoryRepository;
        $this->questionRepository = $questionRepository;
        $this->courseRepository = $courseRepository;
    }

    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $data = $this->courseCategoryRepository->findById($id);
        if (!$data) {
            return response()->json([
                'status' => 404,
                'message' => 'Môn học này không có chưa có trên hệ thống'
            ], 404);
        }

        // pagination
        $courses = $this->courseCategoryRepository->showListCourse($id);

        if (count($courses) === 0) {
            return response()->json([
                'status' => 200,
                'data' => [
                    'id' => $data->id,
                    'name' => $data->name,
                    'slug' => $data->slug,
                    'description' => $data->description ?? '',
                    'courses' => []
                ]
            ], 200);
        }

        $list_course = [];
        foreach ($courses as $course) {
            $percent_progress = $this->courseRepository->getProgressOfUser($course->id);
            $list_course[] = $course->format($course, $percent_progress);
        }

        return response()->json([
            'status' => 200,
            'data' => [
                'id' => $data->id,
                'name' => $data->name,
                'slug' => $data->slug,
                'description' => $data->description ?? '',
                'courses' => ToolService::handleCustomPagination($courses, $list_course)
            ]
        ], 200);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    /**
     * get all question of course category
     */
    public function getAllQuestionOfCategory($course_category_id, Request $request)
    {
        $questions = $this->questionRepository->getAllQuestionOfCategory($course_category_id, $request->name);

        return response()->json([
            'status' => 200,
            'data' => $questions
        ], 200);
    }
}
