<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClassRequest;
use App\Models\ChatMember;
use App\Models\ChatRoom;
use App\Models\Notification;
use App\Models\User;
use App\Repositories\Classes\ClassRepositoryInterface;
use App\Repositories\Course\CourseRepositoryInterface;
use App\Repositories\Notification\NotificationRepositoryInterface;
use App\Repositories\StudentClass\StudentClassRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use App\Services\NotificationService;
use App\Services\ToolService;
use Illuminate\Http\Request;

class ClassController extends Controller
{
    private $classRepository;
    private $userRepository;
    private $studentClassRepository;
    private $courseRepository;
    private $notificationRepository;

    public function __construct(
        ClassRepositoryInterface $classRepository, 
        UserRepositoryInterface $userRepository, 
        StudentClassRepositoryInterface $studentClassRepository,
        CourseRepositoryInterface $courseRepository,
        NotificationRepositoryInterface $notificationRepository
    )
    {
        $this->classRepository = $classRepository;
        $this->userRepository = $userRepository;
        $this->studentClassRepository = $studentClassRepository;
        $this->courseRepository = $courseRepository;
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = $this->classRepository->getAllClassOfUser();

        $list_class = [];
        foreach ($classes as $class) {
            $teacher = $this->userRepository->findById($class->user_id);
            $list_class[] = $class->format($class, $teacher);
        }

        return response()->json([
            'status' => 200,
            'data' => (auth()->user()->role === User::ROLE_ADMIN || auth()->user()->role === User::ROLE_SUPER_ADMIN)
                    ? ToolService::handleCustomPagination($classes, $list_class) 
                    : $list_class
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClassRequest $request)
    {
        $attributes = [
            'name' => $request->input('name'),
            'user_id' => $request->input('user_id')
        ];
        
        $class = $this->classRepository->create($attributes);
        if (!$class) {
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }

        // push notif
        $user = $this->userRepository->findById($request->input('user_id'));
        if ($user) {
            $body = 'Bạn vừa được Admin mời vào quản lý lớp học "'.$request->input('name').'"';
            $notif = $this->notificationRepository->create([
                'user_id' => $user->id,
                'type' => Notification::TYPE_CLASS,
                'content_id' => $class['id'],
                'message' => $body,
                'url' => '/post?classId='.$class['id']
            ]);
            if (isset($user->fcm_token) && $user->fcm_token != '' && $user->fcm_token !== null) {
                NotificationService::sendOne($user->fcm_token, [
                    'title' => "Xin chào $user->name",
                    'body' => ToolService::subWord($body),
                    'icon' => 'https://hdev.info/images/favicon.png',
                    'click_action' => 'https://e-learning.hdev.info/post?classId='.$class['id'],
                    'notification_id' => $notif ? $notif->id : false
                ]);
            }
        }
        
        return response()->json([
            'status' => 201,
            'message' => 'Tạo lớp học thành công',
            'data' => $class
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $class = $this->classRepository->findById($id);
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Khóa học này không có trên hệ thống'
            ], 404);
        }
        
        if (auth()->user()->role !== User::ROLE_ADMIN && auth()->user()->role !== User::ROLE_SUPER_ADMIN) {
            if ($class->user_id !== auth()->user()->id) {
                $check_student_in_class = $this->studentClassRepository->findById($id, auth()->user()->id);
                if (!$check_student_in_class) {
                    return response()->json([
                        'status' => 404,
                        'message' => 'Bạn không có quyền truy cập vào lớp học này'
                    ], 404);
                }
            }
        }

        $teacher = $this->userRepository->findById($class->user_id);
        $data = $class->format($class, $teacher);
        
        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClassRequest $request, $id)
    {
        $find_class = $this->classRepository->findById($id);
        if (!$find_class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học này không có trên hệ thống'
            ], 404);
        }

        $attributes = [
            'name' => $request->input('name'),
            'user_id' => $request->input('user_id')
        ];
        
        $class = $this->classRepository->update($id, $attributes);
        if (!$class) {
            return response()->json([
                'status' => 500,
                'message' => 'Lỗi máy chủ'
            ], 500);
        }

        // push notif
        $user = $this->userRepository->findById($request->input('user_id'));
        if ($user) {
            $body = 'Bạn vừa được Admin mời vào quản lý lớp học "'.$request->input('name').'"';
            $notif = $this->notificationRepository->create([
                'user_id' => $user->id,
                'type' => Notification::TYPE_CLASS,
                'content_id' => $class['id'],
                'message' => $body,
                'url' => '/post?classId='.$class['id']
            ]);
            if (isset($user->fcm_token) && $user->fcm_token != '' && $user->fcm_token !== null) {
                NotificationService::sendOne($user->fcm_token, [
                    'title' => "Xin chào $user->name",
                    'body' => ToolService::subWord($body),
                    'icon' => 'https://hdev.info/images/favicon.png',
                    'click_action' => 'https://e-learning.hdev.info/post?classId='.$class['id'],
                    'notification_id' => $notif ? $notif->id : false
                ]);
            }
        }
        
        return response()->json([
            'status' => 200,
            'message' => 'Cập nhật lớp học thành công',
            'data' => $class
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->classRepository->delete($id);
        if (!$deleted) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học không tồn tại trên hệ thống'
            ], 404);
        }
        
        return response()->json([
            'status' => 200,
            'message' => 'Xóa lớp học thành công'
        ], 200);
    }

    public function getCourseOfClass($class_id)
    {
        $class = $this->classRepository->findById($class_id);
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Khóa học này không có trên hệ thống'
            ], 404);
        }
        
        if (auth()->user()->role !== User::ROLE_ADMIN && auth()->user()->role !== User::ROLE_SUPER_ADMIN) {
            if ($class->user_id !== auth()->user()->id) {
                $check_student_in_class = $this->studentClassRepository->findById($class_id, auth()->user()->id);
                if (!$check_student_in_class) {
                    return response()->json([
                        'status' => 404,
                        'message' => 'Bạn không có quyền truy cập vào lớp học này'
                    ], 404);
                }
            }
        }

        $data = [];
        foreach ($class->courses as $course) {
            $percent_progress = $this->courseRepository->getProgressOfUser($course->id);
            $data[] = $course->format($course, $percent_progress);
        }
        
        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }
    
    public function getStudentOfClass($class_id)
    {
        $class = $this->classRepository->findById($class_id);
        if (!$class) {
            return response()->json([
                'status' => 404,
                'message' => 'Lớp học này không có trên hệ thống'
            ], 404);
        }
        
        if (auth()->user()->role !== User::ROLE_ADMIN && auth()->user()->role !== User::ROLE_SUPER_ADMIN) {
            if ($class->user_id !== auth()->user()->id) {
                $check_student_in_class = $this->studentClassRepository->findById($class_id, auth()->user()->id);
                if (!$check_student_in_class) {
                    return response()->json([
                        'status' => 404,
                        'message' => 'Bạn không có quyền truy cập vào lớp học này'
                    ], 404);
                }
            }
        }

        $list_chat = ChatMember::query()
            ->where('user_id', auth()->user()->id)
            ->get();
        
        $list_id_room_private = [];
        foreach ($list_chat as $item) {
            if ($item->room->type === ChatRoom::ROOM_TYPE_PRIVATE) {
                $list_id_room_private[] = $item->chat_room_id;
            }
        }

        $data = [];
        foreach ($class->students as $student) {
            $room_id = false;
            foreach ($list_id_room_private as $value) {
                $room_exists = ChatMember::where('chat_room_id', $value)->where('user_id', $student->id)->first();
                if ($room_exists) {
                    $room_id = $value;
                    break;
                }
            }
            $data[] = [
                'id' => $student->id,
                'name' => $student->name,
                'email' => $student->email,
                'role' => $student->role,
                'avatar' => $student->avatar,
                'room_id' => $room_id ? $room_id : null
            ];
        }
        
        return response()->json([
            'status' => 200,
            'data' => $data
        ], 200);
    }
}
