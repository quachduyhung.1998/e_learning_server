<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClassAssignedCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class_id' => 'required',
            'course_id' => 'required',
        ];
    }
    
    public function messages()
    {
        return [
            'class_id.required' => 'Bạn chưa chọn lớp học',
            'course_id.required' => 'Bạn chưa chọn khóa học',
        ];
    }
}
