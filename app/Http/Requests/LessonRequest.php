<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LessonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'content' => 'required',
            'course_id' => 'required',
            'type' => 'required',
        ];
    }
    
    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên bài học',
            'content.required' => 'Bạn chưa nhập nội dung cho bài học',
            'course_id.required' => 'Thiếu id khóa học (course_id)',
            'type.required' => 'Bạn chưa chọn kiểu bài học',
        ];
    }
}
