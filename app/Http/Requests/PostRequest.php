<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class_id' => 'required',
            'content' => 'required',
        ];
    }
    
    public function messages()
    {
        return [
            'class_id.required' => 'Thiếu tham số class_id',
            'content.required' => 'Bạn chưa nhập nội dung cho bài viết',
        ];
    }
}
