<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChatPrivateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'send_to_user_id' => 'required',
            'message' => 'required',
            'type' => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'send_to_user_id.required' => 'Thiếu id người nhận (send_to_user_id)',
            'message.required' => 'Thiếu tham số message',
            'type.required' => 'Thiếu tham số type',
        ];
    }
}
