<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentAnswerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'test_id' => 'required',
            'answers' => 'required',
            'time_start' => 'required',
            'time_finish' => 'required',
            // 'total_time_work' => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'test_id.required' => 'Thiếu test ID',
            'answers.required' => 'Bạn chưa nhập mảng câu trả lời',
            'time_start.required' => 'Thiếu thời gian bắt đầu làm',
            'time_finish.required' => 'Thiếu thời gian lúc gửi bài',
            // 'total_time_work.required' => 'Thiếu tổng thời gian làm bài'
        ];
    }
}
