<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'course_category_id' => 'required',
            'type_question' => 'required',
            'answers' => 'required',
            'mark' => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập nội dung câu hỏi',
            'course_category_id.required' => 'Bạn chưa chọn môn học',
            'type_question.required' => 'Bạn chưa chọn loại câu hỏi',
            'answers.required' => 'Bạn chưa nhập câu trả lời',
            'mark.required' => 'Bạn chưa nhập điểm cho câu hỏi này'
        ];
    }
}
