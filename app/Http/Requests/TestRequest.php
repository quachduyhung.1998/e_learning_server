<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'lesson_id' => 'required',
            'time_work' => 'required',
            'list_question_id' => 'required',
        ];
    }
    
    public function messages()
    {
        return [
            'name.required' => 'Bạn chưa nhập tên bài kiểm tra',
            'time_work.required' => 'Bạn chưa nhập thời gian làm bài kiểm tra',
            'lesson_id.required' => 'Thiếu id bài học (lesson_id)',
            'list_question_id.required' => 'Bạn chưa chọn câu hỏi',
        ];
    }
}
