<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $role)
    {
        $roles = [
            'onlyStudent' => [0],
            'student' => [0, 1, 2, 3],
            'teacher' => [1, 2, 3],
            'teacherCourse' => [1, 3],
            'admin' => [2, 3],
            'superAdmin' => [3]
        ];

        $arr_role = $roles[$role] ?? [];

        if (!in_array(auth()->user()->role, $arr_role)) {
            return response()->json([
                'status' => 403,
                'message' => 'Bạn không có quyền truy cập vào nội dung này!!!'
            ], 403);
        }

        return $next($request);
    }
}
