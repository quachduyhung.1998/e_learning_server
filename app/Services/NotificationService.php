<?php

namespace App\Services;

use Illuminate\Support\Facades\File;

class NotificationService
{
    const URL_FCM = 'https://fcm.googleapis.com/fcm/send';

    public static function welcome($token, $user_name)
    {
        $data = [
            'title' => "Xin chào $user_name",
            'body' => 'Chào mừng bạn đến với E-learning, website học trực tuyến hàng đầu Việt Nam',
            'icon' => asset('/images/favicon.png'),
            'image' => asset('/images/welcome.jpg'),
            'click_action' => 'https://e-learning.hdev.info/'
        ];

        self::sendOne($token, $data);
    }

    public static function sendOne($token, array $data)
    {
        try {
            $data = [
                'to' => $token,
                'data' => $data
            ];
            self::send($data);
        }
        catch (\Throwable $th) {
            throw $th;
        }
    }

    public static function sendMulti(array $list_token, array $data)
    {
        try {
            $data = [
                'registration_ids' => $list_token,
                'data' => $data
            ];
            self::send($data);
        }
        catch (\Throwable $th) {
            throw $th;
        }
    }

    public static function send($data)
    {
        $FCM_API_KEY = config('app.fcm_api_key');

        $encodedData = json_encode($data);
        
        $headers = [
            'Authorization: ' . $FCM_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::URL_FCM);
        curl_setopt($ch, CURLOPT_POST, true);
        // curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encodedData);

        $response = curl_exec($ch);
        if (!$response) {
            die('Curl failed: ' . curl_error($ch));
        }

        curl_close($ch);
    }
}