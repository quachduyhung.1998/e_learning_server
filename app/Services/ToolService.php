<?php

namespace App\Services;

use Illuminate\Support\Facades\File;

class ToolService
{
    public static function randomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function subWord($stringText, $space = 30, $add = '...') {
        $charters = array_map('trim', explode(" ", $stringText));
        if (count($charters) < $space) {
            return $stringText;
        }
        $str = [];
        for ($i = 0; $i < count($charters); $i++) {
            if ($i < $space) {
                $str[] = $charters[$i];
            }
        }
        return implode(' ', $str) . $add;
    }

    public static function handleCustomPagination($paginator, $data)
    {
        return [
            'current_page' => $paginator->currentPage(),
            'data' => $data,
            'first_page_url' => $paginator->url(1),
            'last_page' => $paginator->lastPage(),
            'last_page_url' => $paginator->url($paginator->lastPage()),
            'next_page_url' => $paginator->nextPageUrl(),
            'path' => url()->current(),
            'per_page' => $paginator->perPage(),
            'prev_page_url' => $paginator->previousPageUrl(),
        ];
    }

    public static function handleCustomPaginationComment($paginator, $data)
    {
        return [
            'current_page' => $paginator->currentPage(),
            'list_comment' => $data,
            'first_page_url' => $paginator->url(1),
            'last_page' => $paginator->lastPage(),
            'last_page_url' => $paginator->url($paginator->lastPage()),
            'next_page_url' => $paginator->nextPageUrl(),
            'path' => url()->current(),
            'per_page' => $paginator->perPage(),
            'prev_page_url' => $paginator->previousPageUrl(),
        ];
    }

    public static function handleUploadImage($image) {
        try {
            if (!is_null($image)) {
                $fileName = date('Y_m_d_His_') . self::randomString() . '.' . $image->getClientOriginalExtension();
                while (file_exists('storage/' . $fileName)) {
                    $fileName = date('Y_m_d_His_') . self::randomString() . '.' . $image->getClientOriginalExtension();
                }

                $image->move(public_path('storage'), $fileName);

                return url('storage/' . $fileName);
            }
            return false;
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    public static function handleDeleteImage($image)
    {
        try {
            if (file_exists('storage/' . $image)) {
                File::delete(public_path('storage/') . $image);
            }
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}