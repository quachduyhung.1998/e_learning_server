<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            // $table->foreignId('test_id')->constrained()->onDelete('cascade');
            /** type_quiz
             * 1 - chỉ có 1 đáp án đúng
             * 2 - có nhiều đáp án đúng
             */
            $table->integer('type_question')->default(1);
            $table->integer('mark');
            /** answers
             * gồm 1 mảng các câu trả lời
             * [
             *     ['key' => 'bla bla', is_right: 0/1],...
             * ]
             */
            $table->longText('answers');
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->foreignId('course_category_id')->constrained('course_categories')->onDelete('cascade');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
