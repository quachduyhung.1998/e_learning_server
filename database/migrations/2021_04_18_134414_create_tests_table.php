<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->longText('description')->nullable();
            $table->foreignId('lesson_id')->constrained()->onDelete('cascade');
            $table->foreignId('course_id')->constrained()->onDelete('cascade');
            $table->string('list_question_id')->nullable();
            $table->integer('total_question')->default(0);
            $table->integer('total_mark')->default(0);
            $table->integer('time_work'); // phút
            /** is_required
             * 0 - không bắt buộc làm
             * 1 - bắt buộc làm mới cho học bài tiếp (nếu course bắt buộc học lần lượt các lesson)
             */
            $table->integer('is_required')->default(0);
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
