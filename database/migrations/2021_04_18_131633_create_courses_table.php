<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->longText('description')->nullable();
            $table->string('image')->nullable();
            $table->foreignId('course_category_id')->constrained('course_categories')->onDelete('cascade');
            $table->foreignId('user_id')->constrained()->onDelete('cascade');
            $table->integer('total_lesson')->default(0);
            /** is_sequence
             * 0 - không bắt buộc học lần lượt
             * 1 - bắt buộc học lần lượt
             */
            $table->integer('is_sequence')->default(0);
            /** status
             * 0 - nháp
             * 1 - hoàn thành tạo course nhưng chưa public
             * 2 - hiển thị public
             */
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
