<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\ClassAssignedCourseController;
use App\Http\Controllers\ClassController;
use App\Http\Controllers\CourseCategoryController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\LessonController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\StudentAnswerController;
use App\Http\Controllers\StudentClassController;
use App\Http\Controllers\StudentCompleteLessonController;
use App\Http\Controllers\StudentProgressCourseController;
use App\Http\Controllers\TeacherApprovalCourseController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\UploadController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;


// Public route
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);
Route::post('/forgot-password', [AuthController::class, 'forgotPassword']);

// save token firebase
Route::post('/users/save-token', [UserController::class, 'saveToken']);

// notification firebase click
Route::get('/notifications/{notification_id}/update-status-readed', [NotificationController::class, 'updateStatusReaded']);

// Protected route
Route::middleware('auth:sanctum')->group(function () {
    // upload image
    Route::post('upload-image', [UploadController::class, 'uploadImage']);

    // logout
    Route::post('logout', [AuthController::class, 'logout']);

    // User
    Route::apiResource('users', UserController::class)->only(['index'])->middleware('CheckRole:teacher');
    Route::apiResource('users', UserController::class)->only(['show']);
    Route::apiResource('users', UserController::class)->only(['store', 'update', 'destroy'])->middleware('CheckRole:admin');
    Route::get('/users/permissions/{role}', [UserController::class, 'getListUserWithPermissions'])->middleware('CheckRole:teacher');
    Route::put('/users/update-profile', [UserController::class, 'updateProfile']);
    Route::put('/users/change-password', [UserController::class, 'changePassword']);
    Route::put('/users/{id}/change-permissions', [UserController::class, 'changePermissions'])->middleware('CheckRole:admin');

    // Course Category
    Route::apiResource('course-category', CourseCategoryController::class)->only(['show']);
    Route::get('course-category/{course_category_id}/get-all-question', [CourseCategoryController::class, 'getAllQuestionOfCategory'])->middleware('CheckRole:teacherCourse');

    // Course
    Route::apiResource('courses', CourseController::class)->only(['index', 'show']);
    Route::get('/my-courses', [CourseController::class, 'getAllMyCourse']);
    Route::get('/courses-pending-approval', [CourseController::class, 'getAllCoursePendingApproval'])->middleware('CheckRole:teacher');
    Route::get('/courses-approved', [CourseController::class, 'getAllCourseApproved']);
    Route::middleware('CheckRole:teacherCourse')->group(function () {
        Route::apiResource('courses', CourseController::class)->only(['store', 'destroy', 'update']);
        Route::put('/courses/{id}/complete', [CourseController::class, 'updateStatusComplete']);
        Route::put('/courses/{id}/public', [CourseController::class, 'updateStatusPublic']);
        Route::put('/courses/{id}/mode-sequence', [CourseController::class, 'updateStatusSequence']);

        // show all test of course
        Route::get('/courses/{id}/tests', [CourseController::class, 'getAllTest']);
    });

    // Student complete lesson
    Route::apiResource('/lessons/student-complete', StudentCompleteLessonController::class)
        ->only(['store'])
        ->middleware('CheckRole:onlyStudent');
    // Lesson
    Route::apiResource('lessons', LessonController::class)->only(['show']);
    Route::middleware('CheckRole:teacherCourse')->group(function () {
        Route::apiResource('lessons', LessonController::class)->only(['store', 'update', 'destroy']);
        Route::put('lessons/{id}/change-order', [LessonController::class, 'changeOrder']);
    });

    // Test
    Route::get('tests/{test_id}/get-all-question', [TestController::class, 'getAllQuestionOfTest']);
    Route::middleware('CheckRole:teacherCourse')->group(function () {
        Route::apiResource('tests', TestController::class);
    });

    // Question
    Route::middleware('CheckRole:teacherCourse')->group(function() {
        Route::apiResource('questions', QuestionController::class);
    });

    // Class
    Route::get('get-course-of-class/{class_id}', [ClassController::class, 'getCourseOfClass']);
    Route::get('get-student-of-class/{class_id}', [ClassController::class, 'getStudentOfClass']);
    Route::apiResource('classes', ClassController::class)->only(['index', 'show']);
    Route::apiResource('classes', ClassController::class)->only(['store', 'destroy'])->middleware('CheckRole:admin');
    Route::apiResource('classes', ClassController::class)->only(['update'])->middleware('CheckRole:teacher');

    // Student class
    Route::middleware('CheckRole:teacher')->group(function() {
        Route::apiResource('student-class', StudentClassController::class)->except(['show', 'update', 'destroy']);
        Route::post('delete-student-class', [StudentClassController::class, 'destroy'])->name('delete-student-class.destroy');
    });

    // Class assigned course
    Route::middleware('CheckRole:teacher')->group(function() {
        Route::apiResource('class-assigned-course', ClassAssignedCourseController::class)->only(['index', 'store']);
        Route::post('delete-class-assigned-course', [ClassAssignedCourseController::class, 'destroy'])->name('delete-class-assigned-course.destroy');
    });

    // Teacher approval course
    Route::middleware('CheckRole:admin')->group(function() {
        Route::apiResource('teacher-approval-course', TeacherApprovalCourseController::class);
    });

    // Student answer question
    Route::apiResource('working-test', StudentAnswerController::class)->only(['store']);
    Route::get('/courses/{course_id}/test-worked', [StudentAnswerController::class, 'getAllTestWorkedInCourse']);
    Route::get('/lessons/{lesson_id}/test-worked', [StudentAnswerController::class, 'getAllTestWorkedInLesson']);
    // show chi tiết bài test đã làm, từng câu hỏi, câu trả lời, số điểm
    Route::get('/tests/{test_id}/test-result', [StudentAnswerController::class, 'show']);
    // show tất cả các user đã làm bài test
    Route::get('/tests/{test_id}/all-user-worked', [StudentAnswerController::class, 'getAllUserWorked'])->middleware('CheckRole:teacher');

    // student progress course
    Route::get('/courses/{course_id}/all-user-progress', [StudentProgressCourseController::class, 'show'])->middleware('CheckRole:teacher');

    // Chat
    Route::get('chats/get-all', [ChatController::class, 'getAll']);
    Route::get('chats/{room_id}/show-message', [ChatController::class, 'showMessage']);
    Route::get('chats/{room_id}/show-member', [ChatController::class, 'showMember']);
    Route::post('chats/private', [ChatController::class, 'chatPrivate']);
    Route::post('chats/create-group', [ChatController::class, 'storeGroup']);
    Route::post('chats/{room_id}/add-member', [ChatController::class, 'addMember']);
    Route::post('chats/group', [ChatController::class, 'chatGroup']);
    Route::delete('chats/{room_id}/remove-member', [ChatController::class, 'removeMember']);
    Route::delete('chats/{room_id}/leave-group', [ChatController::class, 'leaveGroup']);
    // Route::delete('chats/delete-room/{room_id}', [ChatController::class, 'destroyRoom']);
    Route::delete('chats/delete-message/{message_id}', [ChatController::class, 'destroyMessage']);
    // friend suggestion
    Route::get('chats/get-friend-suggestion', [ChatController::class, 'getFriendSuggest']);

    // Post
    Route::get('/posts/get-all', [PostController::class, 'getAllPost']);
    Route::get('/classes/{class_id}/get-all-post', [PostController::class, 'getAllPostInClass']);
    Route::get('/posts/{post_id}', [PostController::class, 'showPost']);
    Route::post('/posts/create', [PostController::class, 'storePost']);
    Route::put('/posts/{post_id}/update', [PostController::class, 'updatePost']);
    Route::delete('/posts/{post_id}/delete', [PostController::class, 'destroyPost']);
    // Comment
    Route::get('/posts/{post_id}/get-all-comment', [PostController::class, 'getAllCommentInPost']);
    Route::post('/comments/create', [PostController::class, 'storeComment']);
    Route::put('/comments/{comment_id}/update', [PostController::class, 'updateComment']);
    Route::delete('/comments/{comment_id}/delete', [PostController::class, 'destroyComment']);
    // Like
    Route::get('/posts/{post_id}/get-all-like', [PostController::class, 'getAllLikeOfPost']);
    Route::get('/comments/{comment_id}/get-all-like', [PostController::class, 'getAllLikeOfComment']);
    Route::post('/like-post', [PostController::class, 'storeLikePost']);
    Route::post('/like-comment', [PostController::class, 'storeLikeComment']);

    // Notification
    Route::get('/notifications/get-all', [NotificationController::class, 'getAll']);

    // Dashboard Admin
    Route::middleware('CheckRole:admin')->group(function() {
        Route::get('/admin/dashboard', [AdminController::class, 'dashboard']);
    });
});
