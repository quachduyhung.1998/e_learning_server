
var apiUrl = 'https://hdev.info/api';
// var apiUrl = 'http://doan.local/api';

self.addEventListener('push', function (event) {
    let payload = {}
    if (event.data) {
        payload = event.data.json();
    }

    const notificationTitle = payload.data.title;
    const notificationOptions = {
        body: payload.data.body,
        icon: payload.data.icon,
        image: payload.data.image,
        data: {
            url: payload.data.click_action,
            notif_id: payload.data.notification_id,
        }
        // requireInteraction: true,
    };
    
    event.waitUntil(self.registration.showNotification(notificationTitle, notificationOptions));
});


self.addEventListener('notificationclick', function(event) {
    event.notification.close();

    if (event.notification.data.notif_id) {
        let notification_id = event.notification.data.notif_id;
        let url = `${apiUrl}/notifications/${notification_id}/update-status-readed`;
        fetch(url);
    }

    let notificationUrl = event.notification.data.url;
    event.waitUntil(Promise.all([self.clients.openWindow(notificationUrl)]));
});