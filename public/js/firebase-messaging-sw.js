// importScripts('https://www.gstatic.com/firebasejs/8.6.2/firebase-app.js');
// importScripts('https://www.gstatic.com/firebasejs/8.6.2/firebase-messaging.js');

// firebase.initializeApp({
//     apiKey: "AIzaSyDXLqeJvAQnsKF4BbQIoKCdgceIACqk8tc",
//     authDomain: "do-an-97b42.firebaseapp.com",
//     databaseURL: "https://do-an-97b42.firebaseio.com",
//     projectId: "do-an-97b42",
//     storageBucket: "do-an-97b42.appspot.com",
//     messagingSenderId: "588177691940",
//     appId: "1:588177691940:web:f31e31b573b96b6d6bcfb1",
//     measurementId: "G-433HZVFJJH"
// });

// const messaging = firebase.messaging();

// messaging.setBackgroundMessageHandler(function( payload) {
//     console.log('[firebase-messaging-sw.js] Received background message ', payload);

//     const notificationTitle = payload.notification.title;
//     const notificationOptions = {
//         body: payload.notification.body,
//         icon: payload.notification.icon,
//         image: payload.notification.image,
//         click_action: payload.notification.click_action,
//         data: {
//             click_action: payload.notification.click_action
//         }
//         // requireInteraction: true,
//     };
    
//     return self.registration.showNotification(notificationTitle, notificationOptions);
// });


self.addEventListener('push', function (event) {
    let payload = {}
    if (event.data) {
        payload = event.data.json();
    }

    const notificationTitle = payload.notification.title;
    const notificationOptions = {
        body: payload.notification.body,
        icon: payload.notification.icon,
        image: payload.notification.image,
        data: {
            url: payload.notification.click_action
        }
        // requireInteraction: true,
    };
    
    event.waitUntil(self.registration.showNotification(notificationTitle, notificationOptions));
});


self.addEventListener('notificationclick', function(event) {
    event.notification.close();

    let notificationUrl = event.notification.data.url;

    event.waitUntil(Promise.all([self.clients.openWindow(notificationUrl)]));
});