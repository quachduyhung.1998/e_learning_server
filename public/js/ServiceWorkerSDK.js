(function () {
    var scriptFireBase = [
        "https://www.gstatic.com/firebasejs/8.6.2/firebase-app.js",
        "https://www.gstatic.com/firebasejs/8.6.2/firebase-messaging.js"
    ];
    var apiUrl = 'https://hdev.info/api';
    // var apiUrl = 'http://doan.local/api';
    var workerUrl = '/e-learning-sw.js';
    var messaging, userId, fcmToken;
    const USER_SESSION_NAME = 'USER_SESSION';
    const FCM_TOKEN_NAME = 'FCM_TOKEN';

    function checkStateAndCall(path, callback) {
        var _success = false;
        return function () {
            if (!_success && (!this.readyState || (this.readyState == 'complete'))) {
                _success = true;
                // console.log(path, 'is ready');
                callback();
            }
        };
    }
    
    function loadScript() {
        if (!scriptFireBase.length) {
            initFirebase();
        } else {
            let pathFile = scriptFireBase.shift();
            let scriptTag = document.getElementsByTagName('script')[0];
            let newScript = document.createElement('script');
            newScript.type = 'text/javascript';
            newScript.async = true;
            newScript.defer = true;
            newScript.onload = newScript.onreadystatechange = checkStateAndCall(pathFile, loadScript);
            newScript.src = pathFile;
            scriptTag.parentNode.insertBefore(newScript, scriptTag);
        }
    }
    loadScript();

    function initFirebase() {
        if (firebase.messaging.isSupported()) {
            let checkUserLoginedInterval = setInterval(function() {
                var checkLogined = checkUserLogined();
                if (checkLogined) {
                    clearInterval(checkUserLoginedInterval);
                    var firebaseConfig = {
                        apiKey: "AIzaSyDXLqeJvAQnsKF4BbQIoKCdgceIACqk8tc",
                        authDomain: "do-an-97b42.firebaseapp.com",
                        databaseURL: "https://do-an-97b42.firebaseio.com",
                        projectId: "do-an-97b42",
                        storageBucket: "do-an-97b42.appspot.com",
                        messagingSenderId: "588177691940",
                        appId: "1:588177691940:web:f31e31b573b96b6d6bcfb1",
                        measurementId: "G-433HZVFJJH"
                    };

                    firebase.initializeApp(firebaseConfig);
                    messaging = firebase.messaging();

                    if ('serviceWorker' in navigator) {
                        navigator.serviceWorker.register(workerUrl)
                        .then(async function (registration) {
                            console.log('Registered service worker');
                            messaging.useServiceWorker(registration);
                            registration.update();
                            
                            messaging.onTokenRefresh(function () {
                                messaging.getToken()
                                    .then(function (refreshedToken) {
                                        console.log('refresh: ', refreshedToken);
                                        checkPermissionAllow();
                                    }).catch(function (err) {
                                        console.log('Unable to retrieve refreshed token ', err);
                                    });
                            });
    
                            checkPermissionAllow();
                        })
                        .catch(function (err) {
                            console.log('Register service worker failed', err);
                        });
                    } else {
                        if (!('serviceWorker' in navigator)) {
                            console.error('No Service Worker support!')
                        }
                        if (!('PushManager' in window)) {
                            console.error('No Push API Support!')
                        }
                    }
                }
            }, 100);
        }
    }

    function requestPermission() {
        messaging.requestPermission()
            .then(function () {
                return messaging.getToken();
            })
            .then(function (token) {
                console.log('saveTokenForUser');
                window.localStorage.setItem(FCM_TOKEN_NAME, token);
                // cập nhật token
                saveTokenForUser(token);
            })
            .catch(function (err) {
                console.log('Unable to get permission to notify.', err);
            });
    }

    function checkPermissionAllow() {
        if (Notification.permission === 'granted') {
            messaging.getToken().then(function (currentToken) {
                // nếu user đã allow thì gọi hàm cập nhật token
                // if (!currentToken) {
                //     console.log('checkPermissionAllow');
                //     // Chưa allow thì hiển thị yêu cầu
                //     requestPermission();
                // }
            }).catch(function (err) {
                console.log('An error occurred while retrieving token. ', err);
            });
        }
        else if (Notification.permission !== 'denied') {
            // Chưa allow thì hiển thị yêu cầu
            requestPermission();
        }
    }

    function checkUserLogined() {
        let user = window.localStorage.getItem(USER_SESSION_NAME);
        if (user) {
            user = JSON.parse(user);
            userId = user.id;
            return true;
        }
        return false;
    };

    function getFcmToken() {
        let token = window.localStorage.getItem(FCM_TOKEN_NAME);
        if (token) {
            fcmToken = token;
            return true;
        }
        return false;
    };
    
    function saveTokenForUser(token) {
        const url = `${apiUrl}/users/save-token`;
        const data = {
            user_id: userId,
            fcm_token: token
        };

        fetch(url, {
            method: 'POST',
            headers: {
                "Accept": "application/json",
                // "Content-Type": "application/json",
                // "X-Requested-With": "XMLHttpRequest",
            },
            // credentials: "same-origin",
            mode: 'no-cors',
            cache: 'no-cache',
            body: JSON.stringify(data)
        })
        .then(response => response.json())
        .then(result => console.log(result))
        .catch((error) => {
            console.error('Error:', error);
        });
    }
    // saveTokenForUser('token');
})(window);