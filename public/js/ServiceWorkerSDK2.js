(function () {
    var scriptFireBase = [
        "https://www.gstatic.com/firebasejs/8.6.2/firebase-app.js",
        "https://www.gstatic.com/firebasejs/8.6.2/firebase-messaging.js"
    ];
    var apiUrl = 'https://hdev.info/api';
    var workerUrl = '/e-learning-sw.js';

    function checkStateAndCall(path, callback) {
        var _success = false;
        return function () {
            if (!_success && (!this.readyState || (this.readyState == 'complete'))) {
                _success = true;
                // console.log(path, 'is ready');
                callback();
            }
        };
    }
    
    function loadScript() {
        if (!scriptFireBase.length) {
            initFirebase();
        } else {
            let pathFile = scriptFireBase.shift();
            let scriptTag = document.getElementsByTagName('script')[0];
            let newScript = document.createElement('script');
            newScript.type = 'text/javascript';
            newScript.async = true;
            newScript.defer = true;
            newScript.onload = newScript.onreadystatechange = checkStateAndCall(pathFile, loadScript);
            newScript.src = pathFile;
            scriptTag.parentNode.insertBefore(newScript, scriptTag);
        }
    }
    loadScript();

    function initFirebase() {
        if (firebase.messaging.isSupported()) {
            // let checkUserLoginedInterval = setInterval(function() {
                // var checkLogined = checkUserLogined();
                // if (checkLogined) {
                    // clearInterval(checkUserLoginedInterval);
                    var firebaseConfig = {
                        apiKey: "AIzaSyDXLqeJvAQnsKF4BbQIoKCdgceIACqk8tc",
                        authDomain: "do-an-97b42.firebaseapp.com",
                        databaseURL: "https://do-an-97b42.firebaseio.com",
                        projectId: "do-an-97b42",
                        storageBucket: "do-an-97b42.appspot.com",
                        messagingSenderId: "588177691940",
                        appId: "1:588177691940:web:f31e31b573b96b6d6bcfb1",
                        measurementId: "G-433HZVFJJH"
                    };

                    firebase.initializeApp(firebaseConfig);
                    messaging = firebase.messaging();

                    if ('serviceWorker' in navigator) {
                        navigator.serviceWorker.register(workerUrl)
                        .then(async function (registration) {
                            console.log('Registered service worker');
                            messaging.useServiceWorker(registration);
                            registration.update();
                            
                            // messaging.onTokenRefresh(function () {
                            //     messaging.getToken()
                            //         .then(function (refreshedToken) {
                            //             console.log('refresh: ', refreshedToken);
                            //             checkPermissionAllow();
                            //         }).catch(function (err) {
                            //             console.log('Unable to retrieve refreshed token ', err);
                            //         });
                            // });
    
                            checkPermissionAllow();
                        })
                        .catch(function (err) {
                            console.log('Register service worker failed', err);
                        });
                    } else {
                        if (!('serviceWorker' in navigator)) {
                            console.error('No Service Worker support!')
                        }
                        if (!('PushManager' in window)) {
                            console.error('No Push API Support!')
                        }
                    }
                // }
            // }, 100);
        }
    }

    function requestPermission() {
        messaging.requestPermission()
            .then(function () {
                return messaging.getToken();
            })
            .then(function (token) {
                console.log('saveTokenForUser:', token);
            })
            .catch(function (err) {
                console.log('Unable to get permission to notify.', err);
            });
    }

    function checkPermissionAllow() {
        if (Notification.permission !== 'denied') {
            // Chưa allow thì hiển thị yêu cầu
            requestPermission();
        }
    }
})(window);