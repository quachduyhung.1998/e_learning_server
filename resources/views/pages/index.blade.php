<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Firebase</title>

    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script async defer src="{{ asset('js/ServiceWorkerSDK.js') }}"></script>
    <script>

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;
        var PUSHER_APP_KEY = "8a09307a347bebcc29e4"
        var pusher = new Pusher(PUSHER_APP_KEY, {
            cluster: 'ap1',
            forceTLS: true
        });

        var channel = pusher.subscribe('user-1');
        channel.bind('e-learning-chat', function(data) {
            console.log(data);
            var node = document.createElement("LI");
            var textnode = document.createTextNode(data.sender.name + " => "+ data.message);
            node.appendChild(textnode);
            document.getElementById("myList").appendChild(node);

        });
        </script>
    {{-- <script>
        (function (i, s, o, g, r, a, m) {
            a = s.createElement(o);
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.defer = 1; a.src = g;
            if(a.readyState) {  // only required for IE <9
                a.onreadystatechange = function() {
                    if ( a.readyState === "loaded" || a.readyState === "complete" ) {
                        a.onreadystatechange = null;
                        let mess = new serviceWorkerSDK();
                        mess.askAllowNotification('adasd');
                    }
                };
            }
            m.parentNode.insertBefore(a, m);
        })(window, document, 'script', 'https://hdev.info/js/ServiceWorkerSDK.js', 'elearning');
    </script> --}}
</head>
<body>
    index

    <button id="btn-login">login</button>
    <button id="btn-logout">logout</button>


    <h1>Pusher Test</h1>
    <ul id="myList">
    </ul>

    <script>
        document.getElementById('btn-login').addEventListener('click', function() {
            localStorage.setItem('USER_SESSION', JSON.stringify({
                "id": 3,
                "name": "Teacher",
                "email": "teacher@gmail.com",
                "email_verified_at": null,
                "role": 1,
                "user_id": null,
                "contact": null,
                "gender": null,
                "birthday": null,
                "city": null,
                "district": null,
                "wards": null,
                "avatar": null,
                "status": 1,
                "created_at": "2021-05-21T15:49:58.000000Z",
                "updated_at": "2021-05-21T15:49:58.000000Z"
            }));
        });

        document.getElementById('btn-logout').addEventListener('click', function() {
            localStorage.removeItem('USER_SESSION');
        });
    </script>


    {{-- <script>
        var firebaseConfig = {
            apiKey: "AIzaSyDXLqeJ    vAQnsKF4BbQIoKCdgceIACqk8tc",
            authDomain: "do-an-97b42.firebaseapp.com",
            databaseURL: "https://do-an-97b42.firebaseio.com",
            projectId: "do-an-97b42",
            storageBucket: "do-an-97b42.appspot.com",
            messagingSenderId: "588177691940",
            appId: "1:588177691940:web:f31e31b573b96b6d6bcfb1",
            measurementId: "G-433HZVFJJH"
        };

        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);

        // Retrieve Firebase Messaging object.
        const messaging = firebase.messaging();
        // Add the public key generated from the console here.
        messaging.usePublicVapidKey("BBmLlp4WOgql9fVJ3SH-ZUholRF54Z9WNpXT8_av-vM9ft9sGNwnnLl8LgIRatchugOTuPH4Vv5qtMGcVzAt2Ts");

        function IntitalizeFireBaseMessaging() {
            messaging.requestPermission()
                        .then(function () {
                    // console.log('Notification permission granted.');
                    return messaging.getToken();
                })
                .then(function (token) {
                    console.log(token);
                })
                .catch(function (err) {
                    console.log('Unable to get permission to notify.', err);
                });
        }

        messaging.onTokenRefresh(function () {
            messaging.getToken()
                .then(function (newTok        en) {
                    console.log(`new token: ${newToken}`)
                })
                .catch(function (err) {
                    console.log(err)
                });
        });

        IntitalizeFireBaseMessaging();
    </script> --}}
</body>
</html>
