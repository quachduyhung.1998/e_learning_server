
<!DOCTYPE html>
<html class="loading" lang="vi" data-textdirection="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0,minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Xác thực tài khoản thành công | E-learning</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/favicon.png') }}">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/vendors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/page-misc.min.css') }}">

  </head>
  <body class="horizontal-layout horizontal-menu blank-page navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="blank-page">
    <div class="app-content content ">
      <div class="content-overlay"></div>
      <div class="header-navbar-shadow"></div>
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
          <div class="misc-wrapper">
            <a class="brand-logo" href="{{ url('/') }}">
                <img src="{{ asset('images/logo.png') }}" alt="E-learning logo" width="200">
            </a>
            <div class="misc-inner p-2 p-sm-3">
              <div class="w-100 text-center">
                <h2 class="mb-1">Xác thực tài khoản thành công 🚀</h2>
                <p class="mb-3">Giờ đây bạn có thể khám phá và trải nghiệm nền tảng học tập trực tuyến của chúng tôi. Chúc bạn có những trải nghiệm thật vui vẻ!</p>
                <a class="btn btn-primary mb-1 btn-sm-block" href="{{ url('/') }}">Trải nghiệm ngay</a>
                <img class="img-fluid" src="{{ asset('images/coming-soon.svg') }}" alt="Coming soon page"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>