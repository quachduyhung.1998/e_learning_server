<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>Quên mật khẩu</title>
</head>
<body>
  <table style="background-color:#ffffff" border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
    <tbody>
      <tr>
        <td align="center">
          <div style="max-width:558px;padding:0 10px 0 10px;margin:0 auto 0 auto">
            <table
              style="max-width:558px;font-family:Arial,Helvetica,sans-serif;font-weight:normal;font-size:13px;line-height:18px;color:#444444;padding:0;margin:0"
              border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
              <tbody>
                <tr>
                  <td
                    style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;font-size:15px;line-height:18px;padding:22px 0 8px;border-collapse:collapse;border-bottom:1px solid #dcdcdc"
                    align="left" valign="top" width="100%"><a href="{{ url('/') }}" rel="noopener" target="_blank"><img
                        style="display:block"
                        src="{{ asset('images/logo.png') }}"
                        alt="E-learning - Nền tảng học tập trực tuyến hàng đầu Việt Nam" height="40" border="0" class="CToWUd"></a></td>
                </tr>
                <tr>
                  <td style="padding:30px 0 20px 0" width="100%">
                    <table
                      style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;font-size:13px;line-height:18px;color:#444444;background-color:#ffffff;padding:0;margin:0"
                      border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                      <tbody>
                        <tr>
                          <td width="100%">
                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                              <tbody>
                                <tr>
                                  <td
                                    style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;font-size:13px;line-height:18px;color:#333333;padding:5px 0 5px 0"
                                    valign="top" width="100%">
                                    <table style="max-width:100%;min-width:100%;border-collapse:collapse" border="0"
                                      width="100%" cellspacing="0" cellpadding="0" align="left">
                                      <tbody>
                                        <tr>
                                          <td
                                            style="color:#606060;font-family:Arial,sans-serif;font-size:14px;line-height:150%;text-align:left;padding:0 0px 9px 0px"
                                            valign="top">
                                            <div style="text-align:left">Xin chào <strong>{{ $user->name ?? '' }}</strong>,<br><br>Vì lý do bảo mật nên chúng tôi đã thay đổi mật khẩu của bạn.<br>Mật khẩu mới của bạn là: <strong>{{ $new_password ?? '' }}</strong><br><br>Bạn vui lòng đăng nhập với mật khẩu mới này và đổi lại mật khẩu mới của riêng bạn.<br>Thanks!
                                            </div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table style="max-width:100%;min-width:100%;border-collapse:collapse" border="0"
                                      width="100%" cellspacing="0" cellpadding="0" align="left">
                                      <tbody>
                                        <tr>
                                          <td
                                            style="color:#606060;font-family:Arial,sans-serif;font-size:14px;line-height:150%;text-align:left;padding:0 0px 9px 0px"
                                            valign="top">
                                            <div style="text-align:center"><br><strong><em>Chúc bạn học thêm được nhiều kiến thức từ E-learning!</em></strong></div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td
                    style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;font-size:10px;line-height:14px;color:#666666;text-align:left;border-top-style:solid;border-top-color:#dcdcdc;border-top-width:1px;padding:20px 0 0 0"
                    width="100%">
                    <table style="max-width:100%;min-width:100%;border-collapse:collapse" border="0" width="100%"
                      cellspacing="0" cellpadding="0" align="left">
                      <tbody>
                        <tr>
                          <td
                            style="padding:0px 0px 9px;text-align:center;color:#606060;font-family:Arial,sans-serif;font-size:11px;line-height:125%"
                            colspan="2" valign="top">
                            <div style="text-align:left"><strong style="color:#0c61f2">CÔNG TY CỔ PHẦN E-LEARNING</strong></div>
                          </td>
                        </tr>
                        <tr>
                          <td
                            style="padding:0px 0px 9px;text-align:center;color:#606060;font-family:Arial,sans-serif;font-size:11px;line-height:125%"
                            valign="top">
                            <table style="border-spacing:0px" width="100%" align="left">
                              <tbody>
                                <tr>
                                  <td
                                    style="text-align:center;color:#606060;font-family:Arial,sans-serif;font-size:11px;line-height:125%"
                                    valign="top">
                                    <div style="text-align:left"><strong>Địa chỉ: </strong>Số 18 Phố Viên, Đức Thắng, Bắc Từ Liêm, Hà Nội, Việt Nam<br><strong>Điện thoại: </strong><a href="tel:0377565340">0377 565 340</a> - <strong>Email:
                                      </strong><a href="mailto:suport@elearning.com" target="_blank">suport@elearning.com</a> -
                                      <strong>Website: </strong><a href="{{ url('/') }}" target="_blank">{{ url('/') }}</a>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</body>
</html>
