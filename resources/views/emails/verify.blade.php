<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8" />
    <title>Xác thực tài khoản</title>
</head>
<body>
  <table style="background-color:#ffffff" border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
    <tbody>
      <tr>
        <td align="center">
          <div style="max-width:558px;padding:0 10px 0 10px;margin:0 auto 0 auto">
            <table
              style="max-width:558px;font-family:Arial,Helvetica,sans-serif;font-weight:normal;font-size:13px;line-height:18px;color:#444444;padding:0;margin:0"
              border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
              <tbody>
                <tr>
                  <td
                    style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;font-size:15px;line-height:18px;padding:22px 0 8px;border-collapse:collapse;border-bottom:1px solid #dcdcdc"
                    align="left" valign="top" width="100%"><a href="{{ url('/') }}" rel="noopener" target="_blank"><img
                        style="display:block"
                        src="{{ asset('images/logo.png') }}"
                        alt="E-learning - Nền tảng học tập trực tuyến hàng đầu Việt Nam" height="40" border="0" class="CToWUd"></a></td>
                </tr>
                <tr>
                  <td style="padding:30px 0 20px 0" width="100%">
                    <table
                      style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;font-size:13px;line-height:18px;color:#444444;background-color:#ffffff;padding:0;margin:0"
                      border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                      <tbody>
                        <tr>
                          <td width="100%">
                            <table border="0" width="100%" cellspacing="0" cellpadding="0">
                              <tbody>
                                <tr>
                                  <td
                                    style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;font-size:13px;line-height:18px;color:#333333;padding:5px 0 5px 0"
                                    valign="top" width="100%">
                                    <table style="max-width:100%;min-width:100%;border-collapse:collapse" border="0"
                                      width="100%" cellspacing="0" cellpadding="0" align="left">
                                      <tbody>
                                        <tr>
                                          <td
                                            style="color:#606060;font-family:Arial,sans-serif;font-size:14px;line-height:150%;text-align:left;padding:0 0px 9px 0px"
                                            valign="top">
                                            <div style="text-align:left">Xin chào <strong>{{ $user->name ?? '' }}</strong>,<br><br>Cảm ơn bạn đã đăng ký! 👋<br><br>Vui lòng xác thực tài khoản của bạn bằng cách nhấp vào nút bên dưới và tham gia cộng đồng sáng tạo của chúng tôi, bắt đầu khám phá các tài nguyên hoặc trưng bày tác phẩm của bạn.<br><br>Nếu bạn không đăng ký tài khoản tại E-learning, vui lòng bỏ qua email này hoặc liên hệ với chúng tôi theo địa chỉ <a href="mailto:support@elearning.com">support@elearning.com</a>
                                            </div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table style="min-width:100%;border-collapse:collapse" border="0" width="100%"
                                      cellspacing="0" cellpadding="0">
                                      <tbody>
                                        <tr>
                                          <td style="padding-top:0;padding-right:0px;padding-left:0px" align="center"
                                            valign="top">
                                            <table
                                              style="border-collapse:separate!important;border-radius:3px;background-color:#0c61f2;margin-top:10px;"
                                              border="0" cellspacing="0" cellpadding="0">
                                              <tbody>
                                                <tr>
                                                  <td style="font-family:Arial;font-size:14px;" align="center"
                                                    valign="middle"><a
                                                      style="display:block;padding:15px;font-weight:bold;letter-spacing:normal;line-height:100%;text-align:center;text-decoration:none;color:#ffffff;word-wrap:break-word"
                                                      title="Xác thực tài khoản"
                                                      href="{{ route('verify_email', {{ $user ?? '' }}) }}"
                                                      rel="noopener" target="_blank">Xác
                                                      thực ngay</a></td>
                                                </tr>
                                              </tbody>
                                            </table>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                    <table style="max-width:100%;min-width:100%;border-collapse:collapse" border="0"
                                      width="100%" cellspacing="0" cellpadding="0" align="left">
                                      <tbody>
                                        <tr>
                                          <td
                                            style="color:#606060;font-family:Arial,sans-serif;font-size:14px;line-height:150%;text-align:left;padding:0 0px 9px 0px"
                                            valign="top">
                                            <div style="text-align:center"><br><strong><em>Chúc bạn học thêm được nhiều kiến thức từ E-learning!</em></strong></div>
                                          </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td
                    style="font-family:Arial,Helvetica,sans-serif;font-weight:normal;font-size:10px;line-height:14px;color:#666666;text-align:left;border-top-style:solid;border-top-color:#dcdcdc;border-top-width:1px;padding:20px 0 0 0"
                    width="100%">
                    <table style="max-width:100%;min-width:100%;border-collapse:collapse" border="0" width="100%"
                      cellspacing="0" cellpadding="0" align="left">
                      <tbody>
                        <tr>
                          <td
                            style="padding:0px 0px 9px;text-align:center;color:#606060;font-family:Arial,sans-serif;font-size:11px;line-height:125%"
                            colspan="2" valign="top">
                            <div style="text-align:left"><strong style="color:#0c61f2">CÔNG TY CỔ PHẦN E-LEARNING</strong></div>
                          </td>
                        </tr>
                        <tr>
                          <td
                            style="padding:0px 0px 9px;text-align:center;color:#606060;font-family:Arial,sans-serif;font-size:11px;line-height:125%"
                            valign="top">
                            <table style="border-spacing:0px" width="100%" align="left">
                              <tbody>
                                <tr>
                                  <td
                                    style="text-align:center;color:#606060;font-family:Arial,sans-serif;font-size:11px;line-height:125%"
                                    valign="top">
                                    <div style="text-align:left"><strong>Địa chỉ: </strong>Số 18 Phố Viên, Đức Thắng, Bắc Từ Liêm, Hà Nội, Việt Nam<br><strong>Điện thoại: </strong><a href="tel:0377565340">0377 565 340</a> - <strong>Email:
                                      </strong><a href="mailto:suport@elearning.com" target="_blank">suport@elearning.com</a> -
                                      <strong>Website: </strong><a href="{{ url('/') }}" target="_blank">{{ url('/') }}</a>
                                    </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</body>
</html>
